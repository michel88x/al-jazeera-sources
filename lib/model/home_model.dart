class HomeModel {
  String key, title, icon;
  String value;

  HomeModel({
    required this.title,
    required this.value,
    required this.key,
    required this.icon,
  });

  factory HomeModel.fromJson(Map<String, dynamic> json) {
    return HomeModel(
      title: json['title'],
      icon: json['icon'] ?? "",
      key: json['key'],
      value: json['value'].toString(),
    );
  }
}
