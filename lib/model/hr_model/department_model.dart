class DepartmentModel {
  int id;
String manager_id,user_id;
  String name, deleted_at, signature, manager_name;

  DepartmentModel({
    required this.name,
    required this.deleted_at,
    required this.id,
    required this.manager_name,
    required this.manager_id,
    required this.signature,
    required this.user_id,
  });

  factory DepartmentModel.fromJson(Map<String, dynamic> json) {
    return DepartmentModel(
        id: json['id'],
        deleted_at: json['deleted_at'] ?? "",
        manager_id: json['manager_id'].toString(),
        manager_name: json['manager_name'].toString(),
        signature: json['signature'] ?? "",
        user_id: json['user_id'].toString(),
        name: json['name']);
  }
}
