class CompanyModel {
  int id;

  String name;

  CompanyModel({required this.name, required this.id});

  factory CompanyModel.fromJson(Map<String, dynamic> json) {
    return CompanyModel(id: json['id'], name: json['name']);
  }
}
