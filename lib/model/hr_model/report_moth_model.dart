class ReportMonthModel {
  int id;

  String user_by, date, type, text;
  String file, user_id, created_at;

  String updated_at,
      completed_date,
      file1,
      approver_id,
      url_file,
      url_file2,
      created_date,
      deleted_date,
      completed_date2,
      encrypted_id;

  ReportMonthModel(
      {required this.user_id,
      required this.text,
      required this.file1,
      required this.id,
      required this.approver_id,
      required this.type,
      required this.file,
      required this.created_date,
      required this.created_at,
      required this.updated_at,
      required this.date,
      required this.completed_date,
      required this.completed_date2,
      required this.deleted_date,
      required this.encrypted_id,
      required this.url_file,
      required this.url_file2,
      required this.user_by});

  factory ReportMonthModel.fromJson(Map<String, dynamic> json) {
    return ReportMonthModel(
        id: json['id'],
        url_file2: json['url_file2'],
        file1: json['file1'].toString(),
        approver_id: json['approver_id'].toString(),
        created_date: json['created_date'],
        text: json['text'].toString(),
        created_at: json['created_at'],
        type: json['type'].toString(),
        updated_at: json['updated_at'].toString(),
        date: json['date'],
        completed_date2: json['completed_date2'],
        completed_date: json['completed_date'],
        deleted_date: json['deleted_date'],
        encrypted_id: json['encrypted_id'].toString(),
        file: json['file'].toString(),
        user_id: json['user_id'].toString(),
        url_file: json['url_file'],
        user_by: json['user_by']);
  }
}
