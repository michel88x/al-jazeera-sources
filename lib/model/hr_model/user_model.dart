class UserModel {
  int id;

  String name, email;

  UserModel({required this.name, required this.id, required this.email});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(id: json['id'], name: json['name'], email: json['email']);
  }
}
