import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/blocObserver/TodoBlocObserver.dart';
import 'package:employee/screens/general/splash_screen/view/splash_screen.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  ByteData data = await PlatformAssetBundle().load('assets/ca/lets-encrypt-r3.pem');
  SecurityContext.defaultContext.setTrustedCertificatesBytes(data.buffer.asUint8List());
  BlocOverrides.runZoned(
        () =>   runApp(EasyLocalization(
            supportedLocales: const [
              Locale("en","US"),
              Locale("ar", "SA"),
            ],
            path: "assets/translations",
            saveLocale: false,
            startLocale: Locale(defaultLanguageCode, defaultLanguageCountry),
            fallbackLocale: Locale(defaultLanguageCode, defaultLanguageCountry),
            child: const MyApp())),
    blocObserver: TodoBlocObserver(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'مصادر الجزيرة',
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
    );
  }
}
