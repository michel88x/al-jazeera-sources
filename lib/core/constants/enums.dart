enum APIMethod {
  post,
  get,
  put,
  files,
  delete
}

enum ResponseType {
  object,
  list
}