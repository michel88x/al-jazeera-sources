import 'dart:convert';

import 'package:employee/core/constants/enums.dart';
import 'package:employee/core/network/models/base_list_response.dart';
import 'package:employee/core/network/models/s_base_response.dart';
import 'package:employee/core/utils/shared_pref.dart';
import 'package:employee/screens/auth/login_screen/models/user_object.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:logger/logger.dart';

goto(BuildContext context, Widget screen) {
  Navigator.push(context, CupertinoPageRoute(builder: (context) {
    return screen;
  }));
}

gotoData(BuildContext context, Widget screen) async {
  var data =
      await Navigator.push(context, CupertinoPageRoute(builder: (context) {
    return screen;
  }));

  return data;
}

gotoReplace(BuildContext context, Widget screen) {
  Navigator.pushReplacement(context, CupertinoPageRoute(builder: (context) {
    return screen;
  }));
}

gotoRemoveUntil(BuildContext context, Widget screen) {
  Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(builder: (context) => screen),
      (Route<dynamic> route) => false);
}

goBack(BuildContext context, {Object? data}) {
  if (data != null) {
    Navigator.pop(context, data);
  } else {
    Navigator.pop(context);
  }
}

openDialog(BuildContext context, Widget dialog) {
  Navigator.of(context).push(PageRouteBuilder(
      transitionDuration: const Duration(milliseconds: 500),
      transitionsBuilder: (_, a, __, c) => FadeTransition(opacity: a, child: c),
      opaque: false,
      pageBuilder: (BuildContext context, _, __) => dialog));
}

openDialogReplace(BuildContext context, Widget dialog) {
  Navigator.of(context).pushReplacement(PageRouteBuilder(
      transitionDuration: const Duration(milliseconds: 500),
      transitionsBuilder: (_, a, __, c) => FadeTransition(opacity: a, child: c),
      opaque: false,
      pageBuilder: (BuildContext context, _, __) => dialog));
}

void logWizard({String? info, String? warning, String? error, String? debug}) {
  var logger = Logger(
    printer: PrettyPrinter(lineLength: 200, colors: true, printTime: true),
  );
  if (info != null) {
    logger.i(info);
  } else if (warning != null) {
    logger.w(warning);
  } else if (error != null) {
    logger.e(error);
  } else if (debug != null) {
    logger.d(debug);
  }
}

Future<SBaseResponse?>? callObjectAPI(
    {required APIMethod? method,
    required String? url,
    Map<String, String>? body,
    String? stringBody,
    String? accessToken}) async {
  if (stringBody != null) {
    logWizard(
        warning:
            "API Call \nMethod : ${method.toString()}\nUrl : $url\nBody : $stringBody\nAccess token : ${accessToken ?? "No access token"}");
  } else {
    logWizard(
        warning:
            "API Call \nMethod : ${method.toString()}\nUrl : $url\nBody : ${body != null ? body.toString() : "No body"}\nAccess token : ${accessToken ?? "No access token"}");
  }
  try {
    Response? response;
    if (method == APIMethod.get) {
      response = await get(Uri.parse(url!),
          headers: accessToken != null
              ? {
                  "Accept": "application/json",
                  "mobile" : "1",
                  "Authorization": "Bearer $accessToken",
                }
              : {
                  "Accept": "application/json",
                  "mobile" : "1",
                });
      return processAPI(response);
    } else if (method == APIMethod.post || method == APIMethod.put) {
      Map<String, String> headers = <String, String>{};
      headers["Accept"] = "application/json";
      headers["mobile"] = "1";
      if (accessToken != null) {
        headers["Authorization"] = "Bearer $accessToken";
      }
      if (stringBody != null) {
        headers["Content-Type"] = "application/json";
      }
      response = method == APIMethod.post
          ? await post(Uri.parse(url!),
              body: stringBody ?? body, headers: headers)
          : await put(Uri.parse(url!),
              body: stringBody ?? body, headers: headers);
      return processAPI(response);
    } else if (method == APIMethod.delete) {
      Map<String, String> headers = <String, String>{};
      headers["Accept"] = "application/json";
      headers["mobile"] = "1";
      if (accessToken != null) {
        headers["Authorization"] = "Bearer $accessToken";
      }
      if (stringBody != null) {
        headers["Content-Type"] = "application/json";
      }
      response = await delete(Uri.parse(url!), headers: headers);
      return processAPI(response);
    }
  } catch (e) {
    logWizard(error: "error occurred: ${e.toString()}");
    return null;
  }
  return null;
}

Future<BaseListResponse?>? callListAPI(
    {required APIMethod? method,
    required String? url,
    Map<String, String>? body,
    String? stringBody,
    String? accessToken}) async {
  logWizard(
      warning:
          "API Call \nMethod : ${method.toString()}\nUrl : $url\nBody : ${body != null ? body.toString() : "No body"}\nAccess token : ${accessToken ?? "No access token"}");
  try {
    Response? response;
    if (method == APIMethod.get) {
      response = await get(Uri.parse(url!),
          headers: accessToken != null
              ? {
                  "Accept": "application/json",
                  "mobile": "1",
                  "Authorization": "Bearer $accessToken",
                }
              : {
                  "Accept": "application/json",
                  "mobile": "1",
                });
      return processListAPI(response);
    } else if (method == APIMethod.post) {
      response = await post(Uri.parse(url!),
          body: stringBody ?? body,
          headers: accessToken != null
              ? {
                  "Accept": "application/json",
                  "mobile": "1",
                  "Authorization": "Bearer $accessToken",
                }
              : {
                  "Accept": "application/json",
                  "mobile": "1",
                });
      return processListAPI(response);
    }
  } catch (e) {
    logWizard(error: "error occurred: ${e.toString()}");
    return null;
  }
  return null;
}

SBaseResponse? processAPI(Response response) {
  logWizard(error: response.body);
  if (response.statusCode != 500) {
    SBaseResponse base = SBaseResponse.fromJson(json.decode(response.body));
    return base;
  } else {
    return null;
  }
}

Future<BaseListResponse?>? processStreamedListAPI(
    StreamedResponse response) async {
  if (response.statusCode != 500) {
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    logWizard(error: responseString);
    BaseListResponse base =
        BaseListResponse.fromJson(json.decode(responseString));
    return base;
  } else {
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    logWizard(error: responseString);
    return null;
  }
}

Future<SBaseResponse?>? processStreamedAPI(StreamedResponse response) async {
  if (response.statusCode != 500) {
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    logWizard(error: responseString);
    SBaseResponse base = SBaseResponse.fromJson(json.decode(responseString));
    return base;
  } else {
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    logWizard(error: responseString);
    return null;
  }
}

BaseListResponse? processListAPI(Response response) {
  logWizard(error: response.body);
  if (response.statusCode != 500) {
    BaseListResponse base =
        BaseListResponse.fromJson(json.decode(response.body));
    return base;
  } else {
    return null;
  }
}

processResponse(
    {ResponseType? type,
    SBaseResponse? objectResponse,
    BaseListResponse? listResponse,
    VoidCallback? onSuccess,
    VoidCallback? onFailure}) {
  if (type == ResponseType.object) {
    if (objectResponse != null && objectResponse.success == true) {
      onSuccess!;
    } else {
      onFailure!;
    }
  } else {
    if (listResponse != null && listResponse.success == true) {
      onSuccess!;
    } else {
      onFailure!;
    }
  }
}

Future<void> saveUser(UserObject user) async{
  await SharedPref.saveString(SharedPref.user, jsonEncode(user.toJson()));
}

Future<void> saveToken(String token) async{
  await SharedPref.saveString(SharedPref.token, token);
}

Future<UserObject> getUser()async{
  String? userString = await SharedPref.getString(SharedPref.user);
  UserObject user = UserObject.fromJson(json.decode(userString!));
  return user;
}

Future<String?> getToken() async {
  String? token = await SharedPref.getString(SharedPref.token);
  return token;
}

showSuccessMessage(String message){
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.green,
      textColor: Colors.white,
      fontSize: 16.0);
}

showFailureMessage(String message){
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0);
}

String detectLanguage({required String string}) {

  String languageCodes = 'en';

  final RegExp english = RegExp(r'^[a-zA-Z]+');
  final RegExp arabic = RegExp(r'^[\u0621-\u064A]+');

  if (english.hasMatch(string)) languageCodes = 'en';
  if (arabic.hasMatch(string)) languageCodes = 'ar';

  return languageCodes;
}
