import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  static const String pri = "pri";
  static const String log = "log";
  static const String token = "token";
  static const String user = "user";

  static Future<String> saveString(String key, String v) async{
    var sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(key, v);
    return v;
  }

  static Future<String?> getString(String key) async{
    var sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(key) ?? "";
  }
}