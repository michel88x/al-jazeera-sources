import 'package:employee/core/app/app_colors.dart';
import 'package:flutter/material.dart';

class CustomRadio extends StatelessWidget {

  final bool value;
  final double? size;

  const CustomRadio({Key? key, required this.value, this.size = 20}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: value? AppColors.primary : Colors.grey)
      ),
      child: Padding(
        padding: const EdgeInsets.all(3),
        child: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            color: value? AppColors.primary : Colors.transparent,
            shape: BoxShape.circle
          ),
        ),
      ),
    );
  }
}
