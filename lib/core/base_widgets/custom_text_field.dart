import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {

  final double? width;
  final double? borderRadius;
  final Color? borderColor;
  final Color? backColor;
  final EdgeInsetsGeometry? boxPaddings;
  final TextEditingController? controller;
  final String? label;
  final Widget? prefixIcon;
  final TextStyle? labelStyle;
  final double? fieldHorizontalPadding;
  final double? fieldVerticalPadding;
  final bool? isPassword;


  const CustomTextField({
    Key? key,
    this.width,
    this.borderRadius,
    this.borderColor,
    this.backColor,
    this.boxPaddings,
    this.controller,
    this.label,
    this.prefixIcon,
    this.labelStyle,
    this.isPassword = false,
    this.fieldVerticalPadding,
    this.fieldHorizontalPadding
  }) : super(key: key);

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  bool hidePassword = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(widget.borderRadius!),
          border: Border.all(
              width: 1, color: widget.borderColor!),
          color: widget.backColor),
      child: Padding(
        padding: widget.boxPaddings!,
        child: TextField(
          controller: widget.controller,
          obscureText: widget.isPassword! && hidePassword,
          onTap: () {
            setState(() {
              widget.controller!.selection =
                  TextSelection.fromPosition(TextPosition(
                      offset: widget.controller!.text.length));
            });
          },
          decoration: InputDecoration(
            border: InputBorder.none,
            labelText: widget.label,
            prefixIcon: widget.prefixIcon,
            suffixIcon: widget.isPassword!?
            InkWell(
                child: const Icon(
                  Icons.remove_red_eye_outlined,
                  color: Colors.grey,
                ),
                onTap: () {
                  setState(() {
                    hidePassword = !hidePassword;
                  });
                }) : null,
            labelStyle: widget.labelStyle,
            contentPadding: EdgeInsets.symmetric(
                horizontal: widget.fieldHorizontalPadding!, vertical: widget.fieldVerticalPadding!),
          ),
        ),
      ),
    );
  }
}
