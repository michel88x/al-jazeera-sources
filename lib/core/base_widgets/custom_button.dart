import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {

  final VoidCallback? onPressed;
  final EdgeInsetsGeometry? padding;
  final Color? backColor;
  final double? borderRadius;
  final EdgeInsetsGeometry? innerPadding;
  final String? title;
  final TextStyle? textStyle;
  final bool? isTextCentered;


  const CustomButton({
    Key? key,
    this.onPressed,
    this.padding,
    this.backColor,
    this.borderRadius,
    this.innerPadding,
    this.title,
    this.textStyle,
    this.isTextCentered = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Padding(
          padding: padding!,
          child: Container(
            decoration: BoxDecoration(
                color: backColor,
                borderRadius: BorderRadius.circular(borderRadius!)),
            child: Padding(
                padding: innerPadding!,
                child: Text(
                  title!,
                  textAlign: isTextCentered!? TextAlign.center : TextAlign.right,
                  style: textStyle,
                )),
          )),
    );
  }
}
