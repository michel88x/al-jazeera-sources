
import 'dart:io';
import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'dart:ui' as ui;

class PickFileWidget extends StatefulWidget {

  final String? inputFilePath;
  final Function(String) onFileSelected;
  final VoidCallback onFileRemoved;

  const PickFileWidget({Key? key, this.inputFilePath, required this.onFileSelected, required this.onFileRemoved}) : super(key: key);

  @override
  State<PickFileWidget> createState() => _PickFileWidgetState();
}

class _PickFileWidgetState extends State<PickFileWidget> {

  String selectedFilePath = "";
  String selectedFileName = "";
  String selectedFileSize = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.inputFilePath != null && widget.inputFilePath!.isNotEmpty){
      setState(() {
        selectedFilePath = widget.inputFilePath!;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,
      decoration: BoxDecoration(
        color: Colors.grey[100],
        borderRadius: BorderRadius.circular(5)
      ),
      padding: const EdgeInsets.all(5),
      child: selectedFilePath.isNotEmpty?
      Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: Colors.grey[800],
          borderRadius: BorderRadius.circular(5)
        ),
        child: Center(
          child: Directionality(
            textDirection: ui.TextDirection.ltr,
            child: Row(
              children: [
                const SizedBox(width: 5,),
                GestureDetector(
                  onTap: (){
                    setState(() {
                      selectedFilePath = "";
                      selectedFileName = "";
                      selectedFileSize = "";
                    });
                    widget.onFileRemoved();
                  },
                  child: const Icon(
                    Icons.remove_circle,
                    size: 30,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(width: 15,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        selectedFileName,
                        maxLines: 1,
                        overflow: TextOverflow.clip,
                        textAlign: TextAlign.left,
                        style: AppStyles.cairoTwelveBold.copyWith(color: Colors.white),
                      ),
                      Text(
                        selectedFileSize,
                        style: AppStyles.cairoTwelve.copyWith(color: Colors.white),
                      )
                    ],
                  ),
                ),
                const SizedBox(width: 5,),
              ],
            ),
          ),
        ),
      ) : GestureDetector(
        onTap: () async{
          FilePickerResult? result = await FilePicker.platform.pickFiles(
            type: FileType.custom,
            allowedExtensions: ['jpg','png','pdf','doc','mp4']
          );
          if(result != null && result.files.single.path != null){
            setState(() {
              selectedFilePath = result.files.single.path!;
            });
            getFileInfo(selectedFilePath);
            widget.onFileSelected(selectedFilePath);
          }else{
            showFailureMessage("error_selecting_file".tr());
          }
        },
        child: SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: Center(
            child: Text(
              "pick_file".tr(),
              textAlign: TextAlign.center,
              style: AppStyles.cairoFourteen,
            ),
          ),
        ),
      ),
    );
  }

  getFileInfo(String path){
    File file = File(path);
    String fileName = basename(file.path);
    String fileSize = "";
    int bytes = file.lengthSync();
    if (bytes <= 0) fileSize = "0B";
    const suffixes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    var i = (log(bytes) / log(1000)).floor();
    fileSize = '${(bytes / pow(1000, i)).toStringAsFixed(1)} ${suffixes[i]}';
    setState(() {
      selectedFileName = fileName;
      selectedFileSize = fileSize;
    });
  }
}
