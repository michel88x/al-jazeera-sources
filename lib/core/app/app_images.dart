class AppImages{
  static const String images = "assets/images";

  static const String logo = "$images/logo.png";
  static const String spl = "$images/spl.json";
  static const String spla = "$images/spla.json";
  static const String splt = "$images/splt.json";
  static const String user = "$images/user.jpg";
  static const String whats = "$images/whats.png";
}