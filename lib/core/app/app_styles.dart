import 'package:employee/core/app/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppStyles {
  ///Tajawal

  //Normal
  static TextStyle tajawalNormalGrey = GoogleFonts.tajawal(
      textStyle: GoogleFonts.cairo(
    color: Colors.grey,
  ));
  static TextStyle tajawalNormalGreyOpacity = GoogleFonts.tajawal(
      textStyle: GoogleFonts.cairo(
        color: Colors.grey.withOpacity(0.5),
      ));

  ///Acme

  //21
  static TextStyle acmeTwentyOnePrimaryBoldItalic = GoogleFonts.acme(
      fontWeight: FontWeight.bold,
      color: AppColors.primary,
      fontStyle: FontStyle.italic,
      fontSize: 21);

  ///Aleo

  //12
  static TextStyle aleoTwelvePrimaryBold = GoogleFonts.aleo(
      fontWeight: FontWeight.bold, color: AppColors.primary, fontSize: 12);

  //15
  static TextStyle aleoFifteenWhiteBold = GoogleFonts.aleo(
    fontWeight: FontWeight.bold,
    fontSize: 15,
    color: Colors.white,
  );

  //17
  static TextStyle aleoSeventeenWhiteBold = GoogleFonts.aleo(
    fontWeight: FontWeight.bold,
    fontSize: 17,
    color: Colors.white,
  );

  ///Cairo

  //Normal
  static TextStyle cairoNormalBold =
      GoogleFonts.cairo(fontWeight: FontWeight.bold);
  static TextStyle cairoNormalWhiteBold =
      GoogleFonts.cairo(fontWeight: FontWeight.bold, color: Colors.white);
  static TextStyle cairoNormal = GoogleFonts.cairo();
  static TextStyle cairoNormalSecondaryBold = GoogleFonts.cairo(
      color: AppColors.secondary, fontWeight: FontWeight.bold);
  static TextStyle cairoNormalSecondary = AppStyles.cairoNormalSecondary;
  static TextStyle cairoNormalBlack = GoogleFonts.cairo(color: Colors.black);
  static TextStyle cairoNormalPrimary =
      GoogleFonts.cairo(color: AppColors.primary);
  static TextStyle cairoNormalPrimaryBold =
  GoogleFonts.cairo(color: AppColors.primary, fontWeight: FontWeight.bold);
  static TextStyle cairoNormalGrey =
  GoogleFonts.cairo(color: Colors.grey);
  static TextStyle cairoNormalBlackBold =
  GoogleFonts.cairo(color: Colors.black, fontWeight: FontWeight.bold);
  static TextStyle cairoNormalGreenBold =
  GoogleFonts.cairo(color: Colors.green, fontWeight: FontWeight.bold);
  static TextStyle cairoNormalRedBold =
  GoogleFonts.cairo(color: Colors.red, fontWeight: FontWeight.bold);

  //9
  static TextStyle cairoNineBold =
      GoogleFonts.cairo(fontWeight: FontWeight.bold, fontSize: 9);

  //10
  static TextStyle cairoTenSecondaryBold = GoogleFonts.cairo(
      color: AppColors.secondary, fontWeight: FontWeight.bold, fontSize: 10);
  static TextStyle cairoTenBold = GoogleFonts.cairo(
      fontWeight: FontWeight.bold, fontSize: 10);

  //11
  static TextStyle cairoElevenSecondaryBold = GoogleFonts.cairo(
      color: AppColors.secondary,
      fontWeight: FontWeight.bold,
      fontSize: 11);
  static TextStyle cairoEleven = GoogleFonts.cairo(
      fontSize: 11);

  //12
  static TextStyle cairoTwelveWhiteBold = GoogleFonts.cairo(
      fontSize: 12, color: Colors.white, fontWeight: FontWeight.bold);
  static TextStyle cairoTwelvePrimaryBold = GoogleFonts.cairo(
      color: AppColors.primary, fontWeight: FontWeight.bold, fontSize: 12);
  static TextStyle cairoTwelveSecondary =
      GoogleFonts.cairo(color: AppColors.secondary, fontSize: 12);
  static TextStyle cairoTwelve = GoogleFonts.cairo(fontSize: 12);
  static TextStyle cairoTwelveRedBold = GoogleFonts.cairo(
      color: Colors.red,
      fontWeight: FontWeight.bold,fontSize: 12);
  static TextStyle cairoTwelveBold =
      GoogleFonts.cairo(fontWeight: FontWeight.bold, fontSize: 12);
  static TextStyle cairoTwelveGrey =
      GoogleFonts.cairo(color: Colors.grey, fontSize: 12);
  static TextStyle cairoTwelveBlack =
      GoogleFonts.cairo(fontSize: 12, color: Colors.black);
  static TextStyle cairoTwelveBlackBold =
  GoogleFonts.cairo(fontSize: 12, color: Colors.black, fontWeight: FontWeight.bold);
  static TextStyle cairoTwelveGreyBold =
  GoogleFonts.cairo(fontSize: 12, color: Colors.grey, fontWeight: FontWeight.bold);
  static TextStyle cairoTwelveSecondaryBold =
  GoogleFonts.cairo(fontSize: 12, color: AppColors.secondary, fontWeight: FontWeight.bold);

  //14
  static TextStyle cairoFourteenBlueUnderline = GoogleFonts.cairo(
      fontSize: 14.0,
      decoration: TextDecoration.underline,
      color: Colors.blue[700]);
  static TextStyle cairoFourteen = GoogleFonts.cairo(
      fontSize: 14.0,);
  static TextStyle cairoFourteenPrimaryBold = GoogleFonts.cairo(
      fontSize: 14.0,
      fontWeight: FontWeight.bold,
      color: AppColors.primary);
  static TextStyle cairoFourteenWhiteBold = GoogleFonts.cairo(
      fontSize: 14.0,
      fontWeight: FontWeight.bold,
      color: Colors.white);

  //15
  static TextStyle cairoFifteenDarkGreyBold = GoogleFonts.cairo(
      color: AppColors.darkGrey, fontWeight: FontWeight.bold, fontSize: 15);
  static TextStyle cairoFifteenDarkBlackBold = GoogleFonts.cairo(
      color: AppColors.darkBlack, fontWeight: FontWeight.bold, fontSize: 15);
  static TextStyle cairoFifteenWhiteBold = GoogleFonts.cairo(
      color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15);
  static TextStyle cairoFifteenPrimaryBold = GoogleFonts.cairo(
      color: AppColors.primary, fontWeight: FontWeight.bold, fontSize: 15);
  static TextStyle cairoFifteen = GoogleFonts.cairo(fontSize: 15);

  //16
  static TextStyle cairoSixteenBlackBold = GoogleFonts.cairo(
      fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold);
  static TextStyle cairoSeventeenDarkBlackBold = GoogleFonts.cairo(
      fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.bold);

  //19
  static TextStyle cairoNineteenWhiteBold = GoogleFonts.cairo(
      fontSize: 19.0, fontWeight: FontWeight.bold, color: Colors.white);
  static TextStyle cairoNineteenPrimaryBold = GoogleFonts.cairo(
      fontSize: 19.0, fontWeight: FontWeight.bold, color: AppColors.primary);
  static TextStyle cairoNineteenSecondaryBold = GoogleFonts.cairo(
      fontSize: 19.0, fontWeight: FontWeight.bold, color: AppColors.secondary);
  static TextStyle cairoNineteen = GoogleFonts.cairo(
    fontSize: 19,
  );

  //20
  static TextStyle cairoTwentyBlackBold = GoogleFonts.cairo(
      fontSize: 20.0, color: Colors.black, fontWeight: FontWeight.bold);

  //27
  static TextStyle cairoTwentySevenBlackBold = GoogleFonts.cairo(
    color: Colors.black,
    fontSize: 27,
    fontWeight: FontWeight.bold,
  );
}
