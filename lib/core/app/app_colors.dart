import 'package:flutter/material.dart';

class AppColors{
  static const Color primary = Color(0xFF7367F0);
  static const Color secondary = Color(0xFFff6868);

  //Red
  static const Color red = Color(0xFFEA5455);
  static const Color glowingRed = Color(0xFFFF0101);

  //Orange
  static const Color lightOrange = Color(0xFFFF9F43);

  //Green
  static const Color lightGreen = Color(0xFF28C76F);

  //Grey
  static const Color darkGrey = Color(0xFF6E6B7B);
  static const Color lightGrey = Color(0xFFC3C3C3);

  //Black
  static const Color darkBlack = Color(0xFF1E1E1E);

}