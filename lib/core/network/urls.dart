class Urls{
  static const String base = "https://masader-j.com/m-test/masader/api2";
  static const String baseImage = "https://masader-j.com/m-test/masader/";

  static const String login = "$base/login";
  static const String resendCode = "$base/resend";
  static const String confirmOtp = "$base/confirm";
  static const String matchingEmployeesBalances = "$base/admin/employee-balance-check";
  static const String vacations = "$base/admin/leaves";
  static const String achievements = "$base/admin/task-daily-report/2";
  static const String workPlan = "$base/admin/work-plan";
  static const String addWorkPlanResume = "$base/admin/work-plan/aprrove";
  static const String workRecords = "$base/admin/users/logs";
  static const String permissionRequests = "$base/admin/vacation-reports";
  static const String vacationRequests = "$base/admin/vacation";
  static const String employeesAccounts = "$base/admin/users"; //?page={page_number}
  static const String searchFiltersList = "$base/list_selecte";
  static const String allEmployeesAccounts = "$base/admin/users?page=1&all_pages=true";
  static const String employeeAccount = "$base/admin/user/"; //{user_id}/reemployee
  static const String allEmployeeSalaries = "$base/admin/salary?page=1&all_pages=true";
  static const String employeeSalary = "$base/admin/salary/"; //{account_id}/report
  static const String documents = "$base/admin/documents";
  static const String documentTypes = "$base/list_selecte?list=document_types";
  static const String employeesActivityRecords = "$base/admin/activity-log";
  static const String employeesTimeRecords = "$base/admin/time-in-system";
  static const String delegatesCommissions = "$base/admin/delegates-commissions";
  static const String assignJobs = "$base/admin/assignment-of-works";
  static const String companyExpenses = "$base/admin/workshopCompany";
  static const String companyEquipments = "$base/admin/company-equipment";
}