import 'package:employee/core/network/models/confirm_object.dart';

class SBaseResponse {
  bool success = false;
  ConfirmObject? confirm;
  String? message = "";
  Map<String, dynamic>? data = {};

  SBaseResponse({
    this.confirm,
    this.message,
    this.data,
    required this.success,
  });

  SBaseResponse.fromJson(Map<String, dynamic> jsonn)
      : success = jsonn["success"] ?? false,
        confirm = jsonn["confirm"] != null? ConfirmObject.fromJson(jsonn["confirm"]) : null,
        data = jsonn["data"],
        message = jsonn["message"] ?? "";
}