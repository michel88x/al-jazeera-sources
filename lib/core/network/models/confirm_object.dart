import 'package:employee/screens/auth/login_screen/models/permission_object.dart';

class ConfirmObject {
  bool? confirm;
  bool? isItAccount;
  List<PermissionObject>? permissions;

  ConfirmObject({this.confirm, this.isItAccount, this.permissions});

  ConfirmObject.fromJson(Map<String, dynamic> json) {
    confirm = json['confirm'];
    isItAccount = json['is_it_account'];
    if (json['permissions'] != null) {
      permissions = <PermissionObject>[];
      json['permissions'].forEach((v) {
        permissions!.add(new PermissionObject.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['confirm'] = this.confirm;
    data['is_it_account'] = this.isItAccount;
    if (this.permissions != null) {
      data['user_permissions'] = this.permissions!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}