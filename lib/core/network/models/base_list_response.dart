class BaseListResponse {
  bool? success = false;
  bool? confirm = false;
  String? message = "";
  List<dynamic>? data = [];

  BaseListResponse({
    this.success,
    this.confirm,
    this.message,
    this.data,
  });

  BaseListResponse.fromJson(Map<String, dynamic> jsonn)
      : success = jsonn["success"] ?? false,
        confirm = jsonn["confirm"] ?? false,
        message = jsonn["message"] ?? "",
        data = jsonn["data"];
}