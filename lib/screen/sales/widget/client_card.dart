import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ClientCardPage extends StatefulWidget {
  int index;
  int type;

  ClientCardPage(this.index, this.type);

  @override
  State<ClientCardPage> createState() => _ClientCardPage();
}

class _ClientCardPage extends State<ClientCardPage> {
  List<bool> click = [false, false, false, false, false, false];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
      child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [
                BoxShadow(color: Colors.grey, blurRadius: 5, spreadRadius: 1)
              ]),
          child: Padding(
            padding: EdgeInsets.fromLTRB(0, 10, 20, 20),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width - 200,
                          child: widget.type == 8 ||
                                  widget.type == 9 ||
                                  widget.type == 10 ||
                                  widget.type == 11 ||
                                  widget.type == 12 ||
                                  widget.type == 13 ||
                                  widget.type == 17 ||
                                  widget.type == 18
                              ? Text(
                                  'المندوب : عبداللطيف مسئول الورشة',
                                  style: AppStyles.cairoNormalBold,
                                )
                              : widget.type == 19
                                  ? Text(
                                      'الجهة',
                                      style: AppStyles.cairoNormalBold,
                                    )
                                  : Text(
                                      'شذي الجنوب',
                                      style: AppStyles.cairoNormalBold,
                                    ),
                        ),
                        widget.type == 17
                            ? SizedBox(
                                height: 5,
                              )
                            : widget.type == 18
                                ? SizedBox(
                                    height: 5,
                                  )
                                : Visibility(
                                    child: Text(''),
                                    visible: false,
                                  ),
                        widget.type == 17
                            ? Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'المرسل : ',
                                    style: AppStyles.cairoNormalSecondary,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width - 240,
                                    child: Text(
                                      'محمد عبدالفتاح',
                                      style: AppStyles.cairoNormal,
                                    ),
                                  ),
                                ],
                              )
                            : widget.type == 18
                                ? Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'المرسل : ',
                                        style:
                                            AppStyles.cairoNormalSecondary,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width -
                                                240,
                                        child: Text(
                                          'محمد عبدالفتاح',
                                          style: AppStyles.cairoNormal,
                                        ),
                                      ),
                                    ],
                                  )
                                : Visibility(
                                    child: Text(''),
                                    visible: false,
                                  ),
                        widget.type == 17
                            ? SizedBox(
                                height: 5,
                              )
                            : widget.type == 18
                                ? SizedBox(
                                    height: 5,
                                  )
                                : Visibility(
                                    child: Text(''),
                                    visible: false,
                                  ),
                        widget.type == 17
                            ? Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'الموصل : ',
                                    style: AppStyles.cairoNormalSecondary,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width - 240,
                                    child: Text(
                                      'نبيل الباشا تسويق',
                                      style: AppStyles.cairoNormal,
                                    ),
                                  ),
                                ],
                              )
                            : widget.type == 18
                                ? Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'الموصل : ',
                                        style:
                                            AppStyles.cairoNormalSecondary,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width -
                                                240,
                                        child: Text(
                                          'نبيل الباشا تسويق',
                                          style: AppStyles.cairoNormal,
                                        ),
                                      ),
                                    ],
                                  )
                                : Visibility(
                                    child: Text(''),
                                    visible: false,
                                  ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          crossAxisAlignment: widget.type == 8 ||
                                  widget.type == 9 ||
                                  widget.type == 10 ||
                                  widget.type == 11 ||
                                  widget.type == 12 ||
                                  widget.type == 13 ||
                                  widget.type == 17 ||
                                  widget.type == 18 ||
                                  widget.type == 19
                              ? CrossAxisAlignment.start
                              : CrossAxisAlignment.center,
                          children: [
                            widget.type == 8
                                ? Text(
                                    'الطرف الثاني : ',
                                    style: AppStyles.cairoNormalSecondary,
                                  )
                                : widget.type == 9 ||
                                        widget.type == 10 ||
                                        widget.type == 11 ||
                                        widget.type == 12 ||
                                        widget.type == 13 ||
                                        widget.type == 17 ||
                                        widget.type == 18
                                    ? Text(
                                        'اسم العميل : ',
                                        style:
                                            AppStyles.cairoNormalSecondary,
                                      )
                                    : widget.type == 19
                                        ? Text(
                                            'رقم المنافسة : ',
                                            style: AppStyles.cairoNormalSecondary
                                          )
                                        : Icon(Icons.email_outlined,
                                            color: AppColors.secondary, size: 15),
                            SizedBox(
                              width: 5,
                            ),
                            widget.type == 8
                                ? Container(
                                    width:
                                        MediaQuery.of(context).size.width - 230,
                                    child: Text(
                                      'شركة الدريس للخدمات البترولية والنقليات',
                                      style: AppStyles.cairoNormal,
                                    ),
                                  )
                                : widget.type == 9 ||
                                        widget.type == 10 ||
                                        widget.type == 11 ||
                                        widget.type == 12 ||
                                        widget.type == 13 ||
                                        widget.type == 17 ||
                                        widget.type == 18
                                    ? Container(
                                        width:
                                            MediaQuery.of(context).size.width -
                                                240,
                                        child: Text(
                                          'فرع شركة لاحق طلق عبدالرحمن المقاطي للنقليات',
                                          style: AppStyles.cairoNormal,
                                        ),
                                      )
                                    : widget.type == 19
                                        ? Text('345678',
                                            style: AppStyles.cairoNormal)
                                        : Text(
                                            'Shazaaljanoub76@gmail.com',
                                            style: AppStyles.cairoNormal,
                                          ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        widget.type == 8 ||
                                widget.type == 10 ||
                                widget.type == 11 ||
                                widget.type == 12 ||
                                widget.type == 13 ||
                                widget.type == 17 ||
                                widget.type == 18 ||
                                widget.type == 19
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : widget.type == 9
                                ? Visibility(
                                    child: Text(''),
                                    visible: false,
                                  )
                                : Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Icon(Icons.phone_android,
                                          color: AppColors.secondary, size: 15),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        '966559592030',
                                        style: AppStyles.cairoNormal,
                                      ),
                                    ],
                                  ),
                        SizedBox(
                          height: 5,
                        ),
                        widget.type == 0 ||
                                widget.type == 10 ||
                                widget.type == 11 ||
                                widget.type == 17 ||
                                widget.type == 18
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : widget.type == 8
                                ? Visibility(
                                    child: Text(''),
                                    visible: false,
                                  )
                                : widget.type == 5
                                    ? Row(
                                        children: [
                                          Text(
                                            'صندوق البريد : ',
                                            style: AppStyles.cairoTwelveSecondary,
                                          ),
                                          Text(
                                            '',
                                            style: AppStyles.cairoNormal,
                                          ),
                                        ],
                                      )
                                    : widget.type == 9 ||
                                            widget.type == 12 ||
                                            widget.type == 19
                                        ? Row(
                                            children: [
                                              Text(
                                                'عدد أيام التأخير : ',
                                                style: AppStyles.cairoTwelveSecondary,
                                              ),
                                              Text(
                                                '0',
                                                style: AppStyles.cairoNormal,
                                              ),
                                            ],
                                          )
                                        : widget.type == 13
                                            ? Row(
                                                children: [
                                                  Text(
                                                    'الرصيد : ',
                                                    style: AppStyles.cairoTwelveSecondary,
                                                  ),
                                                  Text(
                                                    '43689',
                                                    style: AppStyles.cairoNormal,
                                                  ),
                                                ],
                                              )
                                            : Row(
                                                children: [
                                                  Text(
                                                    'المندوب : ',
                                                    style: AppStyles.cairoTwelveSecondary,
                                                  ),
                                                  Text(
                                                    'حسام خيرى عليان',
                                                    style: AppStyles.cairoNormal,
                                                  ),
                                                ],
                                              ),
                        widget.type == 8 ||
                                widget.type == 10 ||
                                widget.type == 11 ||
                                widget.type == 12 ||
                                widget.type == 13 ||
                                widget.type == 17 ||
                                widget.type == 18
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : SizedBox(
                                height: 5,
                              ),
                        widget.type == 0 ||
                                widget.type == 12 ||
                                widget.type == 13
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : widget.type == 5 ||
                                    widget.type == 17 ||
                                    widget.type == 18
                                ? Row(
                                    children: [
                                      Text(
                                        'المدينة : ',
                                        style: AppStyles.cairoTwelveSecondary,
                                      ),
                                      Text(
                                        'السعودية / العنوان',
                                        style: AppStyles.cairoNormal,
                                      ),
                                    ],
                                  )
                                : widget.type == 8
                                    ? Row(
                                        children: [
                                          Text(
                                            'حالة الاستكمال : ',
                                            style: AppStyles.cairoTwelveSecondary,
                                          ),
                                          Text(
                                            'مكتمل',
                                            style: AppStyles.cairoNormal,
                                          ),
                                        ],
                                      )
                                    : widget.type == 9 || widget.type == 10
                                        ? Row(
                                            children: [
                                              Text(
                                                'Customer id : ',
                                                style: AppStyles.cairoTwelveSecondary,
                                              ),
                                              Text(
                                                '54',
                                                style: AppStyles.cairoNormal,
                                              ),
                                            ],
                                          )
                                        : widget.type == 11
                                            ? Row(
                                                children: [
                                                  Text(
                                                    'رقم الفاتورة : ',
                                                    style: AppStyles.cairoTwelveSecondary,
                                                  ),
                                                  Text(
                                                    '221001000458',
                                                    style: AppStyles.cairoNormal,
                                                  ),
                                                ],
                                              )
                                            : widget.type == 19
                                                ? Row(
                                                    children: [
                                                      Text(
                                                        'الوصف : ',
                                                        style:
                                                            AppStyles.cairoTwelveSecondary,
                                                      ),
                                                      Text(
                                                        'test',
                                                        style:
                                                            AppStyles.cairoNormal,
                                                      ),
                                                    ],
                                                  )
                                                : Row(
                                                    children: [
                                                      Text(
                                                        'درجة العميل : ',
                                                        style:
                                                            AppStyles.cairoTwelveSecondary,
                                                      ),
                                                      Text(
                                                        'A',
                                                        style:
                                                            AppStyles.cairoNormal,
                                                      ),
                                                    ],
                                                  ),
                        widget.type == 0 ||
                                widget.type == 10 ||
                                widget.type == 11 ||
                                widget.type == 12 ||
                                widget.type == 13
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : widget.type == 1
                                ? Visibility(
                                    child: Text(''),
                                    visible: false,
                                  )
                                : widget.type == 9
                                    ? Visibility(
                                        child: Text(''),
                                        visible: false,
                                      )
                                    : widget.type == 2
                                        ? Row(
                                            children: [
                                              Text(
                                                'التاريخ الشهري : ',
                                                style: AppStyles.cairoTwelveSecondary,
                                              ),
                                              Text(
                                                '12/12/2023',
                                                style: AppStyles.cairoNormal,
                                              ),
                                            ],
                                          )
                                        : widget.type == 8
                                            ? Row(
                                                children: [
                                                  Text(
                                                    'تاريخ العقد : ',
                                                    style: AppStyles.cairoTwelveSecondary,
                                                  ),
                                                  Text(
                                                    '12/12/2023',
                                                    style: AppStyles.cairoNormal,
                                                  ),
                                                ],
                                              )
                                            : widget.type == 17 ||
                                                    widget.type == 18
                                                ? Row(
                                                    children: [
                                                      Text(
                                                        'تاريخ الإرسال : ',
                                                        style:
                                                            AppStyles.cairoTwelveSecondary,
                                                      ),
                                                      Text(
                                                        '12/12/2023',
                                                        style:
                                                            AppStyles.cairoNormal,
                                                      ),
                                                    ],
                                                  )
                                                : widget.type == 19
                                                    ? Row(
                                                        children: [
                                                          Text(
                                                            'المبلغ : ',
                                                            style: AppStyles.cairoTwelveSecondary,
                                                          ),
                                                          Text(
                                                            '45678',
                                                            style: AppStyles.cairoNormal,
                                                          ),
                                                        ],
                                                      )
                                                    : Row(
                                                        children: [
                                                          Text(
                                                            'التاريخ : ',
                                                            style: AppStyles.cairoTwelveSecondary,
                                                          ),
                                                          Text(
                                                            '12/12/2023',
                                                            style: AppStyles.cairoNormal,
                                                          ),
                                                        ],
                                                      ),
                        widget.type != 5
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : Row(
                                children: [
                                  Text(
                                    'العنوان : ',
                                    style: AppStyles.cairoTwelveSecondary,
                                  ),
                                  Text(
                                    '',
                                    style: AppStyles.cairoNormal,
                                  ),
                                ],
                              ),
                        widget.type == 17
                            ? Row(
                                children: [
                                  Text(
                                    'رقم الفاتورة : ',
                                    style: AppStyles.cairoTwelveSecondary,
                                  ),
                                  Text(
                                    '234567890987654',
                                    style: AppStyles.cairoNormal,
                                  ),
                                ],
                              )
                            : widget.type == 18
                                ? Row(
                                    children: [
                                      Text(
                                        'رقم الفاتورة : ',
                                        style: AppStyles.cairoTwelveSecondary,
                                      ),
                                      Text(
                                        '234567890987654',
                                        style: AppStyles.cairoNormal,
                                      ),
                                    ],
                                  )
                                : Visibility(
                                    child: Text(''),
                                    visible: false,
                                  ),
                        widget.type == 18
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'المستلم : ',
                                        style: AppStyles.cairoTwelveSecondary,
                                      ),
                                      Text(
                                        'محمد احمد',
                                        style: AppStyles.cairoNormal,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 40,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'تاريخ الاستلام : ',
                                        style: AppStyles.cairoTwelveSecondary,
                                      ),
                                      Text(
                                        '12/12/2023',
                                        style: AppStyles.cairoNormal,
                                      ),
                                    ],
                                  )
                                ],
                              )
                            : Visibility(
                                child: Text(''),
                                visible: false,
                              )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0.0),
                    child: widget.type == 10 ||
                            widget.type == 17 ||
                            widget.type == 18
                        ? Visibility(
                            child: Text(''),
                            visible: false,
                          )
                        : widget.type == 8
                            ? Text(
                                'معتمد',
                                style: AppStyles.cairoNormalSecondaryBold,
                              )
                            : widget.type == 9 ||
                                    widget.type == 11 ||
                                    widget.type == 12
                                ? Text(
                                    'مكتمل',
                                    style: AppStyles.cairoNormalSecondaryBold,
                                  )
                                : widget.type == 13
                                    ? Column(
                                        children: [
                                          Text(
                                            'السنة / الشهر',
                                            style: AppStyles.cairoElevenSecondaryBold,
                                          ),
                                          SizedBox(
                                            height: 0,
                                          ),
                                          Text(
                                            '2023 / 03',
                                            style:
                                                AppStyles.cairoEleven,
                                          )
                                        ],
                                      )
                                    : widget.type==19?Text(
                      '12/12/2023',
                      style: AppStyles.cairoNormalSecondaryBold,
                    ):Text(
                                        'asma',
                                        style: AppStyles.cairoNormalSecondaryBold,
                                      ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              widget.type == 17 || widget.type == 18
                  ? Visibility(
                      child: Text(''),
                      visible: false,
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        widget.type == 13
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : Column(
                                children: [
                                  widget.type == 5
                                      ? Text(
                                          'الرمز البريدي',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: AppColors.secondary),
                                        )
                                      : widget.type == 8
                                          ? Text(
                                              'رقم العقد',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: AppColors.secondary),
                                            )
                                          : widget.type == 9 ||
                                                  widget.type == 10
                                              ? Text(
                                                  'تاريخ النهاية',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: AppColors.secondary),
                                                )
                                              : widget.type == 11
                                                  ? Text(
                                                      'المبلغ',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: AppColors.secondary),
                                                    )
                                                  : widget.type == 12
                                                      ? Text(
                                                          'التاريخ',
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color: AppColors.secondary),
                                                        )
                                                      :widget.type == 19
                                      ? Text(
                                    'تاريخ انتهاء العرض',
                                    textAlign:
                                    TextAlign.center,
                                    style: TextStyle(
                                        fontWeight:
                                        FontWeight
                                            .bold,
                                        color: AppColors.secondary),
                                  ): Text(
                                                          'فترة الإتمان',
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color: AppColors.secondary),
                                                        ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    width: 80,
                                    child: widget.type == 5
                                        ? Text(
                                            '8765456',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(fontSize: 11),
                                          )
                                        : widget.type == 8
                                            ? Text(
                                                '326',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(fontSize: 11),
                                              )
                                            : widget.type == 9 ||
                                                    widget.type == 10 ||
                                                    widget.type == 12
                                                ? Text(
                                                    '12/12/2023',
                                                    textAlign: TextAlign.center,
                                                    style:
                                                        TextStyle(fontSize: 11),
                                                  )
                                                : widget.type == 11
                                                    ? Text(
                                                        '220',
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            fontSize: 11),
                                                      )
                                                    : widget.type == 19
                                        ? Text(
                                      '12/12/2023',
                                      textAlign:
                                      TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 11),
                                    )
                                        : Text(
                                                        'لم تحدد',
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            fontSize: 11),
                                                      ),
                                  ),
                                ],
                              ),
                        widget.type == 13
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : Container(
                                height: 70,
                                width: 1,
                                color: Colors.grey.withOpacity(0.6),
                              ),
                        Column(
                          children: [
                            Row(
                              children: [
                                widget.type == 2
                                    ? Icon(
                                        Icons.attach_file,
                                        color: AppColors.secondary,
                                        size: 14,
                                      )
                                    : Visibility(
                                        child: Text(''),
                                        visible: false,
                                      ),
                                widget.type == 8
                                    ? Text(
                                        'مدة العقد',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: AppColors.secondary,
                                        ),
                                      )
                                    : widget.type == 9 || widget.type == 10
                                        ? Text(
                                            'تاريخ الإضافة',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: AppColors.secondary,
                                            ),
                                          )
                                        : widget.type == 11 || widget.type == 12 ||widget.type==19
                                            ? Text(
                                                'تاريخ الاستكمال',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: AppColors.secondary,
                                                ),
                                              )
                                            : widget.type == 13
                                                ? Text(
                                                    'التاريخ',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: AppColors.secondary,
                                                    ),
                                                  )
                                                : Text(
                                                    'السجل التجاري',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontWeight: FontWeight
                                                            .bold,
                                                        color: AppColors.secondary,
                                                        decoration:
                                                            widget.type == 2
                                                                ? TextDecoration
                                                                    .underline
                                                                : TextDecoration
                                                                    .none),
                                                  ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              width: widget.type == 13 ? 160 : 100,
                              child: widget.type == 8
                                  ? Text(
                                      '120',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 11),
                                    )
                                  : widget.type == 10 ||
                                          widget.type == 9 ||
                                          widget.type == 11 ||
                                          widget.type == 12 ||
                                          widget.type == 13||widget.type==19
                                      ? Text(
                                          '12/12/2033',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 11),
                                        )
                                      : Text(
                                          '5855258121',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 11),
                                        ),
                            ),
                          ],
                        ),
                        Container(
                          height: 70,
                          width: 1,
                          color: Colors.grey.withOpacity(0.6),
                        ),
                        Column(
                          children: [
                            Row(
                              children: [
                                widget.type == 2
                                    ? Icon(
                                        Icons.attach_file,
                                        color: AppColors.secondary,
                                        size: 14,
                                      )
                                    : Visibility(
                                        child: Text(''),
                                        visible: false,
                                      ),
                                widget.type == 8
                                    ? Text(
                                        'العدد',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: AppColors.secondary,
                                        ),
                                      )
                                    : widget.type == 9
                                        ? Text(
                                            'تاريخ الاستكمال',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: AppColors.secondary,
                                            ),
                                          )
                                        : widget.type == 10 ||
                                                widget.type == 12 ||
                                                widget.type == 13 ||widget.type==19
                                            ? Text(
                                                'تاريخ الحذف',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: AppColors.secondary,
                                                ),
                                              )
                                            : widget.type == 11
                                                ? Text(
                                                    'التاريخ',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: AppColors.secondary,
                                                    ),
                                                  )
                                                : Text(
                                                    'الرقم الضريبي',
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontWeight: FontWeight
                                                            .bold,
                                                        color: AppColors.secondary,
                                                        decoration:
                                                            widget.type == 2
                                                                ? TextDecoration
                                                                    .underline
                                                                : TextDecoration
                                                                    .none),
                                                  ),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              width: widget.type == 13 ? 160 : 100,
                              child: widget.type == 8
                                  ? Text(
                                      '10',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 11),
                                    )
                                  : widget.type == 9 ||
                                          widget.type == 10 ||
                                          widget.type == 11 ||
                                          widget.type == 12 ||
                                          widget.type == 13||widget.type==19
                                      ? Text(
                                          '01/01/2024',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 11),
                                        )
                                      : Text(
                                          '311293358700003',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 11),
                                        ),
                            ),
                          ],
                        ),
                        Container(
                          height: 70,
                          width: 1,
                          color: Colors.grey.withOpacity(0.6),
                        )
                      ],
                    )
            ]),
          )),
    );
  }
}
