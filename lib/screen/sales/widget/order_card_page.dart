import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class OrderCardPage extends StatefulWidget {
  int index;
  int type;

  OrderCardPage(this.index, this.type);

  @override
  State<OrderCardPage> createState() => _OrderCardPage();
}

class _OrderCardPage extends State<OrderCardPage> {
  List<bool> click = [false, false, false, false, false, false];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
      child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [
                BoxShadow(color: Colors.grey, blurRadius: 5, spreadRadius: 1)
              ]),
          child: Padding(
            padding: EdgeInsets.fromLTRB(0, 10, 20, 20),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width - 200,
                          child: Text(
                            widget.type == 7
                                ? 'وائل اليوسفى العضو المنتدب'
                                : widget.type == 6
                                    ? 'شركة المؤسسة الوطنية السعودية المحدودة'
                                    : widget.type == 20
                                        ? 'اسم الشركة'
                                        : widget.type == 14
                                            ? 'المندوب'
                                            : widget.type == 22 ||
                                                    widget.type == 23
                                                ? 'الموظف : بسام الزمر'
                                                : widget.type == 24
                                                    ? 'عبداللطيف مسئول الورشة'
                                                    : 'الاسم',
                            style: AppStyles.cairoNormalBold,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.type == 1
                                  ? 'الطلب : '
                                  : widget.type == 2
                                      ? 'الاقتراح : '
                                      : widget.type == 3
                                          ? 'الشكودى : '
                                          : widget.type == 4
                                              ? 'الدردشة : '
                                              : widget.type == 6
                                                  ? 'المندوب : '
                                                  : widget.type == 20
                                                      ? 'رقم الجوال : '
                                                      : 'العميل : ',
                              style: AppStyles.cairoNormalSecondaryBold,
                            ),
                            widget.type == 6
                                ? Padding(
                                    padding:
                                        const EdgeInsets.fromLTRB(5, 5, 5, 0.0),
                                    child: Text('محمد عبدالفتاح'),
                                  )
                                : widget.type == 7
                                    ? Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            5, 5, 5, 0.0),
                                        child: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                240,
                                            child: Text(
                                                'شركه أبناء زيد بن علي أبو حيد للنقليات')),
                                      )
                                    : widget.type == 22 ||
                                            widget.type == 23 ||
                                            widget.type == 24
                                        ? Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                5, 5, 5, 0.0),
                                            child: Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width -
                                                    240,
                                                child: Text(
                                                    'شركة الجري للنقليات مساهمة مقفلة')),
                                          )
                                        : Visibility(
                                            child: Text(''),
                                            visible: false,
                                          )
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        widget.type == 6
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : widget.type == 7
                                ? Row(
                                    children: [
                                      Text(
                                        'المندوب : ',
                                        style: AppStyles.cairoNormalSecondaryBold,
                                      ),
                                      Text('عبداللطيف مسئول الورشة')
                                    ],
                                  )
                                : widget.type == 22 ||
                                        widget.type == 23 ||
                                        widget.type == 24
                                    ? Row(
                                        children: [
                                          Text(
                                            'الحالة : ',
                                            style: AppStyles.cairoNormalSecondaryBold,
                                          ),
                                          Text(widget.type == 24
                                              ? 'لم تحدد'
                                              : 'مكتمل')
                                        ],
                                      )
                                    : Row(
                                        children: [
                                          widget.type == 20
                                              ? Text(
                                                  'السجل التجاري : ',
                                                  style: AppStyles.cairoNormal,
                                                )
                                              : widget.type == 14
                                                  ? Text(
                                                      'فترة الإتمان : ',
                                                      style:
                                                          AppStyles.cairoNormal,
                                                    )
                                                  : Text(
                                                      'الإجراء : ',
                                                      style:
                                                          AppStyles.cairoNormal,
                                                    ),
                                        ],
                                      ),
                        SizedBox(
                          height: 5,
                        ),
                        widget.type == 7
                            ? Row(
                                children: [
                                  Text(
                                    'فترة الأجل : ',
                                    style: AppStyles.cairoNormalSecondaryBold,
                                  ),
                                  Text('15')
                                ],
                              )
                            : widget.type == 23
                                ? Row(
                                    children: [
                                      Text(
                                        'عدد أيام التأخير : ',
                                        style: AppStyles.cairoNormalSecondaryBold,
                                      ),
                                      Text('22')
                                    ],
                                  )
                                : widget.type != 20
                                    ? Visibility(
                                        child: Text(''),
                                        visible: false,
                                      )
                                    : Row(
                                        children: [
                                          Text(
                                            'الرقم الضريبي : ',
                                            style: AppStyles.cairoNormal,
                                          ),
                                        ],
                                      ),
                      ],
                    ),
                  ),
                  widget.type == 20
                      ? Visibility(
                          child: Text(''),
                          visible: false,
                        )
                      : Padding(
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0.0),
                          child: Text(
                            '12/12/2023',
                            style: AppStyles.cairoNormalSecondary,
                          ),
                        ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              widget.type == 6
                  ? Visibility(
                      child: Text(''),
                      visible: false,
                    )
                  : widget.type == 20
                      ? Visibility(
                          child: Text(''),
                          visible: false,
                        )
                      : widget.type == 7
                          ? Visibility(
                              child: Text(''),
                              visible: false,
                            )
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                click[widget.index] == false
                                    ? Visibility(
                                        child: Text(''),
                                        visible: false,
                                      )
                                    : Container(
                                        color: Colors.grey,
                                        height: 1,
                                        width:
                                            MediaQuery.of(context).size.width,
                                      ),
                                click[widget.index] == false
                                    ? Visibility(
                                        child: Text(''),
                                        visible: false,
                                      )
                                    : Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Container(
                                              child: Text(widget.type == 14
                                                  ? 'ملاحظات الموظف'
                                                  : widget.type == 22 ||
                                                          widget.type == 23
                                                      ? 'ملاحظات المدير'
                                                      :widget.type == 24
                                                          ? 'متابعة تحصيل'
                                                          : 'الرد')),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          widget.type == 22 || widget.type == 23
                                              ? Text('ملاحظات الموظف'):
                                          widget.type == 24
                                              ? Text('السبب : اتصال')
                                              : Visibility(
                                                  child: Text(''),
                                                  visible: false,
                                                )
                                        ],
                                      ),
                                SizedBox(
                                  height:
                                      click[widget.index] == false ? 20 : 20,
                                ),
                                Center(
                                  child: InkWell(
                                    onTap: () {
                                      setState(() {
                                        click[widget.index] =
                                            !click[widget.index];
                                      });
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: AppColors.primary,
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(30, 5, 30, 5),
                                          child: Text(
                                            click[widget.index] == false
                                                ? widget.type == 14
                                                    ? 'ملاحظات الموظف'
                                                    : widget.type == 22 ||
                                                            widget.type == 23 ||
                                                widget.type == 24
                                                ? 'الملاحظات'
                                                        : 'الرد'
                                                : 'إخفاء',
                                            style: AppStyles.cairoNormalWhiteBold,
                                          )),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height:
                                      click[widget.index] == false ? 10 : 20,
                                ),
                              ],
                            )
            ]),
          )),
    );
  }
}
