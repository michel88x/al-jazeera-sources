import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ClientConfirmCardPage extends StatefulWidget {
  int index;
  int type;

  ClientConfirmCardPage(this.index, this.type);

  @override
  State<ClientConfirmCardPage> createState() => _ClientConfirmCardPage();
}

class _ClientConfirmCardPage extends State<ClientConfirmCardPage> {
  List<bool> click = [false, false, false, false, false, false];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
      child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [
                BoxShadow(color: Colors.grey, blurRadius: 5, spreadRadius: 1)
              ]),
          child: Padding(
            padding: EdgeInsets.fromLTRB(0, 10, 20, 20),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width - 200,
                          child: Text(
                            widget.type == 26 || widget.type == 27
                                ? 'شركة مصنع مقاييس الدقة للمعدات'
                                : 'اسم الشركة : ',
                            style: AppStyles.cairoNormalBold,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        widget.type == 26 || widget.type == 27
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(Icons.email_outlined,
                                      color: AppColors.secondary, size: 15),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    'Shazaaljanoub76@gmail.com',
                                    style: AppStyles.cairoNormal,
                                  ),
                                ],
                              ),
                        widget.type == 26 || widget.type == 27
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : SizedBox(
                                height: 5,
                              ),
                        widget.type == 26 || widget.type == 27
                            ? Visibility(
                                child: Text(''),
                                visible: false,
                              )
                            : Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(Icons.phone_android,
                                      color: AppColors.secondary, size: 15),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    '966559592030',
                                    style: AppStyles.cairoNormal,
                                  ),
                                ],
                              ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              widget.type == 26 || widget.type == 27
                                  ? 'المندوب : '
                                  : 'العنوان : ',
                              style: AppStyles.cairoTwelveSecondary,
                            ),
                            Text(
                              widget.type == 26 || widget.type == 27
                                  ? 'عبد اللطيف مسؤول الورشة'
                                  : 'السعودية / الرياض',
                              style: AppStyles.cairoNormal,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              widget.type == 26
                                  ? 'فترة السماح للأجل : '
                                  : widget.type == 27
                                      ? 'البنك : '
                                      : 'العمر : ',
                              style: AppStyles.cairoTwelveSecondary,
                            ),
                            Text(
                              widget.type == 27 ? 'مصرف الانماء' : '32',
                              style: AppStyles.cairoNormal,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              widget.type == 26 || widget.type == 27
                                  ? 'المبلغ : '
                                  : 'الجنس : ',
                              style: AppStyles.cairoTwelveSecondary,
                            ),
                            Text(
                              widget.type == 26 || widget.type == 27
                                  ? '45678'
                                  : 'ذكر',
                              style: AppStyles.cairoNormal,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              widget.type == 26 || widget.type == 27
                                  ? 'الحالة : '
                                  : 'الحالة الإجتماعية : ',
                              style: AppStyles.cairoTwelveSecondary,
                            ),
                            Text(
                              widget.type == 26 || widget.type == 27
                                  ? 'مكتمل'
                                  : 'أعزب',
                              style: AppStyles.cairoNormal,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width - 100,
                              child: Row(
                                children: [
                                  Text(
                                    widget.type == 26 || widget.type == 27
                                        ? 'تاريخ الاستكمال : '
                                        : 'نوع الاستبيان : ',
                                    style: AppStyles.cairoTwelveSecondary,
                                  ),
                                  Text(
                                    widget.type == 26 || widget.type == 27
                                        ? '12/12/2023'
                                        : '',
                                    style: AppStyles.cairoNormal,
                                  ),
                                ],
                              ),
                            ),
                            widget.type == 26 || widget.type == 27?   Icon(Icons.attach_file):Visibility(child: Text(''),visible: false,)
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              widget.type == 26 || widget.type == 27
                                  ? 'التاريخ : '
                                  : 'الاستبيان : ',
                              style: AppStyles.cairoTwelveSecondary,
                            ),
                            Text(
                              widget.type == 26 || widget.type == 27
                                  ? '12/12/2023'
                                  : '',
                              style: AppStyles.cairoNormal,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
            ]),
          )),
    );
  }
}
