import 'package:employee/screen/sales/widget/client_card.dart';
import 'package:employee/screen/sales/widget/order_card_page.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:employee/widget/app_bar_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ClientNotVerfyPage extends StatefulWidget {
  String title;
  int type;

  ClientNotVerfyPage(this.title, this.type);

  @override
  State<ClientNotVerfyPage> createState() => _ClientNotVerfyPageState();
}

class _ClientNotVerfyPageState extends State<ClientNotVerfyPage>
    with SingleTickerProviderStateMixin {
 
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
            backgroundColor: Colors.white,

            body:   Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top:90.0),
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return ClientCardPage(index, widget.type);
                    },
                    shrinkWrap: true,
                    itemCount: 10,
                  ),
                ),
                App_Bar(
                  title: widget.title,
                  LeadingIcon: Icons.keyboard_arrow_right,
                  LeadingOnClick: (){
                    Navigator.pop(context);

                  },

                ),

              ],
            ),);
  }
}
