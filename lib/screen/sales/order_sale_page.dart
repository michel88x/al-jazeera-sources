import 'package:employee/screen/sales/widget/order_card_page.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:employee/widget/app_bar_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class OrderSalePage extends StatefulWidget {
  String title;
  int type;

  OrderSalePage(this.title, this.type);

  @override
  State<OrderSalePage> createState() => _OrderSalePageState();
}

class _OrderSalePageState extends State<OrderSalePage> {
  List<bool> click = [false, false, false, false, false, false];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,

        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(top:90.0),
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return OrderCardPage(index, widget.type);
                },
                shrinkWrap: true,
                itemCount: click.length,
              ),
            ),
            App_Bar(
              title: widget.title,
              LeadingIcon: Icons.keyboard_arrow_right,
              LeadingOnClick: (){
                Navigator.pop(context);

              },

            ),

          ],
        ));
  }
}
