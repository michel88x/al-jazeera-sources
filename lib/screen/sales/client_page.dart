import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/screen/sales/widget/client_card.dart';
import 'package:employee/screen/sales/widget/order_card_page.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ClientPage extends StatefulWidget {
  String title;
  int type;

  ClientPage(this.title, this.type);

  @override
  State<ClientPage> createState() => _ClientPageState();
}

class _ClientPageState extends State<ClientPage>
    with SingleTickerProviderStateMixin {
  List<bool> click = [false, false, false, false, false, false];
  TabController? _tabController;

  final List<Tab> _tabBar = <Tab>[
    Tab(
        child: Text('العملاء الأساسين',
            textAlign: TextAlign.center,
            style: AppStyles.cairoTenSecondaryBold)),
    Tab(
        child: Text('العملاء المحتملين',
            textAlign: TextAlign.center,
            style: AppStyles.cairoTenSecondaryBold)),
    Tab(
        child: Text('العملاء بدون مندوبين',
            textAlign: TextAlign.center,
            style: AppStyles.cairoTenSecondaryBold)),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = new TabController(vsync: this, length: _tabBar.length);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
                bottom: PreferredSize(
                    preferredSize: Size(100, 60),
                    child: Container(
                        height: 60,
                        color: Colors.white,
                        child: TabBar(
                          indicatorColor: AppColors.secondary,
                          tabs: _tabBar,
                          onTap: (ind) {},
                          controller: _tabController,
                        ))),
                leading: InkWell(
                    child: Icon(
                      Icons.keyboard_arrow_right,
                      color: Colors.white,
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    }),
                title: Text(widget.title,
                    style: AppStyles.cairoNineteenWhiteBold),
                centerTitle: true,
                flexibleSpace: Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                    Colors.grey,
                    AppColors.primary.withOpacity(0.6),
                    AppColors.primary
                  ])),
                )),
            body: TabBarView(controller: _tabController, children: [
              ListView.builder(
                itemBuilder: (context, index) {
                  return ClientCardPage(index, 0);
                },
                shrinkWrap: true,
                itemCount: click.length,
              ),
              ListView.builder(
                itemBuilder: (context, index) {
                  return ClientCardPage(index, 1);
                },
                shrinkWrap: true,
                itemCount: click.length,
              ),
              ListView.builder(
                itemBuilder: (context, index) {
                  return ClientCardPage(index, 2);
                },
                shrinkWrap: true,
                itemCount: click.length,
              )
            ])));
  }
}
