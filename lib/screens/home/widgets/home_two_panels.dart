import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_images.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/screens/home/bloc/home_cubit.dart';
import 'package:employee/screens/home/helper/home_helper.dart';
import 'package:employee/screens/home/bloc/home_state.dart';
import 'package:employee/screens/home/repository/home_repository_impl.dart';
import 'package:employee/screens/home/widgets/home_list_item.dart';
import 'package:employee/screens/home/widgets/home_messages_list_item.dart';
import 'package:employee/widget/app_bar_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TwoPanels extends StatefulWidget {
  final AnimationController controller;

  const TwoPanels({Key? key, required this.controller}) : super(key: key);

  @override
  _TwoPanelsState createState() => _TwoPanelsState();
}

class _TwoPanelsState extends State<TwoPanels> with TickerProviderStateMixin {

  ///Controllers
  late TabController tabController = TabController(length: 3, vsync: this);
  late final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 2),
    vsync: this,
  )..addListener(() {});
  final RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints){
        return BlocProvider(
            create: (context) => HomeCubit(HomeRepositoryImpl()),
            child: Stack(
              children: <Widget>[
                Scaffold(
                  body: Stack(
                    children: [
                      BlocBuilder<HomeCubit, HomeState>(
                        builder: (context, state) {
                          if (state is HomeInitial) {
                            context.read<HomeCubit>().getHomeList(context);
                            return Stack(
                              children: const [
                                Center(
                                  child: CircularProgressIndicator(),
                                ),
                              ],
                            );
                          } else if (state is HomeSuccess) {
                            return SmartRefresher(
                                enablePullDown: true,
                                enablePullUp: true,
                                header: const WaterDropHeader(),
                                footer: CustomFooter(
                                  builder:
                                      (BuildContext context, LoadStatus? mode) {
                                    Widget body;
                                    if (mode == LoadStatus.idle) {
                                      body = const Text("");
                                    } else if (mode == LoadStatus.loading) {
                                      body = const CupertinoActivityIndicator();
                                    } else if (mode == LoadStatus.failed) {
                                      body = const Text("Load Failed!Click retry!");
                                    } else if (mode == LoadStatus.canLoading) {
                                      body = const Text("release to load more");
                                    } else {
                                      body = const Text("No more Data");
                                    }
                                    return SizedBox(
                                      height: 55.0,
                                      child: Center(child: body),
                                    );
                                  },
                                ),
                                controller: _refreshController,
                                onLoading: () async {
                                  // monitor network fetch

                                  await Future.delayed(
                                      const Duration(milliseconds: 1000));

                                  // if failed,use refreshFailed()
                                  _refreshController.loadComplete();
                                },
                                onRefresh: () async {
                                  // monitor network fetch

                                  context.read<HomeCubit>().getHomeList(context);

                                  await Future.delayed(
                                      const Duration(milliseconds: 1000));

                                  // if failed,use refreshFailed()
                                  _refreshController.refreshCompleted();
                                },
                                child: ListView(
                                  shrinkWrap: true,
                                  children: [
                                    Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              20, 110, 20, 10.0),
                                          child: Container(
                                            // height: MediaQuery.of(context).size.height,
                                            child: ListView.builder(
                                              physics:
                                              const NeverScrollableScrollPhysics(),
                                              shrinkWrap: true,
                                              itemCount: state.homeL.length,
                                              padding: EdgeInsets.zero,
                                              itemBuilder: (context, index) {
                                                return HomeListItem(index: index, model: state.homeL[index]);
                                              },
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width -
                                              50,
                                          child: Text('performance_indicator'.tr(),
                                              style: AppStyles.cairoNormalBold),
                                        ),
                                        Padding(
                                          padding:
                                          const EdgeInsets.fromLTRB(25, 10, 25, 10),
                                          child: Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                  BorderRadius.circular(15),
                                                  color: AppColors.lightGreen,
                                                  boxShadow: [
                                                    BoxShadow(
                                                        color: Colors.black
                                                            .withOpacity(0.4),
                                                        spreadRadius: 0,
                                                        blurRadius: 2)
                                                  ]),
                                              child: Padding(
                                                padding: const EdgeInsets.fromLTRB(
                                                    10, 10, 10, 20),
                                                child: Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                    children: [
                                                      Column(
                                                        mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                        children: [
                                                          SizedBox(
                                                            width: MediaQuery.of(
                                                                context)
                                                                .size
                                                                .width -
                                                                200,
                                                            child: Text(
                                                                state.itemPer.value,
                                                                style: AppStyles.cairoFifteenDarkBlackBold),
                                                          ),
                                                          SizedBox(
                                                            width: MediaQuery.of(
                                                                context)
                                                                .size
                                                                .width -
                                                                140,
                                                            child: RichText(
                                                              text: TextSpan(
                                                                  text:
                                                                  'performance_indicator'.tr(),
                                                                  style: AppStyles.cairoNormalBlack,
                                                                  children: <
                                                                      TextSpan>[
                                                                    TextSpan(
                                                                        text:
                                                                        ' (${"view_related_indicators".tr()}) ',
                                                                        style: AppStyles.cairoNormalPrimary)
                                                                  ]),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Container(
                                                        decoration: BoxDecoration(
                                                            shape: BoxShape.circle,
                                                            color: AppColors.lightGrey
                                                                .withOpacity(0.8)),
                                                        child: const Padding(
                                                            padding:
                                                            EdgeInsets.all(10),
                                                            child: Icon(Icons.send,
                                                                size: 20,
                                                                color: AppColors.primary)),
                                                      )
                                                    ]),
                                              )),
                                        ),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width -
                                              50,
                                          child: Text(
                                              'messages_sent_from_sys_to_employee'.tr(),
                                              style: AppStyles.cairoNormalBold),
                                        ),
                                        Padding(
                                          padding:
                                          const EdgeInsets.fromLTRB(25, 10, 25, 10),
                                          child: SizedBox(
                                              height: 120,
                                              child: ListView.builder(
                                                itemBuilder: (context, index) {
                                                  return HomeMessagesListItem(model: state.msgL[index]);
                                                },
                                                shrinkWrap: true,
                                                itemCount: state.msgL.length,
                                                scrollDirection: Axis.horizontal,
                                              )),
                                        ),
                                      ],
                                    ),
                                  ],
                                ));
                          } else {
                            return const CircularProgressIndicator();
                          }
                        },
                      ),
                      App_Bar(
                        title: "main_page".tr(),
                        LeadingIcon: Icons.menu,
                        LeadingOnClick: () {
                          if (z.isOpen!()) {
                            z.close;
                          } else {
                            z.toggle!();
                          }
                        },
                      )
                    ],
                  ),
                ),
              ],
            ));
      },
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    tabController.dispose();
    super.dispose();
  }
}