import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/model/home_model.dart';
import 'package:flutter/material.dart';

class HomeListItem extends StatelessWidget {

  final int index;
  final HomeModel model;
  const HomeListItem({Key? key, required this.index, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(
          5, 10, 5, 10),
      child: Container(
          decoration: BoxDecoration(
              borderRadius:
              BorderRadius.circular(
                  15),
              color: index != 2
                  ? Colors.white
                  : AppColors.red,
              boxShadow: [
                BoxShadow(
                    color: Colors.black
                        .withOpacity(
                        0.4),
                    spreadRadius: 0,
                    blurRadius: 2)
              ]),
          child: Padding(
            padding:
            const EdgeInsets.fromLTRB(
                10, 10, 10, 20),
            child: Row(
                mainAxisAlignment:
                MainAxisAlignment
                    .spaceBetween,
                crossAxisAlignment:
                CrossAxisAlignment
                    .center,
                children: [
                  Column(
                    mainAxisAlignment:
                    MainAxisAlignment
                        .start,
                    crossAxisAlignment:
                    CrossAxisAlignment
                        .start,
                    children: [
                      Container(
                        width: MediaQuery.of(
                            context)
                            .size
                            .width -
                            200,
                        child: Text(
                            model.title,
                            style: AppStyles.cairoFifteenDarkGreyBold),
                      ),
                      model.value == ''
                          ? const Visibility(
                        visible:
                        false,
                        child: Text(
                            ''),
                      )
                          : Row(
                        children: [
                          Text(
                              model.value,
                              style: AppStyles.cairoFifteen),
                        ],
                      ),
                    ],
                  ),
                  Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: index == 0
                            ? AppColors.lightGrey.withOpacity(0.6)
                            : index == 2
                            ? AppColors.lightGrey
                            : AppColors.lightOrange.withOpacity(0.4)),
                    child: Padding(
                        padding:
                        const EdgeInsets
                            .all(
                            10),
                        child: Icon(
                          Icons
                              .menu_open_outlined,
                          size: 20,
                          color: index ==
                              0
                              ? AppColors.primary
                              : index ==
                              2
                              ? Colors
                              .white
                              : AppColors.lightOrange,
                        )),
                  )
                ]),
          )),
    );
  }
}
