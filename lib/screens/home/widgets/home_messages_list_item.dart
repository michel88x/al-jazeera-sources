import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_images.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/model/home_model.dart';
import 'package:flutter/material.dart';

class HomeMessagesListItem extends StatelessWidget {
  final HomeModel model;

  const HomeMessagesListItem({Key? key,required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black.withOpacity(0.4),
                    spreadRadius: 0,
                    blurRadius: 2)
              ]),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width - 200,
                        child: Text(model.value,
                            style: AppStyles.cairoSeventeenDarkBlackBold),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width - 140,
                        child: Text(model.title, style: AppStyles.cairoNormal),
                      ),
                    ],
                  ),
                  Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColors.lightGrey.withOpacity(0.3)),
                    child: Padding(
                        padding: model.icon != Icons.add.toString()
                            ? const EdgeInsets.all(10)
                            : const EdgeInsets.all(5),
                        child: model.icon != Icons.add.toString()
                            ? const Icon(Icons.menu_open_outlined,
                                size: 20, color: AppColors.primary)
                            : Image.asset(
                                AppImages.whats,
                                color: AppColors.primary,
                                height: 30,
                                width: 30,
                              )),
                  )
                ]),
          )),
    );
  }
}
