import 'package:employee/core/app/app_styles.dart';
import 'package:flutter/material.dart';

class HomeDrawerItem extends StatelessWidget {

  final VoidCallback? onPressed;
  final IconData? icon;
  final String? title;

  const HomeDrawerItem({Key? key, this.onPressed, this.icon, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Row(
        children: [
          const SizedBox(
            width: 10,
          ),
          Icon(icon!, color: Colors.white, size: 13),
          const SizedBox(
            width: 2,
          ),
          Text(
            title!,
            style: AppStyles.cairoNineteenWhiteBold,
          ),
        ],
      ),
    );
  }
}
