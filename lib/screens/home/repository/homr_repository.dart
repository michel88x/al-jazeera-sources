import 'package:employee/screens/home/models/home_response.dart';

abstract class HomeRepository {
  Future<HomeResponse> getHomeList(String token);
}
