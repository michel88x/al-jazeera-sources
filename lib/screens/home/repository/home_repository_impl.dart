import 'package:employee/screens/home/models/home_response.dart';
import 'package:employee/screens/home/repository/homr_repository.dart';
import 'package:employee/screens/home/services/home_service.dart';

class HomeRepositoryImpl implements HomeRepository {
  HomeService service = HomeService();

  @override
  Future<HomeResponse> getHomeList(String token) {
    return service.getHomeList(token);
  }
}