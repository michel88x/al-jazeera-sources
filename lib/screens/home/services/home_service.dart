import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:employee/screens/human_resources_screen/models/drop_down_list_response.dart';
import 'package:employee/screens/human_resources_screen/models/general_response.dart';
import 'package:employee/screens/home/models/home_response.dart';
import 'package:employee/screens/human_resources_screen/models/employee_account_response.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:http/http.dart' as http;

class HomeService {
  Dio _dio = Dio();

  Future<HomeResponse> getHomeList(String token) async {
    String ut = URL;
    print(ut);
    http.Response response = await http.get(Uri.parse(URL), headers: {
      'Authorization': 'Bearer ' + token,
      'mobile': '1',
      'Accept': 'application/json'
    });
    if (response.statusCode == 200) {
      return HomeResponse.fromJson(jsonDecode(response.body));
    } else {
      return HomeResponse.withError(jsonDecode(response.body));
    }
  }
}
