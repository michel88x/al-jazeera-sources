import 'package:employee/model/home_model.dart';

class HomeResponse {
  final bool success;
  final List<HomeModel> homeList;

  final String message;

  HomeResponse(this.message, this.success, this.homeList);

  HomeResponse.fromJson(Map<String, dynamic> json)
      : message = json['message'],
        homeList = json["data"] != null
            ? List<HomeModel>.from(
                json["data"].map((x) => HomeModel.fromJson(x)))
            : [],
        success = json['success'];

  HomeResponse.withError(Map<String, dynamic> json)
      : message = json['message'],
        homeList = [],
        success = false;
}
