import 'package:employee/model/home_model.dart';
import 'package:employee/screens/home/models/home_response.dart';
import 'package:equatable/equatable.dart';

abstract class HomeState extends Equatable {
  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class HomeLoaded extends HomeState {}

class HomeSuccess extends HomeState {
  final List<HomeModel> homeL;
  final List<HomeModel> msgL;
  final HomeModel itemPer;

  HomeSuccess({required this.homeL,required this.msgL,required this.itemPer});
}

class HomeError extends HomeState {
  final String message;

  HomeError({required this.message});
}
