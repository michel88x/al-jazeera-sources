import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/utils/shared_pref.dart';
import 'package:employee/model/home_model.dart';
import 'package:employee/screens/home/repository/home_repository_impl.dart';
import 'package:employee/screens/home/repository/homr_repository.dart';
import 'package:employee/screens/home/models/home_response.dart';
import 'package:employee/screens/home/bloc/home_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeCubit extends Cubit<HomeState> {
  final HomeRepositoryImpl _homeRepository;

  HomeCubit(this._homeRepository) : super(HomeInitial());
  List<HomeModel> homeList = [];
  List<HomeModel> homeListAll = [];
  List<HomeModel> msgList = [];

  HomeModel? performancevalue;

  static HomeCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  Future<void> getHomeList(BuildContext context) async {
    try {
      emit(HomeInitial());
      homeList.clear();
      homeListAll.clear();
      msgList.clear();
      performancevalue=null;
      String? token = await SharedPref.getString(SharedPref.token);
      HomeResponse data = await _homeRepository.getHomeList(token!);
      if (data.success == true) {
        homeList.clear();
        homeListAll.clear();
        msgList.clear();
        performancevalue=null;
        homeListAll.addAll(data.homeList);
        for (int i = 0; i < homeListAll.length; i++) {
          if (homeListAll[i].key == 'performance_value') {
            performancevalue = homeListAll[i];
          }
        }
        for (int i = 0; i < homeListAll.length; i++) {
          if (homeListAll[i].key == 'Email' ||
              homeListAll[i].key == 'Sms' ||
              homeListAll[i].key == 'Whatsapp') {
            msgList.add(homeListAll[i]);
          } else {
            homeList.add(homeListAll[i]);
          }
        }
        homeList.add(HomeModel(
            title: 'add_daily_report'.tr(),
            icon: 'Icons.add',
            value: '',
            key: 'addR'));
        homeList.add(HomeModel(
            title: 'create_mission'.tr(), icon: 'Icons.add', value: '', key: 'addM'));

        // emit(HrSuccess(data: data));
        emit(HomeSuccess(
            homeL: homeList, msgL: msgList, itemPer: performancevalue!));
      }
    } on Exception {
      emit(HomeError(
          message: "can_not_fetch_list".tr()));
    }
  }
}
