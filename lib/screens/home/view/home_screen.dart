import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/screens/human_resources_screen/view/human_resources_screen.dart';
import 'package:employee/screens/home/helper/home_helper.dart';
import 'package:employee/screens/home/widgets/home_body.dart';
import 'package:employee/screens/home/widgets/home_drawer_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/config.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomePage createState() => _HomePage();
}

class _HomePage extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        return false;
      },
      child: ZoomDrawer(
        controller: z,
        borderRadius: 24,
        style: DrawerStyle.defaultStyle,
        isRtl: true,
        showShadow: true,
        openCurve: Curves.fastOutSlowIn,
        slideWidth: MediaQuery.of(context).size.width * 0.65,
        duration: const Duration(milliseconds: 500),
        menuBackgroundColor: AppColors.primary,
        mainScreen: HomeBody(),
        menuScreen: Theme(
          data: ThemeData.dark(),
          child: Scaffold(
            backgroundColor: AppColors.primary,
            body: Padding(
              padding: const EdgeInsets.only(left: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 100, 20, 0.0),
                    child: InkWell(
                        onTap: () {
                          z.close?.call();
                        },
                        child: const Icon(Icons.cancel)),
                  ),
                  const SizedBox(
                    height: 100,
                  ),
                  HomeDrawerItem(
                    title: "human_resources".tr(),
                    icon: Icons.group_work,
                    onPressed: (){
                      final navigator = Navigator.of(
                        context,
                      );
                      z.close?.call()?.then(
                            (value) => navigator.push(
                          MaterialPageRoute(
                            builder: (_) => HumanResourcesScreen(),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
