import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/base_widgets/loader.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:employee/core/utils/shared_pref.dart';
import 'package:employee/screens/home/view/home_screen.dart';
import 'package:employee/screens/auth/login_screen/view/login_page.dart';
import 'package:employee/screens/general/privacy_screen/view/privacy_screen.dart';
import 'package:employee/screens/general/splash_screen/helper/splash_helper.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: FutureBuilder<String?>(
          future: getToken(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data == null || snapshot.data!.isEmpty) {
                return IntroductionScreen(
                  pages: SplashHelper.getSplashPages(),
                  onDone: () async {
                    _goToNextPage();
                  },
                  onSkip: () async {
                    _goToNextPage();
                  },
                  next: Text(
                    "next".tr(),
                    style: AppStyles.cairoNineteenPrimaryBold,
                  ),
                  done: Text(
                    "next".tr(),
                    style: AppStyles.cairoNineteenSecondaryBold,
                  ),
                  skip: Text(
                    "skip".tr(),
                    style: AppStyles.cairoNineteenPrimaryBold,
                  ),
                  showSkipButton: true,
                  dotsDecorator:
                      const DotsDecorator(activeColor: AppColors.primary),
                );
              }
              else {
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  gotoReplace(context, HomeScreen());
                });
                return Container();
              }
            } else {
              return const Center(
                child: Loader(),
              );
            }
          }),
    );
  }

  _goToNextPage() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      goto(context, LoginScreen());
    });
  }
}
