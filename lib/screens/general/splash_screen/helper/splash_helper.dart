import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_images.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:lottie/lottie.dart';

class SplashHelper{
  static List<PageViewModel> getSplashPages(){
    return <PageViewModel>[
      PageViewModel(
          titleWidget: Text(
            "splash_title_1".tr(),
            style: AppStyles.cairoTwentySevenBlackBold,
          ),
          bodyWidget: Text(
            "splash_desc_1".tr(),
            textAlign: TextAlign.center,
            style: AppStyles.cairoNineteen,
          ),
          image: SizedBox(
            height: 200,
            child: Lottie.asset(AppImages.spl),
          )
      ),
      PageViewModel(
          titleWidget: Text(
            "splash_title_2".tr(),
            style: AppStyles.cairoTwentySevenBlackBold,
          ),
          bodyWidget: Text(
            "splash_desc_2".tr(),
            textAlign: TextAlign.center,
            style: AppStyles.cairoNineteen,
          ),
          image: SizedBox(
            height: 200,
            child: Lottie.asset(AppImages.spla),
          )
      ),
      PageViewModel(
          titleWidget: Text(
            "splash_title_3".tr(),
            style: AppStyles.cairoTwentySevenBlackBold,
          ),
          bodyWidget: Text(
            "splash_desc_3".tr(),
            textAlign: TextAlign.center,
            style: AppStyles.cairoNineteen,
          ),
          image: SizedBox(
            height: 200,
            child: Lottie.asset(AppImages.splt),
          )
      )
    ];
  }
}