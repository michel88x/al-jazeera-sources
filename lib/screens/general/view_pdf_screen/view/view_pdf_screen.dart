import 'package:employee/core/constants/constant.dart';
import 'package:employee/widget/app_bar_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';

class ViewPDFScreen extends StatefulWidget {
  String title;
  String url;

  ViewPDFScreen(this.title, this.url);

  @override
  State<ViewPDFScreen> createState() => _ViewPDFScreenState();
}

class _ViewPDFScreenState extends State<ViewPDFScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 90.0),
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: PDF(
                  onError: (error) {
                    print(error.toString());
                  },
                  onPageError: (page, error) {
                    print('$page: ${error.toString()}');
                  },
                ).cachedFromUrl(widget.url),
              ),
            ),
            App_Bar(
              title: widget.title,
              LeadingIcon: Icons.keyboard_arrow_right,
              LeadingOnClick: () {
                Navigator.pop(context);
              },
            ),
          ],
        ));
  }
}
