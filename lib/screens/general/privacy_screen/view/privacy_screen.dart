import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:employee/core/utils/shared_pref.dart';
import 'package:employee/screens/home/view/home_screen.dart';
import 'package:employee/widget/app_bar_page.dart';
import 'package:flutter/material.dart';

class PrivacyScreen extends StatefulWidget {
  @override
  State<PrivacyScreen> createState() => _PrivacyScreenState();
}

class _PrivacyScreenState extends State<PrivacyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            leading: const Visibility(visible: false, child: Text('')),
            title: Text('privacy_policy'.tr(),
                style:AppStyles.cairoNineteenWhiteBold),
            centerTitle: true,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient:
                      LinearGradient(colors: [Colors.grey, AppColors.primary.withOpacity(0.6), AppColors.primary])),
            )),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            App_Bar(
              title: 'privacy_policy'.tr(),
            ),
            Expanded(
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        'privacy_policy_text'.tr(),
                        style: AppStyles.cairoTwelve,)
                    ],
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () async {
                await SharedPref.saveString(SharedPref.pri, "1");
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  goto(context, HomeScreen());
                });
              },
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                    color: AppColors.secondary,
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                      child: Text('agree'.tr(),
                          style: AppStyles.cairoFifteenWhiteBold,
                          textAlign: TextAlign.center),
                    )),
              ),
            )
          ],
        )
    );
  }
}
