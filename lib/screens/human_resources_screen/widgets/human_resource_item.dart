import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/screens/general/view_pdf_screen/view/view_pdf_screen.dart';
import 'package:employee/screens/human_resources_screen/models/human_resource_object.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/view/employees_accounts_page.dart';
import 'package:flutter/material.dart';

class HumanResourceItem extends StatelessWidget {
  
  final int index;
  final HumanResourceObject item;
  const HumanResourceItem({Key? key, required this.item, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (index == 0) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return EmployeesAccountsPage(item.title!,0);
          }));
        } else if (index == 1) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return EmployeesAccountsPage(item.title!,1);
          }));
        }
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.4),
                      spreadRadius: 0,
                      blurRadius: 2)
                ]),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: 20,
                    child: Icon(item.icon, color: AppColors.primary),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 15, 8, 0),
                    child: Text(
                      item.title!,
                      textAlign: TextAlign.center,
                      style: AppStyles.cairoNineBold,
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
