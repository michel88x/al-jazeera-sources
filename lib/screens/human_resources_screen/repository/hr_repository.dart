import 'package:dio/dio.dart';
import 'package:employee/screens/human_resources_screen/models/drop_down_list_response.dart';
import 'package:employee/screens/human_resources_screen/models/general_response.dart';
import 'package:employee/screens/home/models/home_response.dart';
import 'package:employee/screens/human_resources_screen/models/employee_account_response.dart';
import 'package:employee/screens/human_resources_screen/models/report_month_response.dart';

abstract class HrRepository {
  Future<EmployeeAccountResponse> getEmpAccList(String token, String page);

  Future<EmployeeAccountResponse> getEmpAccExitList(String token, String page);

  Future<DropDownListResponse> getDropDownList(String token);

  Future<GeneralResponse> deleteEmployee(String token, int id);

  Future<GeneralResponse> reemployeeEmployee(String token, int id);

  Future<GeneralResponse> addEmployee(String token, FormData body);

  Future<GeneralResponse> updateEmployee(String token, FormData body, int id);

  Future<GeneralResponse> dismissalEmployee(String token, int id);
}
