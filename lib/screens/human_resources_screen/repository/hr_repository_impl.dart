import 'package:dio/dio.dart';
import 'package:employee/screens/human_resources_screen/models/drop_down_list_response.dart';
import 'package:employee/screens/human_resources_screen/models/general_response.dart';
import 'package:employee/screens/human_resources_screen/models/employee_account_response.dart';
import 'package:employee/screens/human_resources_screen/repository/hr_repository.dart';
import 'package:employee/screens/human_resources_screen/services/hr_service.dart';

class HrRepositoryImpl implements HrRepository {
  HrService service = HrService();

  @override
  Future<EmployeeAccountResponse> getEmpAccList(
    String token,
    String page, {
    String? startDate,
    String? endDate,
    int? accountType,
    String? workDirection,
    String? name,
    int? country,
    int? dep,
    int? dep2,
    int? directManager,
    String? deletionFilter,
  }) {
    return service.getEmpAccList(token, page,
        startDate: startDate,
        endDate: endDate,
        accountType: accountType,
        workDirection: workDirection,
        country: country,
        name: name,
        dep: dep,
        dep2: dep2,
        directManager: directManager,
        deletionFilter: deletionFilter);
  }

  @override
  Future<EmployeeAccountResponse> getEmpAccExitList(
    String token,
    String page, {
    String? startDate,
    String? endDate,
    int? accountType,
    String? workDirection,
    String? name,
    int? country,
    int? dep,
    int? dep2,
    int? directManager,
    String? deletionFilter,
  }) {
    return service.getEmpAccExitList(token, page,
        startDate: startDate,
        endDate: endDate,
        accountType: accountType,
        workDirection: workDirection,
        country: country,
        name: name,
        dep: dep,
        dep2: dep2,
        directManager: directManager,
        deletionFilter: deletionFilter);
  }

  @override
  Future<GeneralResponse> deleteEmployee(String token, int id) {
    return service.deleteEmployee(token, id);
  }

  @override
  Future<GeneralResponse> reemployeeEmployee(String token, int id) {
    return service.reemploymentEmployee(token, id);
  }

  @override
  Future<GeneralResponse> addEmployee(String token, FormData body) {
    return service.addEmployee(token, body);
  }

  @override
  Future<GeneralResponse> updateEmployee(String token, FormData body, int id) {
    return service.updateEmployee(token, body, id);
  }

  @override
  Future<GeneralResponse> dismissalEmployee(String token, int id) {
    return service.dismissalEmployee(token, id);
  }

  @override
  Future<DropDownListResponse> getDropDownList(String token) {
    return service.getDropDownList(token);
  }
}
