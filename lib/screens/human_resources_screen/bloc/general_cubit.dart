import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:employee/repository/general_repository.dart';
import 'package:employee/screens/human_resources_screen/bloc/general_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GeneralCubit extends Cubit<GeneralState> {
  final GeneralRepository _genRepository;

  GeneralCubit(this._genRepository) : super(GeneralInitial());

  int page = 1;

  static GeneralCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  Future<dynamic> getHrListGeneral(BuildContext context, String url) async {
    try {
      emit(GeneralInitial());
      page = 1;
      SharedPreferences sharedPrefs = await SharedPreferences.getInstance();

      String? token = sharedPrefs.getString('token');
      Map<String, dynamic> data =
          await _genRepository.getHrListGeneral(token!, url);
      if (data['success'] == true || data['data'].length>0) {

        // emit(HrSuccess(data: data));
        emit(GeneralLoaded(data: data));
      }
    } on Exception {
      emit(GeneralError(
          message: "can_not_fetch_list".tr()));
    }
  }
}
