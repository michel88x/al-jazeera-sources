import 'package:employee/model/home_model.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employee_account_model.dart';
import 'package:employee/screens/human_resources_screen/models/drop_down_list_response.dart';
import 'package:employee/screens/human_resources_screen/models/report_month_response.dart';
import 'package:equatable/equatable.dart';

abstract class HrState extends Equatable {
  @override
  List<Object> get props => [];
}

class HrInitial extends HrState {}

class HrAdd extends HrState {
  DropDownListResponse data;
  HrAdd({required this.data});
}

class HrLoaded extends HrState {
  final List<EmployeeAccountModel> data;

  HrLoaded({required this.data});
}

class HrLoadedDropDown extends HrState {
  final DropDownListResponse data;

  HrLoadedDropDown({required this.data});
}

class HrLoadedMore extends HrState {
  final List<EmployeeAccountModel> data;

  HrLoadedMore({required this.data});
}

class HrDelete extends HrState {
  final String message;
  final List<EmployeeAccountModel> data;

  HrDelete({required this.message, required this.data});
}

class HrError extends HrState {
  final String message;

  HrError({required this.message});
}
