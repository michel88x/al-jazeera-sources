import 'package:equatable/equatable.dart';

abstract class GeneralState extends Equatable {
  @override
  List<Object> get props => [];
}

class GeneralInitial extends GeneralState {}

class GeneralLoaded extends GeneralState {
  final Map<String, dynamic> data;

  GeneralLoaded({required this.data});
}

class GeneralError extends GeneralState {
  final String message;

  GeneralError({required this.message});
}
