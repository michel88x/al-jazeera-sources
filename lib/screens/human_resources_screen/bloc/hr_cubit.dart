import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:employee/model/home_model.dart';
import 'package:employee/model/hr_model/company_model.dart';
import 'package:employee/model/hr_model/department_model.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employee_account_model.dart';
import 'package:employee/model/hr_model/report_moth_model.dart';
import 'package:employee/model/hr_model/user_model.dart';
import 'package:employee/screens/human_resources_screen/models/drop_down_list_response.dart';
import 'package:employee/screens/human_resources_screen/models/general_response.dart';
import 'package:employee/screens/human_resources_screen/models/employee_account_response.dart';
import 'package:employee/screens/human_resources_screen/bloc/hr_state.dart';
import 'package:employee/screens/human_resources_screen/repository/hr_repository_impl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HrCubit extends Cubit<HrState> {
  final HrRepositoryImpl _hrRepository;

  HrCubit(this._hrRepository) : super(HrInitial());

  int page = 1;
  CompanyModel? adminItem;

  UserModel? userItem;
  CompanyModel? delegateItem;
  DropDownListResponse? res;
  CompanyModel? comItem;

  DepartmentModel? depItem;
  DepartmentModel? secItem;
  List<String> actList = ['active'.tr(), 'disabled'.tr()];
  String? item;
  List<EmployeeAccountModel> empAccList = [];
  List<HomeModel> empDetPage = [];
  List<ReportMonthModel> reportMList = [];

  List<ReportMonthModel> reportDList = [];

  //function got return object from this cubit
  static HrCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  Future<void> getEmpAccList(
    BuildContext context, {
    String? startDate,
    String? endDate,
    int? accountType,
    String? workDirection,
    String? name,
    int? country,
    int? dep,
    int? dep2,
    int? directManager,
    String? deletionFilter,
  }) async {
    try {
      emit(HrInitial());
      page = 1;
      empAccList.clear();
      SharedPreferences sharedPrefs = await SharedPreferences.getInstance();

      String? token = sharedPrefs.getString('token');
      EmployeeAccountResponse data = await _hrRepository.getEmpAccList(
        token!,
        page.toString(),
        startDate: startDate,
        endDate: endDate,
        accountType: accountType,
        workDirection: workDirection,
        country: country,
        name: name,
        dep: dep,
        dep2: dep2,
        directManager: directManager,
        deletionFilter: deletionFilter,
      );
      if (data.success == true) {
        print("Access token: $token ${data.empAccList.length}");
        empAccList = data.empAccList;
        // emit(HrSuccess(data: data));
        emit(HrLoaded(data: empAccList));
      }
    } on Exception {
      emit(HrError(message: "can_not_fetch_list".tr()));
    }
  }

  Future<void> addEmployee(BuildContext context, FormData body) async {
    print("Hereeee");
    try {
      SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
      String? token = sharedPrefs.getString('token');
      String x = "";
      emit(HrAdd(data: res!));

      GeneralResponse data = await _hrRepository.addEmployee(token!, body);
      if (data.status == true) {
        getEmpAccList(context).then((value) {
          Navigator.pop(context, true);
          emit(HrInitial());
        });
      } else {
        emit(HrLoadedDropDown(data: res!));

        Fluttertoast.showToast(
            msg: data.message,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    } catch (e) {
      emit(HrLoadedDropDown(data: res!));
    }
  }

  Future<void> updateEmployee(
      BuildContext context, FormData body, int id) async {
    try {
      SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
      String? token = sharedPrefs.getString('token');
      String x = "";
      emit(HrAdd(data: res!));

      GeneralResponse data =
          await _hrRepository.updateEmployee(token!, body, id);
      if (data.status == true) {
        getEmpAccList(context).then((value) {
          Navigator.pop(context, true);
          emit(HrInitial());
        });
      } else {
        emit(HrLoadedDropDown(data: res!));

        // Fluttertoast.showToast(
        //     msg: data.message,
        //     toastLength: Toast.LENGTH_SHORT,
        //     gravity: ToastGravity.BOTTOM,
        //     timeInSecForIosWeb: 1,
        //     backgroundColor: Colors.red,
        //     textColor: Colors.white,
        //     fontSize: 16.0);
      }
    } catch (e) {
      emit(HrLoadedDropDown(data: res!));
    }
  }

  Future<void> GetAllListUser(
      BuildContext context, int type, EmployeeAccountModel items) async {
    try {
      emit(HrInitial());

      SharedPreferences sharedPrefs = await SharedPreferences.getInstance();

      String? token = sharedPrefs.getString('token');
      DropDownListResponse data = await _hrRepository.getDropDownList(token!);
      if (data.success == true) {
        res = data;
        if (type == 0) {
          adminItem = data.isAdminList[0];
          userItem = data.userssList[0];
          comItem = data.companyList[0];

          depItem = data.departmentList[0];
          secItem = data.departmentList[0];
          item = actList[0];
        } else {
          for (int i = 0; i < data.isAdminList.length; i++) {
            if (data.isAdminList[i].id.toString() == items.isAdmin.toString()) {
              adminItem = data.isAdminList[i];
            }
          }

          for (int i = 0; i < data.companyList.length; i++) {
            if (data.companyList[i].name
                .toString()
                .contains(items.activity.toString(), 0)) {
              comItem = data.companyList[i];
            }
          }

          for (int i = 0; i < data.departmentList.length; i++) {
            if (data.departmentList[i].id == items.departmentId) {
              depItem = data.departmentList[i];
            }
          }
          for (int i = 0; i < data.departmentList.length; i++) {
            if (data.departmentList[i].id == items.departmentId2) {
              secItem = data.departmentList[i];
            }
          }
          if (items.statusId == 1) {
            item = actList[0];
          } else {
            item = actList[1];
          }
        }
        // emit(HrSuccess(data: data));
        emit(HrLoadedDropDown(data: data));
      } else {
/*
          Fluttertoast.showToast(
              msg: "الرجاء التأكد من صحة المعلومات المدخلة",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);*/
      }
    } on Exception {
      emit(HrError(message: "can_not_fetch_list".tr()));
    }
  }

  Future<void> getEmpAccListMore(BuildContext context, int length,
      {String? startDate,
      String? endDate,
      int? accountType,
      String? workDirection,
      String? name,
      int? country,
      int? dep,
      int? dep2,
      int? directManager,
      String? deletionFilter}) async {
    try {
      //emit(HrLoadedMore(data: empAccList));
      emit(HrLoaded(data: empAccList));
      SharedPreferences sharedPrefs = await SharedPreferences.getInstance();

      String? token = sharedPrefs.getString('token');
      page++;
      EmployeeAccountResponse data = await _hrRepository.getEmpAccList(
          token!, page.toString(),
          startDate: startDate,
          endDate: endDate,
          accountType: accountType,
          workDirection: workDirection,
          name: name,
          country: country,
          dep: dep,
          dep2: dep2,
          directManager: directManager,
          deletionFilter: deletionFilter);
      if (data.success == true) {
        // emit(HrSuccess(data: data));
        empAccList.addAll(data.empAccList);
        emit(HrLoaded(data: empAccList));
      } else {
/*
          Fluttertoast.showToast(
              msg: "الرجاء التأكد من صحة المعلومات المدخلة",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);*/
      }
    } on Exception {
      emit(HrError(message: "can_not_fetch_list".tr()));
    }
  }

  Future<void> getEmpAccExitList(
    BuildContext context, {
    String? startDate,
    String? endDate,
    int? accountType,
    String? workDirection,
    String? name,
    int? country,
    int? dep,
    int? dep2,
    int? directManager,
    String? deletionFilter,
  }) async {
    try {
      SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
      emit(HrInitial());
      page = 1;
      empAccList.clear();
      String? token = sharedPrefs.getString('token');
      EmployeeAccountResponse data = await _hrRepository.getEmpAccExitList(
        token!,
        page.toString(),
        startDate: startDate,
        endDate: endDate,
        accountType: accountType,
        workDirection: workDirection,
        country: country,
        name: name,
        dep: dep,
        dep2: dep2,
        directManager: directManager,
        deletionFilter: deletionFilter,
      );
      if (data.success == true) {
        // emit(HrSuccess(data: data));
        empAccList = data.empAccList;

        emit(HrLoaded(data: empAccList));
      } else {
/*
          Fluttertoast.showToast(
              msg: "الرجاء التأكد من صحة المعلومات المدخلة",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);*/
      }
    } on Exception {
      emit(HrError(message: "can_not_fetch_list".tr()));
    }
  }

  Future<void> getEmpAccExitListMore(BuildContext context, int length,
      {String? startDate,
      String? endDate,
      int? accountType,
      String? workDirection,
      String? name,
      int? country,
      int? dep,
      int? dep2,
      int? directManager,
      String? deletionFilter}) async {
    try {
      SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
      emit(HrLoaded(data: empAccList));
      page++;

      String? token = sharedPrefs.getString('token');
      EmployeeAccountResponse data = await _hrRepository.getEmpAccExitList(
          token!, page.toString(),
          startDate: startDate,
          endDate: endDate,
          accountType: accountType,
          workDirection: workDirection,
          name: name,
          country: country,
          dep: dep,
          dep2: dep2,
          directManager: directManager,
          deletionFilter: deletionFilter);
      if (data.success == true) {
        // emit(HrSuccess(data: data));
        empAccList.addAll(data.empAccList);

        emit(HrLoaded(data: empAccList));
      } else {
/*
          Fluttertoast.showToast(
              msg: "الرجاء التأكد من صحة المعلومات المدخلة",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);*/
      }
    } on Exception {
      emit(HrError(message: "can_not_fetch_list".tr()));
    }
  }

  Future<void> deleteEmployee(BuildContext context, int id) async {
    try {
      SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
      String? token = sharedPrefs.getString('token');

      emit(HrDelete(message: '', data: empAccList));

      GeneralResponse data = await _hrRepository.deleteEmployee(token!, id);
      if (data.status == true) {
        getEmpAccList(context);
      } else {
        emit(HrLoaded(data: empAccList));

        Fluttertoast.showToast(
            msg: "element_already_deleted".tr(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    } on Exception {
      emit(HrError(message: "can_not_fetch_list".tr()));
    }
  }

  Future<void> reemployeeEmployee(BuildContext context, int id) async {
    try {
      SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
      String? token = sharedPrefs.getString('token');

      emit(HrDelete(message: '', data: empAccList));

      GeneralResponse data = await _hrRepository.reemployeeEmployee(token!, id);
      if (data.status == true) {
        getEmpAccExitList(context);
      } else {
        emit(HrLoaded(data: empAccList));

        Fluttertoast.showToast(
            msg: "employee_already_reemoloyeed".tr(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    } on Exception {
      emit(HrError(message: "can_not_fetch_list".tr()));
    }
  }

  Future<void> dismissalEmployee(BuildContext context, int id) async {
    try {
      SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
      String? token = sharedPrefs.getString('token');

      emit(HrDelete(message: '', data: empAccList));

      GeneralResponse data = await _hrRepository.dismissalEmployee(token!, id);
      if (data.status == true) {
        getEmpAccList(context);
        // Fluttertoast.showToast(
        //     msg: "fired_successfully".tr(),
        //     toastLength: Toast.LENGTH_SHORT,
        //     gravity: ToastGravity.BOTTOM,
        //     timeInSecForIosWeb: 1,
        //     backgroundColor: Colors.green,
        //     textColor: Colors.white,
        //     fontSize: 16.0);
      } else {
        emit(HrLoaded(data: empAccList));

        Fluttertoast.showToast(
            msg: "already_fired".tr(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    } on Exception {
      emit(HrError(message: "can_not_fetch_list".tr()));
    }
  }
}
