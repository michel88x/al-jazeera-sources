import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:employee/core/network/urls.dart';
import 'package:employee/screens/human_resources_screen/models/employee_account_response.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

class GeneralService {
  Dio _dio = Dio();

  Future<Map<String, dynamic>> getHrListGeneral(String token, String url) async {
    http.Response response = await http.get(Uri.parse(url), headers: {
      'Authorization': 'Bearer ' + token,
      'mobile': '1',
      'Accept': 'application/json'
    });
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return jsonDecode(response.body);
    }
  }
}
