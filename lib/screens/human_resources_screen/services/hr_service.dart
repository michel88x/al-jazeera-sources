import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/network/urls.dart';
import 'package:employee/screens/human_resources_screen/models/drop_down_list_response.dart';
import 'package:employee/screens/human_resources_screen/models/general_response.dart';
import 'package:employee/screens/human_resources_screen/models/employee_account_response.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;


class HrService {
  Dio _dio = Dio();

  Future<EmployeeAccountResponse> getEmpAccList(
    String token,
    String page, {
    String? startDate,
    String? endDate,
    int? accountType,
    String? workDirection,
    String? name,
    int? country,
    int? dep,
    int? dep2,
    int? directManager,
    String? deletionFilter,
  }) async {
    http.Response response;
    if ((startDate == "" || startDate == null) &&
        (endDate == "" || endDate == null) &&
        (accountType == 0 || accountType == null) &&
        (workDirection == "" || workDirection == null) &&
        (name == "please_select".tr() || name == "" || name == null) &&
        (country == null || country == -1) &&
        (dep == 0 || dep == null) &&
        (dep2 == 0 || dep2 == null) &&
        (directManager == 0 || directManager == null) &&
        (deletionFilter == "please_select".tr() || deletionFilter == null)) {
      response = await http
          .get(Uri.parse('${Urls.employeesAccounts}?page=$page'), headers: {
        'Authorization': 'Bearer $token',
        'mobile': '1',
        'Accept': 'application/json'
      });
    } else {
      String modStartDate =
          (startDate != null && startDate != "") ? startDate : "false";
      String modEndDate =
          (endDate != null && endDate != "") ? endDate : "false";
      String modAccountType =
          (accountType != null && accountType != 0) ? "$accountType" : "false";
      String modWorkDirection =
          (workDirection != null && workDirection != "" && workDirection != "0")
              ? workDirection
              : "false";
      String modName =
          (name != null && name != "" && name != "please_select".tr())
              ? name
              : "false";
      String modCountry =
          (country != null && country != -1) ? "$country" : "false";
      String modDep = (dep != null && dep != 0) ? "$dep" : "false";
      String modDep2 = (dep2 != null && dep2 != 0) ? "$dep2" : "false";
      String modManagerName = (directManager != null && directManager != 0)
          ? "$directManager"
          : "false";
      String modDeletionFilter = (deletionFilter != null &&
              deletionFilter != "" &&
              deletionFilter != "please_select".tr())
          ? deletionFilter
          : "false";
      String x = "";
      response = await http.get(
          Uri.parse(
              '${Urls.employeesAccounts}?page=$page&department_id2=$modDep&department_id=$modDep2&country_id=$modCountry&name=$modName&activity_id=$modWorkDirection&is_admin=$modAccountType&start_date=$modStartDate&end_date=$modEndDate&deleted_at=$modDeletionFilter&manager_id=$modManagerName'),
          headers: {
            'Authorization': 'Bearer $token',
            'mobile': '1',
            'Accept': 'application/json'
          });
    }

    if (response.statusCode == 200) {
      return EmployeeAccountResponse.fromJson(jsonDecode(response.body));
    } else {
      return EmployeeAccountResponse.withError(jsonDecode(response.body));
    }
  }

  Future<EmployeeAccountResponse> getEmpAccExitList(
    String token,
    String page, {
    String? startDate,
    String? endDate,
    int? accountType,
    String? workDirection,
    String? name,
    int? country,
    int? dep,
    int? dep2,
    int? directManager,
    String? deletionFilter,
  }) async {
    http.Response response;
    if ((startDate == "" || startDate == null) &&
        (endDate == "" || endDate == null) &&
        (accountType == 0 || accountType == null) &&
        (workDirection == "" || workDirection == null) &&
        (name == "please_select".tr() || name == "" || name == null) &&
        (country == null || country == -1) &&
        (dep == 0 || dep == null) &&
        (dep2 == 0 || dep2 == null) &&
        (directManager == 0 || directManager == null) &&
        (deletionFilter == "please_select".tr() || deletionFilter == null)) {
      response = await http.get(
          Uri.parse('${Urls.employeesAccounts}?page=$page&dismissal=1'),
          headers: {
            'Authorization': 'Bearer $token',
            'mobile': '1',
            'Accept': 'application/json'
          });
    } else {
      String modStartDate =
          (startDate != null && startDate != "") ? startDate : "false";
      String modEndDate =
          (endDate != null && endDate != "") ? endDate : "false";
      String modAccountType =
          (accountType != null && accountType != 0) ? "$accountType" : "false";
      String modWorkDirection =
          (workDirection != null && workDirection != "" && workDirection != "0")
              ? workDirection
              : "false";
      String modName =
          (name != null && name != "" && name != "please_select".tr())
              ? name
              : "false";
      String modCountry =
          (country != null && country != -1) ? "$country" : "false";
      String modDep = (dep != null && dep != 0) ? "$dep" : "false";
      if(modDep == "-2"){
        modDep = "null";
      }
      String modDep2 = (dep2 != null && dep2 != 0) ? "$dep2" : "false";
      String modManagerName = (directManager != null && directManager != 0)
          ? "$directManager"
          : "false";
      String modDeletionFilter = (deletionFilter != null &&
              deletionFilter != "" &&
              deletionFilter != "please_select".tr())
          ? deletionFilter
          : "false";
      String x = "";
      response = await http.get(
          Uri.parse(
              '${Urls.employeesAccounts}?page=$page&department_id2=$modDep2&department_id=$modDep&country_id=$modCountry&name=$modName&activity_id=$modWorkDirection&is_admin=$modAccountType&start_date=$modStartDate&end_date=$modEndDate&deleted_at=$modDeletionFilter&manager_id=$modManagerName&dismissal=1'),
          headers: {
            'Authorization': 'Bearer $token',
            'mobile': '1',
            'Accept': 'application/json'
          });
    }
    if (response.statusCode == 200) {
      return EmployeeAccountResponse.fromJson(jsonDecode(response.body));
    } else {
      return EmployeeAccountResponse.withError(jsonDecode(response.body));
    }
  }

  Future<DropDownListResponse> getDropDownList(String token) async {
    http.Response response = await http.get(Uri.parse(URL + 'list_selecte'),
        headers: {
          'Authorization': 'Bearer ' + token,
          'mobile': '1',
          'Accept': 'application/json'
        });
    if (response.statusCode == 200) {
      return DropDownListResponse.fromJson(jsonDecode(response.body));
    } else {
      return DropDownListResponse.withError(jsonDecode(response.body));
    }
  }

  Future<GeneralResponse> deleteEmployee(String token, int id) async {
    http.Response response = await http
        .delete(Uri.parse(URL + 'admin/users/' + id.toString()), headers: {
      'Authorization': 'Bearer ' + token,
      'mobile': '1',
      'Accept': 'application/json'
    });
    if (response.statusCode == 200) {
      return GeneralResponse.fromJson(jsonDecode(response.body));
    } else {
      return GeneralResponse.withError(jsonDecode(response.body));
    }
  }

  Future<GeneralResponse> reemploymentEmployee(String token, int id) async {
    print("djfdfjdf: $token");
    print("$id");
    http.Response response = await http
        .get(Uri.parse("${Urls.employeeAccount}$id/reemployee"), headers: {
      'Authorization': 'Bearer $token',
      'mobile': '1',
      'Accept': 'application/json'
    });
    if (response.statusCode == 200) {
      return GeneralResponse.fromJson(jsonDecode(response.body));
    } else {
      return GeneralResponse.withError(jsonDecode(response.body));
    }
  }

  Future<GeneralResponse> addEmployee(String token, FormData body) async {
    try {
      Response response = await _dio.post(URL + 'admin/users',
          options: Options(headers: {
            'Authorization': 'Bearer ' + token,
            'mobile': '1',
            'Accept': 'application/json'
          }),
          data: body);
      if (response.statusCode == 200) {
        return GeneralResponse.fromJson(response.data);
      } else {
        return GeneralResponse.withError(jsonDecode(response.data));
      }
    } on DioError catch (e) {
      String x = "";
      print("Exception occured: ${e.message} stackTrace");
      print("Exception occured: ${e.error.toString()} stackTrace");
      print(e.response!.data);
      if (e.response != null && e.response!.data != null) {
        Map<String, dynamic> res = e.response!.data!;
        if (res.containsKey("errors")) {
          Map<String, dynamic> errors = json.decode(json.encode(res["errors"]));
          String error = errors[errors.keys.toList().first]
              .toString()
              .replaceFirst("[", "")
              .replaceFirst("]", "");
          Fluttertoast.showToast(
              msg: error,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        } else {
          if (res.containsKey("message")) {
            Fluttertoast.showToast(
                msg: e.response!.data["message"].toString(),
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          } else {
            Fluttertoast.showToast(
                msg:
                    "الرجاء التأكد من مدخلاتك، يمكن وجود بعض الحقول موجودة سابقا",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          }
        }
      } else {
        Fluttertoast.showToast(
            msg: "الرجاء التأكد من مدخلاتك، يمكن وجود بعض الحقول موجودة سابقا",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
      return GeneralResponse.withError(jsonDecode(e.response!.data));
    }
  }

  Future<GeneralResponse> updateEmployee(
      String token, FormData body, int id) async {
    if (body.files.isNotEmpty) {
      try {
        Response response = await _dio.put('${URL}admin/users/$id',
            options: Options(headers: {
              'Authorization': 'Bearer $token',
              'Accept': 'application/json',
              'mobile': '1',
            }),
            data: body);
        if (response.statusCode == 200) {
          return GeneralResponse.fromJson(response.data);
        } else {
          return GeneralResponse.withError(jsonDecode(response.data));
        }
      } on DioError catch (e) {
        print("Exception occured: ${e.message} stackTrace");
        print("Exception occured: ${e.error.toString()} stackTrace");
        print(e.response!.data);
        if (e.response != null && e.response!.data != null) {
          Map<String, dynamic> res = e.response!.data!;
          if (res.containsKey("errors")) {
            Map<String, dynamic> errors =
                json.decode(json.encode(res["errors"]));
            String error = errors[errors.keys.toList().first]
                .toString()
                .replaceFirst("[", "")
                .replaceFirst("]", "");
            Fluttertoast.showToast(
                msg: error,
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          } else {
            if (res.containsKey("message")) {
              Fluttertoast.showToast(
                  msg: e.response!.data["message"].toString(),
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            } else {
              Fluttertoast.showToast(
                  msg:
                      "الرجاء التأكد من مدخلاتك، يمكن وجود بعض الحقول موجودة سابقا",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            }
          }
        } else {
          Fluttertoast.showToast(
              msg:
                  "الرجاء التأكد من مدخلاتك، يمكن وجود بعض الحقول موجودة سابقا",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        }
        return GeneralResponse.withError(jsonDecode(e.response!.data));
      }
    } else {
      Map<String, dynamic> formDataMap = {};
      for (MapEntry<String, String> entry in body.fields) {
        formDataMap[entry.key] = entry.value;
      }

      try {
        http.Response response =
            await http.put(Uri.parse('${URL}admin/users/$id'),
                headers: {
                  'Authorization': 'Bearer $token',
                  'Accept': 'application/json',
                  'mobile': '1',
                },
                body: formDataMap);
        if (response.statusCode == 200) {
          return GeneralResponse.fromJson(json.decode(response.body));
        } else {
          Map<String, dynamic> res = json.decode(response.body);
          if (res.containsKey("errors")) {
            Map<String, dynamic> errors =
                json.decode(json.encode(res["errors"]));
            String error = errors[errors.keys.toList().first]
                .toString()
                .replaceFirst("[", "")
                .replaceFirst("]", "");
            Fluttertoast.showToast(
                msg: error,
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          } else {
            if (res.containsKey("message")) {
              Fluttertoast.showToast(
                  msg: json.decode(response.body)["message"].toString(),
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            } else {
              Fluttertoast.showToast(
                  msg:
                      "الرجاء التأكد من مدخلاتك، يمكن وجود بعض الحقول موجودة سابقا",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            }
          }
          return GeneralResponse.withError(jsonDecode(response.body));
        }
      } on DioError catch (e) {
        if (e.response != null && e.response!.data != null) {
          Map<String, dynamic> res = e.response!.data!;
          if (res.containsKey("errors")) {
            Map<String, dynamic> errors =
                json.decode(json.encode(res["errors"]));
            String error = errors[errors.keys.toList().first]
                .toString()
                .replaceFirst("[", "")
                .replaceFirst("]", "");
            Fluttertoast.showToast(
                msg: error,
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          } else {
            if (res.containsKey("message")) {
              Fluttertoast.showToast(
                  msg: e.response!.data["message"].toString(),
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            } else {
              Fluttertoast.showToast(
                  msg:
                      "الرجاء التأكد من مدخلاتك، يمكن وجود بعض الحقول موجودة سابقا",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            }
          }
        } else {
          Fluttertoast.showToast(
              msg:
                  "الرجاء التأكد من مدخلاتك، يمكن وجود بعض الحقول موجودة سابقا",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        }
        return GeneralResponse.withError(jsonDecode(e.response!.data));
      }
    }
  }

  Future<GeneralResponse> dismissalEmployee(String token, int id) async {
    http.Response response = await http.post(
        Uri.parse(URL + 'admin/users/' + id.toString() + '/dismissal'),
        headers: {
          'Authorization': 'Bearer ' + token,
          'mobile': '1',
          'Accept': 'application/json'
        });
    if (response.statusCode == 200) {
      return GeneralResponse.fromJson(jsonDecode(response.body));
    } else {
      return GeneralResponse.withError(jsonDecode(response.body));
    }
  }
}
