import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employee_account_model.dart';

class EmployeeAccountResponse {
  final bool success;
  final List<EmployeeAccountModel> empAccList;

  final String message;

  EmployeeAccountResponse(this.message, this.success, this.empAccList);

  EmployeeAccountResponse.fromJson(Map<String, dynamic> json)
      : message = json['message'],
        empAccList = json["data"] != null
            ? List<EmployeeAccountModel>.from(
                json["data"].map((x) => EmployeeAccountModel.fromJson(x)))
            : [],
        success = json['success'];

  EmployeeAccountResponse.withError(Map<String, dynamic> json)
      : message = json['message'],
        empAccList = [],
        success = false;
}
