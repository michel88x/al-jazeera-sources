import 'package:employee/model/hr_model/company_model.dart';
import 'package:employee/model/hr_model/department_model.dart';
import 'package:employee/model/hr_model/user_model.dart';

class DropDownListResponse {
  final bool success;
  final List<UserModel> userssList;

  final List<CompanyModel> tasksList;
  final List<DepartmentModel> departmentList;
  final List<CompanyModel> isAdminList;
  final List<CompanyModel> companyList;
  final List<CompanyModel> delegatesList;
  final String message;

  DropDownListResponse(this.message, this.success, this.companyList,
      this.departmentList, this.isAdminList, this.tasksList, this.userssList, this.delegatesList);

  DropDownListResponse.fromJson(Map<String, dynamic> json)
      : message = json['message'],
        userssList = json["data"]['users'] != null
            ? List<UserModel>.from(
                json["data"]['users'].map((x) => UserModel.fromJson(x)))
            : [],
        tasksList = json["data"]['tasks'] != null
            ? List<CompanyModel>.from(
                json["data"]['tasks'].map((x) => CompanyModel.fromJson(x)))
            : [],
        departmentList = json["data"]['departments'] != null
            ? List<DepartmentModel>.from(json["data"]['departments']
                .map((x) => DepartmentModel.fromJson(x)))
            : [],
        isAdminList = json["data"]['is_admin'] != null
            ? List<CompanyModel>.from(
                json["data"]['is_admin'].map((x) => CompanyModel.fromJson(x)))
            : [],
        companyList = json["data"]['companies'] != null
            ? List<CompanyModel>.from(
                json["data"]['companies'].map((x) => CompanyModel.fromJson(x)))
            : [],
        delegatesList = json["data"]['Delegate_list'] != null
            ? List<CompanyModel>.from(
            json["data"]['Delegate_list'].map((x) => CompanyModel.fromJson(x)))
            : [],
        success = json['success'];

  DropDownListResponse.withError(Map<String, dynamic> json)
      : message = json['message'],
        userssList = [],
        tasksList = [],
        isAdminList = [],
        departmentList = [],
        companyList = [],
        delegatesList = [],
        success = false;
}
