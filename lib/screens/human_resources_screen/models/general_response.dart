class GeneralResponse {
  final bool status;
  final String message;

  GeneralResponse(this.message, this.status);

  GeneralResponse.fromJson(Map<String, dynamic> json)
      : message = json['message'],
        status = json['status'] == null ? json['success'] : json['status'];

  GeneralResponse.withError(Map<String, dynamic> json)
      : message = json['message'],
        status = false;
}
