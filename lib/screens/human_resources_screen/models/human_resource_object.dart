import 'package:flutter/cupertino.dart';

class HumanResourceObject{
  final String? title;
  final IconData? icon;

  HumanResourceObject({this.title, this.icon});
}