import 'package:employee/model/hr_model/report_moth_model.dart';

class ReportMonthResponse {
  final bool success;
  final List<ReportMonthModel> reportMList;

  final String message;

  ReportMonthResponse(this.message, this.success, this.reportMList);

  ReportMonthResponse.fromJson(Map<String, dynamic> json)
      : message = json['message'],
        reportMList = json["data"] != null
            ? List<ReportMonthModel>.from(
                json["data"].map((x) => ReportMonthModel.fromJson(x)))
            : [],
        success = json['success'];

  ReportMonthResponse.withError(Map<String, dynamic> json)
      : message = json['message'],
        reportMList = [],
        success = false;
}
