import 'package:easy_localization/easy_localization.dart';
import 'package:employee/screens/human_resources_screen/helper/human_resources_helper.dart';
import 'package:employee/screens/human_resources_screen/models/human_resource_object.dart';
import 'package:employee/screens/human_resources_screen/widgets/human_resource_item.dart';
import 'package:employee/widget/app_bar_page.dart';
import 'package:flutter/material.dart';

class HumanResourcesScreen extends StatefulWidget {
  @override
  State<HumanResourcesScreen> createState() => _HumanResourcesScreenState();
}

class _HumanResourcesScreenState extends State<HumanResourcesScreen> {

  late List<HumanResourceObject> items;

  @override
  void initState() {
    super.initState();
    items = HumanResourcesHelper.getHumanResourcesSections();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,

      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(20, 120, 20, 20.0),
            child: GridView.builder(
              shrinkWrap: true,
              itemCount: items.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              childAspectRatio: 0.8,
              mainAxisSpacing: 2,
              crossAxisSpacing: 4),
              padding: EdgeInsets.zero,
              itemBuilder: (context, index) {
            return HumanResourceItem(item: items[index], index: index,);
              },
            ),
          ),
          App_Bar(
            title: 'human_resources'.tr(),
            LeadingIcon: Icons.keyboard_arrow_right,
            LeadingOnClick: (){
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
