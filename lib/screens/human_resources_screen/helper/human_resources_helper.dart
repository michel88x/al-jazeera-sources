import 'package:easy_localization/easy_localization.dart';
import 'package:employee/screens/human_resources_screen/models/human_resource_object.dart';
import 'package:flutter/material.dart';

class HumanResourcesHelper {
  static List<HumanResourceObject> getHumanResourcesSections(){
    return <HumanResourceObject>[
      HumanResourceObject(title: "employees_accounts".tr(), icon: Icons.eight_mp_outlined),
      HumanResourceObject(title: "fired_employees_accounts".tr(), icon: Icons.eight_mp),
    ];
  }
}