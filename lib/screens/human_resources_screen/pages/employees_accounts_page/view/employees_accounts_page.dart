import 'package:dynamic_height_grid_view/dynamic_height_grid_view.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_images.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/base_widgets/custom_button.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:employee/screens/human_resources_screen/bloc/hr_cubit.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employee_account_model.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/add_employee_page/view/add_employee_page.dart';
import 'package:employee/screens/human_resources_screen/bloc/hr_state.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/download_employee_accounts_pdf_page/download_employee_accounts_pdf_page.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/download_employee_accounts_xls_page/view/download_employee_accounts_xls_page.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employee_account_item.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/view/employees_accounts_search_widget.dart';
import 'package:employee/screens/human_resources_screen/repository/hr_repository_impl.dart';
import 'package:employee/widget/app_bar_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class EmployeesAccountsPage extends StatefulWidget {
  String title;
  int type;

  EmployeesAccountsPage(this.title, this.type);

  @override
  State<EmployeesAccountsPage> createState() => _EmployeesAccountsPageState();
}

class _EmployeesAccountsPageState extends State<EmployeesAccountsPage> {
  final RefreshController _refreshController1 =
      RefreshController(initialRefresh: false);
  final RefreshController _refreshController2 =
      RefreshController(initialRefresh: false);
  String savedStartDate = "";
  String savedEndDate = "";
  int savedAccountType = 0;
  String savedWorkDirection = "";
  String savedName = "";
  int savedCountry = -1;
  int savedDep = 0;
  int savedDep2 = 0;
  int savedDirectManager = 0;
  String savedDeletionFilter = "";

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => HrCubit(HrRepositoryImpl()),
        child: Scaffold(
          backgroundColor: Colors.white,
          body: BlocBuilder<HrCubit, HrState>(
            builder: (context, state) {
              if (state is HrInitial) {
                if (widget.type == 0) {
                  context.read<HrCubit>().getEmpAccList(context,
                  startDate: savedStartDate,
                    endDate: savedEndDate,
                    accountType: savedAccountType,
                    workDirection: savedWorkDirection,
                    name: savedName,
                    country: savedCountry,
                    dep: savedDep,
                    dep2: savedDep2,
                    directManager: savedDirectManager,
                    deletionFilter: savedDeletionFilter
                  );
                } else {
                  context.read<HrCubit>().getEmpAccExitList(context,startDate: savedStartDate,
                      endDate: savedEndDate,
                      accountType: savedAccountType,
                      workDirection: savedWorkDirection,
                      name: savedName,
                      country: savedCountry,
                      dep: savedDep,
                      dep2: savedDep2,
                      directManager: savedDirectManager,
                      deletionFilter: savedDeletionFilter);
                }
                return Stack(
                  children: [
                    const Center(
                      child: CircularProgressIndicator(),
                    ),
                    App_Bar(
                      title: widget.title,
                      LeadingIcon: Icons.keyboard_arrow_right,
                      LeadingOnClick: () {
                        goBack(context);
                      },
                      actionIcon: widget.type == 0 ? Icons.add : null,
                      actionClick: () async{
                        var data = await gotoData(
                            context,
                            AddEmplyoeePage(
                                0,
                                EmployeeAccountModel(
                                    type: '',
                                    finalPlaces: [],
                                    name: '',
                                    image: '',
                                    email: '',
                                    id: 0,
                                    activity: '',
                                    bn: '',
                                    cardFrom: 0,
                                    cardTo: 0,
                                    confirmCode: 0,
                                    daysOff: '',
                                    daysOn: '',
                                    deletedAt: '',
                                    department1: '',
                                    department2: '',
                                    department: '',
                                    departmentId2: 0,
                                    departmentId: 0,
                                    dismissalDate: '',
                                    dismissalId: 0,
                                    endAutoTime: '',
                                    endWorkTime: '',
                                    id2: 0,
                                    idNumber: '',
                                    isAdmin: 0,
                                    isConfirmed: 0,
                                    isOpen: 0,
                                    managerId: 0,
                                    managerName: '',
                                    mobile: '',
                                    note: '',
                                    position: '',
                                    sendEmail: '',
                                    sendSms: '',
                                    sendWhatsapp: '',
                                    signature: '',
                                    startAutoTime: '',
                                    startWorkTime: '',
                                    statusId: 0,
                                    country: "",
                                    violationOnOff: "غير مفعل")));
                        if(data != null && data){
                          context.read<HrCubit>().page = 1;
                          savedStartDate = "";
                          savedEndDate = "";
                          savedAccountType = 0;
                          savedWorkDirection = "";
                          savedName = "";
                          savedCountry = -1;
                          savedDep = 0;
                          savedDep2 = 0;
                          savedDirectManager = 0;
                          savedDeletionFilter = "";
                          // monitor network fetch

                          if (widget.type == 0) {
                            context.read<HrCubit>().getEmpAccList(context,
                                startDate: savedStartDate,
                                endDate: savedEndDate,
                                accountType: savedAccountType,
                                workDirection: savedWorkDirection,
                                name: savedName,
                                country: savedCountry,
                                dep: savedDep,
                                dep2: savedDep2,
                                directManager: savedDirectManager,
                                deletionFilter: savedDeletionFilter);
                          } else {
                            context
                                .read<HrCubit>()
                                .getEmpAccExitList(context,
                                startDate: savedStartDate,
                                endDate: savedEndDate,
                                accountType: savedAccountType,
                                workDirection: savedWorkDirection,
                                name: savedName,
                                country: savedCountry,
                                dep: savedDep,
                                dep2: savedDep2,
                                directManager: savedDirectManager,
                                deletionFilter: savedDeletionFilter);
                          }

                          await Future.delayed(
                              const Duration(milliseconds: 1000));

                          // if failed,use refreshFailed()
                          _refreshController2.refreshCompleted();
                        }
                      },
                    )
                  ],
                );
              } else if (state is HrLoaded) {
                return Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 90, 20, 20.0),
                      child: SmartRefresher(
                          enablePullDown: true,
                          enablePullUp: true,
                          header: const WaterDropHeader(),
                          footer: CustomFooter(
                            builder: (BuildContext context, LoadStatus? mode) {
                              Widget body;
                              if (mode == LoadStatus.idle) {
                                body = const Text("No more Data");
                              } else if (mode == LoadStatus.loading) {
                                body = const CupertinoActivityIndicator();
                              } else if (mode == LoadStatus.failed) {
                                body = const Text("Load Failed!Click retry!");
                              } else if (mode == LoadStatus.canLoading) {
                                body = const Text("release to load more");
                              } else {
                                body = const Text("No more Data");
                              }
                              return Container(
                                height: 55.0,
                                child: Center(child: body),
                              );
                            },
                          ),
                          controller: _refreshController2,
                          onRefresh: () async {
                            context.read<HrCubit>().page = 1;
                            savedStartDate = "";
                            savedEndDate = "";
                            savedAccountType = 0;
                            savedWorkDirection = "";
                            savedName = "";
                            savedCountry = -1;
                            savedDep = 0;
                            savedDep2 = 0;
                            savedDirectManager = 0;
                            savedDeletionFilter = "";
                            // monitor network fetch

                            if (widget.type == 0) {
                              context.read<HrCubit>().getEmpAccList(context,
                                  startDate: savedStartDate,
                                  endDate: savedEndDate,
                                  accountType: savedAccountType,
                                  workDirection: savedWorkDirection,
                                  name: savedName,
                                  country: savedCountry,
                                  dep: savedDep,
                                  dep2: savedDep2,
                                  directManager: savedDirectManager,
                                  deletionFilter: savedDeletionFilter);
                            } else {
                              context
                                  .read<HrCubit>()
                                  .getEmpAccExitList(context,
                                  startDate: savedStartDate,
                                  endDate: savedEndDate,
                                  accountType: savedAccountType,
                                  workDirection: savedWorkDirection,
                                  name: savedName,
                                  country: savedCountry,
                                  dep: savedDep,
                                  dep2: savedDep2,
                                  directManager: savedDirectManager,
                                  deletionFilter: savedDeletionFilter);
                            }

                            await Future.delayed(
                                const Duration(milliseconds: 1000));

                            // if failed,use refreshFailed()
                            _refreshController2.refreshCompleted();
                          },
                          onLoading: () async {
                            // monitor network fetch

                            if (widget.type == 0) {
                              context.read<HrCubit>().getEmpAccListMore(
                                  context, state.data.length,
                                  startDate: savedStartDate,
                                  endDate: savedEndDate,
                                  accountType: savedAccountType,
                                  workDirection: savedWorkDirection,
                                  name: savedName,
                                  country: savedCountry,
                                  dep: savedDep,
                                  dep2: savedDep2,
                                  directManager: savedDirectManager,
                                  deletionFilter: savedDeletionFilter);
                            } else {
                              context.read<HrCubit>().getEmpAccExitListMore(
                                  context, state.data.length,
                                  startDate: savedStartDate,
                                  endDate: savedEndDate,
                                  accountType: savedAccountType,
                                  workDirection: savedWorkDirection,
                                  name: savedName,
                                  country: savedCountry,
                                  dep: savedDep,
                                  dep2: savedDep2,
                                  directManager: savedDirectManager,
                                  deletionFilter: savedDeletionFilter);
                            }

                            await Future.delayed(
                                const Duration(milliseconds: 5000));

                            // if failed,use refreshFailed()
                            _refreshController2.loadComplete();
                            setState(() {});
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              EmployeesAccountsSearchWidget(
                                inputStartDate: savedStartDate,
                                inputEndDate: savedEndDate,
                                inputAccountId: savedAccountType,
                                inputWorkDirection: savedWorkDirection,
                                inputName: savedName,
                                inputCountry: savedCountry,
                                inputDep: savedDep,
                                inputDep2: savedDep2,
                                inputDirectManager: savedDirectManager,
                                inputDeletionFilter: savedDeletionFilter,
                                type: widget.type,
                                onSubmit: (selectedStartDate,
                                    selectedEndDate,
                                    selectedAccountType,
                                    selectedWorkDirection,
                                    selectedName,
                                    selectedCountry,
                                    selectedDep,
                                    selectedDep2,
                                    selectedDirectManager,
                                    selectedDeletionFilter) {
                                  savedStartDate = selectedStartDate;
                                  savedEndDate = selectedEndDate;
                                  savedAccountType = selectedAccountType;
                                  savedWorkDirection = selectedWorkDirection;
                                  savedName = selectedName;
                                  savedCountry = selectedCountry;
                                  savedDep = selectedDep;
                                  savedDep2 = selectedDep2;
                                  savedDirectManager = selectedDirectManager;
                                  savedDeletionFilter = selectedDeletionFilter;
                                  if(widget.type == 0){
                                    context
                                        .read<HrCubit>()
                                        .getEmpAccList(context, startDate: savedStartDate,
                                        endDate: savedEndDate,
                                        accountType: savedAccountType,
                                        workDirection: savedWorkDirection,
                                        name: savedName,
                                        country: savedCountry,
                                        dep: savedDep,
                                        dep2: savedDep2,
                                        directManager: savedDirectManager,
                                        deletionFilter: savedDeletionFilter);
                                  }else{
                                    context
                                        .read<HrCubit>()
                                        .getEmpAccExitList(context, startDate: savedStartDate,
                                        endDate: savedEndDate,
                                        accountType: savedAccountType,
                                        workDirection: savedWorkDirection,
                                        name: savedName,
                                        country: savedCountry,
                                        dep: savedDep,
                                        dep2: savedDep2,
                                        directManager: savedDirectManager,
                                        deletionFilter: savedDeletionFilter);
                                  }
                                },
                              ),
                              CustomButton(
                                onPressed: () {
                                  goto(context, DownloadEmployeeAccountPdfPage(type: widget.type,));
                                },
                                isTextCentered: true,
                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0.0),
                                backColor: AppColors.primary,
                                borderRadius: 15,
                                innerPadding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                                title: 'print'.tr(),
                                textStyle: AppStyles.aleoSeventeenWhiteBold,
                              ),
                              CustomButton(
                                onPressed: () {
                                  goto(context, DownloadEmployeeAccountsXlsPage(type: widget.type,));
                                },
                                isTextCentered: true,
                                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0.0),
                                backColor: AppColors.primary,
                                borderRadius: 15,
                                innerPadding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                                title: 'export_excel'.tr(),
                                textStyle: AppStyles.aleoSeventeenWhiteBold,
                              ),
                              DynamicHeightGridView(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: state.data.length,
                                crossAxisSpacing: 4,
                                crossAxisCount: 2,
                                mainAxisSpacing: 2,
                                builder: (context, index) {
                                  return EmployeeAccountItem(
                                    data: state.data[index],
                                    type: widget.type,
                                    onDeleteEmployee: () {
                                      context.read<HrCubit>().deleteEmployee(
                                          context, state.data[index].id!);
                                    },
                                    onDismissEmployee: () {
                                      context.read<HrCubit>().dismissalEmployee(
                                          context, state.data[index].id!);
                                    },
                                    onReemploymentEmployee: (){
                                      context.read<HrCubit>().reemployeeEmployee(
                                          context, state.data[index].id!);
                                    },
                                    onEditPressed: () async{
                                      var data = await gotoData(context, AddEmplyoeePage(1, state.data[index]));
                                      if(data != null && data){
                                        context.read<HrCubit>().page = 1;
                                        savedStartDate = "";
                                        savedEndDate = "";
                                        savedAccountType = 0;
                                        savedWorkDirection = "";
                                        savedName = "";
                                        savedCountry = -1;
                                        savedDep = 0;
                                        savedDep2 = 0;
                                        savedDirectManager = 0;
                                        savedDeletionFilter = "";
                                        // monitor network fetch

                                        if (widget.type == 0) {
                                          context.read<HrCubit>().getEmpAccList(context,
                                              startDate: savedStartDate,
                                              endDate: savedEndDate,
                                              accountType: savedAccountType,
                                              workDirection: savedWorkDirection,
                                              name: savedName,
                                              country: savedCountry,
                                              dep: savedDep,
                                              dep2: savedDep2,
                                              directManager: savedDirectManager,
                                              deletionFilter: savedDeletionFilter);
                                        } else {
                                          context
                                              .read<HrCubit>()
                                              .getEmpAccExitList(context,
                                              startDate: savedStartDate,
                                              endDate: savedEndDate,
                                              accountType: savedAccountType,
                                              workDirection: savedWorkDirection,
                                              name: savedName,
                                              country: savedCountry,
                                              dep: savedDep,
                                              dep2: savedDep2,
                                              directManager: savedDirectManager,
                                              deletionFilter: savedDeletionFilter);
                                        }
                                        await Future.delayed(
                                            const Duration(milliseconds: 1000));
                                        _refreshController2.refreshCompleted();
                                      }
                                    },
                                  );
                                },
                              ),
                            ],
                          )),
                    ),
                    App_Bar(
                      title: widget.title,
                      LeadingIcon: Icons.keyboard_arrow_right,
                      LeadingOnClick: () {
                        goBack(context);
                      },
                      actionIcon: widget.type == 0 ? Icons.add : null,
                      actionClick: () async{
                        var data = await gotoData(
                            context,
                            AddEmplyoeePage(
                                0,
                                EmployeeAccountModel(
                                    type: '',
                                    finalPlaces: [],
                                    name: '',
                                    image: '',
                                    email: '',
                                    id: 0,
                                    activity: '',
                                    bn: '',
                                    cardFrom: 0,
                                    cardTo: 0,
                                    confirmCode: 0,
                                    daysOff: '',
                                    daysOn: '',
                                    deletedAt: '',
                                    department1: '',
                                    department2: '',
                                    department: '',
                                    departmentId2: 0,
                                    departmentId: 0,
                                    dismissalDate: '',
                                    dismissalId: 0,
                                    endAutoTime: '',
                                    endWorkTime: '',
                                    id2: 0,
                                    idNumber: '',
                                    isAdmin: 0,
                                    isConfirmed: 0,
                                    isOpen: 0,
                                    managerId: 0,
                                    managerName: '',
                                    mobile: '',
                                    note: '',
                                    position: '',
                                    sendEmail: '',
                                    sendSms: '',
                                    sendWhatsapp: '',
                                    signature: '',
                                    startAutoTime: '',
                                    startWorkTime: '',
                                    statusId: 0,
                                    country: "",
                                    violationOnOff: "غير مفعل")));
                        if(data != null && data){
                          context.read<HrCubit>().page = 1;
                          savedStartDate = "";
                          savedEndDate = "";
                          savedAccountType = 0;
                          savedWorkDirection = "";
                          savedName = "";
                          savedCountry = -1;
                          savedDep = 0;
                          savedDep2 = 0;
                          savedDirectManager = 0;
                          savedDeletionFilter = "";
                          // monitor network fetch

                          if (widget.type == 0) {
                            context.read<HrCubit>().getEmpAccList(context,
                                startDate: savedStartDate,
                                endDate: savedEndDate,
                                accountType: savedAccountType,
                                workDirection: savedWorkDirection,
                                name: savedName,
                                country: savedCountry,
                                dep: savedDep,
                                dep2: savedDep2,
                                directManager: savedDirectManager,
                                deletionFilter: savedDeletionFilter);
                          } else {
                            context
                                .read<HrCubit>()
                                .getEmpAccExitList(context,
                                startDate: savedStartDate,
                                endDate: savedEndDate,
                                accountType: savedAccountType,
                                workDirection: savedWorkDirection,
                                name: savedName,
                                country: savedCountry,
                                dep: savedDep,
                                dep2: savedDep2,
                                directManager: savedDirectManager,
                                deletionFilter: savedDeletionFilter);
                          }

                          await Future.delayed(
                              const Duration(milliseconds: 1000));

                          // if failed,use refreshFailed()
                          _refreshController2.refreshCompleted();
                        }
                      },
                    )
                  ],
                );
              } else if (state is HrLoadedMore) {
                return Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 90, 20, 20.0),
                      child: Container(
                        child: SmartRefresher(
                            enablePullDown: true,
                            enablePullUp: true,
                            header: const WaterDropHeader(),
                            footer: CustomFooter(
                              builder:
                                  (BuildContext context, LoadStatus? mode) {
                                Widget body;
                                if (mode == LoadStatus.idle) {
                                  body = const Text("No more Data");
                                } else if (mode == LoadStatus.loading) {
                                  body = const CupertinoActivityIndicator();
                                } else if (mode == LoadStatus.failed) {
                                  body = const Text("Load Failed!Click retry!");
                                } else if (mode == LoadStatus.canLoading) {
                                  body = const Text("release to load more");
                                } else {
                                  body = const Text("No more Data");
                                }
                                return Container(
                                  height: 55.0,
                                  child: Center(child: body),
                                );
                              },
                            ),
                            controller: _refreshController1,
                            onRefresh: () async {
                              context.read<HrCubit>().page = 1;

                              // monitor network fetch
                              savedStartDate = "";
                              savedEndDate = "";
                              savedAccountType = 0;
                              savedWorkDirection = "";
                              savedName = "";
                              savedCountry = -1;
                              savedDep = 0;
                              savedDep2 = 0;
                              savedDirectManager = 0;
                              savedDeletionFilter = "";
                              if (widget.type == 0) {
                                context.read<HrCubit>().getEmpAccList(context,
                                    startDate: savedStartDate,
                                    endDate: savedEndDate,
                                    accountType: savedAccountType,
                                    workDirection: savedWorkDirection,
                                    name: savedName,
                                    country: savedCountry,
                                    dep: savedDep,
                                    dep2: savedDep2,
                                    directManager: savedDirectManager,
                                    deletionFilter: savedDeletionFilter);
                              } else {
                                context
                                    .read<HrCubit>()
                                    .getEmpAccExitList(context,
                                    startDate: savedStartDate,
                                    endDate: savedEndDate,
                                    accountType: savedAccountType,
                                    workDirection: savedWorkDirection,
                                    name: savedName,
                                    country: savedCountry,
                                    dep: savedDep,
                                    dep2: savedDep2,
                                    directManager: savedDirectManager,
                                    deletionFilter: savedDeletionFilter);
                              }

                              await Future.delayed(
                                  const Duration(milliseconds: 1000));

                              // if failed,use refreshFailed()
                              _refreshController1.refreshCompleted();
                            },
                            onLoading: () async {
                              // monitor network fetch

                              if (widget.type == 0) {
                                context.read<HrCubit>().getEmpAccListMore(
                                    context, state.data.length,
                                    startDate: savedStartDate,
                                    endDate: savedEndDate,
                                    accountType: savedAccountType,
                                    workDirection: savedWorkDirection,
                                    name: savedName,
                                    country: savedCountry,
                                    dep: savedDep,
                                    dep2: savedDep2,
                                    directManager: savedDirectManager,
                                    deletionFilter: savedDeletionFilter);
                              } else {
                                context.read<HrCubit>().getEmpAccExitListMore(
                                    context, state.data.length,
                                    startDate: savedStartDate,
                                    endDate: savedEndDate,
                                    accountType: savedAccountType,
                                    workDirection: savedWorkDirection,
                                    name: savedName,
                                    country: savedCountry,
                                    dep: savedDep,
                                    dep2: savedDep2,
                                    directManager: savedDirectManager,
                                    deletionFilter: savedDeletionFilter);
                              }

                              await Future.delayed(
                                  const Duration(milliseconds: 5000));

                              // if failed,use refreshFailed()
                              _refreshController1.loadComplete();
                              setState(() {});
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                EmployeesAccountsSearchWidget(
                                  inputStartDate: savedStartDate,
                                  inputEndDate: savedEndDate,
                                  inputAccountId: savedAccountType,
                                  inputWorkDirection: savedWorkDirection,
                                  inputName: savedName,
                                  inputCountry: savedCountry,
                                  inputDep: savedDep,
                                  inputDep2: savedDep2,
                                  inputDirectManager: savedDirectManager,
                                  inputDeletionFilter: savedDeletionFilter,
                                  type: widget.type,
                                  onSubmit: (selectedStartDate,
                                      selectedEndDate,
                                      selectedAccountType,
                                      selectedWorkDirection,
                                      selectedName,
                                      selectedCountry,
                                      selectedDep,
                                      selectedDep2,
                                      selectedDirectManager,
                                      selectedDeletionFilter) {
                                    savedStartDate = selectedStartDate;
                                    savedEndDate = selectedEndDate;
                                    savedAccountType = selectedAccountType;
                                    savedWorkDirection = selectedWorkDirection;
                                    savedName = selectedName;
                                    savedCountry = selectedCountry;
                                    savedDep = selectedDep;
                                    savedDep2 = selectedDep2;
                                    savedDirectManager = selectedDirectManager;
                                    savedDeletionFilter = selectedDeletionFilter;
                                    if(widget.type == 0){
                                      context
                                          .read<HrCubit>()
                                          .getEmpAccList(context, startDate: savedStartDate,
                                          endDate: savedEndDate,
                                          accountType: savedAccountType,
                                          workDirection: savedWorkDirection,
                                          name: savedName,
                                          country: savedCountry,
                                          dep: savedDep,
                                          dep2: savedDep2,
                                          directManager: savedDirectManager,
                                          deletionFilter: savedDeletionFilter);
                                    }else{
                                      context
                                          .read<HrCubit>()
                                          .getEmpAccExitList(context, startDate: savedStartDate,
                                          endDate: savedEndDate,
                                          accountType: savedAccountType,
                                          workDirection: savedWorkDirection,
                                          name: savedName,
                                          country: savedCountry,
                                          dep: savedDep,
                                          dep2: savedDep2,
                                          directManager: savedDirectManager,
                                          deletionFilter: savedDeletionFilter);
                                    }
                                  },
                                ),
                                CustomButton(
                                  onPressed: () {
                                    goto(context, DownloadEmployeeAccountPdfPage(type: widget.type,));
                                  },
                                  isTextCentered: true,
                                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 0.0),
                                  backColor: AppColors.primary,
                                  borderRadius: 15,
                                  innerPadding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  title: 'print'.tr(),
                                  textStyle: AppStyles.aleoSeventeenWhiteBold,
                                ),
                                CustomButton(
                                  onPressed: () {
                                    goto(context, DownloadEmployeeAccountsXlsPage(type: widget.type,));
                                  },
                                  isTextCentered: true,
                                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 0.0),
                                  backColor: AppColors.primary,
                                  borderRadius: 15,
                                  innerPadding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  title: 'export_excel'.tr(),
                                  textStyle: AppStyles.aleoSeventeenWhiteBold,
                                ),
                                DynamicHeightGridView(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: state.data.length,
                                  crossAxisSpacing: 4,
                                  crossAxisCount: 2,
                                  mainAxisSpacing: 2,
                                  builder: (context, index) {
                                    return EmployeeAccountItem(
                                      data: state.data[index],
                                      type: widget.type,
                                      onDeleteEmployee: () {
                                        context.read<HrCubit>().deleteEmployee(
                                            context, state.data[index].id!);
                                      },
                                      onDismissEmployee: () {
                                        context.read<HrCubit>().dismissalEmployee(
                                            context, state.data[index].id!);
                                      },
                                      onReemploymentEmployee: (){
                                        context.read<HrCubit>().reemployeeEmployee(
                                            context, state.data[index].id!);
                                      },
                                      onEditPressed: () async{
                                        var data = await gotoData(context, AddEmplyoeePage(1, state.data[index]));
                                        if(data != null && data){
                                          context.read<HrCubit>().page = 1;
                                          savedStartDate = "";
                                          savedEndDate = "";
                                          savedAccountType = 0;
                                          savedWorkDirection = "";
                                          savedName = "";
                                          savedCountry = -1;
                                          savedDep = 0;
                                          savedDep2 = 0;
                                          savedDirectManager = 0;
                                          savedDeletionFilter = "";
                                          // monitor network fetch

                                          if (widget.type == 0) {
                                            context.read<HrCubit>().getEmpAccList(context,
                                                startDate: savedStartDate,
                                                endDate: savedEndDate,
                                                accountType: savedAccountType,
                                                workDirection: savedWorkDirection,
                                                name: savedName,
                                                country: savedCountry,
                                                dep: savedDep,
                                                dep2: savedDep2,
                                                directManager: savedDirectManager,
                                                deletionFilter: savedDeletionFilter);
                                          } else {
                                            context
                                                .read<HrCubit>()
                                                .getEmpAccExitList(context,
                                                startDate: savedStartDate,
                                                endDate: savedEndDate,
                                                accountType: savedAccountType,
                                                workDirection: savedWorkDirection,
                                                name: savedName,
                                                country: savedCountry,
                                                dep: savedDep,
                                                dep2: savedDep2,
                                                directManager: savedDirectManager,
                                                deletionFilter: savedDeletionFilter);
                                          }
                                          await Future.delayed(
                                              const Duration(milliseconds: 1000));
                                          _refreshController2.refreshCompleted();
                                        }
                                      },
                                    );
                                  },
                                ),
                              ],
                            )),
                      ),
                    ),
                    App_Bar(
                      title: widget.title,
                      LeadingIcon: Icons.keyboard_arrow_right,
                      LeadingOnClick: () {
                        goBack(context);
                      },
                      actionIcon: widget.type == 0 ? Icons.add : null,
                      actionClick: () async{
                        var data = await gotoData(
                            context,
                            AddEmplyoeePage(
                                0,
                                EmployeeAccountModel(
                                    type: '',
                                    finalPlaces: [],
                                    name: '',
                                    image: '',
                                    email: '',
                                    id: 0,
                                    activity: '',
                                    bn: '',
                                    cardFrom: 0,
                                    cardTo: 0,
                                    confirmCode: 0,
                                    daysOff: '',
                                    daysOn: '',
                                    deletedAt: '',
                                    department1: '',
                                    department2: '',
                                    department: '',
                                    departmentId2: 0,
                                    departmentId: 0,
                                    dismissalDate: '',
                                    dismissalId: 0,
                                    endAutoTime: '',
                                    endWorkTime: '',
                                    id2: 0,
                                    idNumber: '',
                                    isAdmin: 0,
                                    isConfirmed: 0,
                                    isOpen: 0,
                                    managerId: 0,
                                    managerName: '',
                                    mobile: '',
                                    note: '',
                                    position: '',
                                    sendEmail: '',
                                    sendSms: '',
                                    sendWhatsapp: '',
                                    signature: '',
                                    startAutoTime: '',
                                    startWorkTime: '',
                                    statusId: 0,
                                    country: "",
                                    violationOnOff: "غير مفعل")));
                        if(data != null && data){
                          context.read<HrCubit>().page = 1;
                          savedStartDate = "";
                          savedEndDate = "";
                          savedAccountType = 0;
                          savedWorkDirection = "";
                          savedName = "";
                          savedCountry = -1;
                          savedDep = 0;
                          savedDep2 = 0;
                          savedDirectManager = 0;
                          savedDeletionFilter = "";
                          // monitor network fetch

                          if (widget.type == 0) {
                            context.read<HrCubit>().getEmpAccList(context,
                                startDate: savedStartDate,
                                endDate: savedEndDate,
                                accountType: savedAccountType,
                                workDirection: savedWorkDirection,
                                name: savedName,
                                country: savedCountry,
                                dep: savedDep,
                                dep2: savedDep2,
                                directManager: savedDirectManager,
                                deletionFilter: savedDeletionFilter);
                          } else {
                            context
                                .read<HrCubit>()
                                .getEmpAccExitList(context,
                                startDate: savedStartDate,
                                endDate: savedEndDate,
                                accountType: savedAccountType,
                                workDirection: savedWorkDirection,
                                name: savedName,
                                country: savedCountry,
                                dep: savedDep,
                                dep2: savedDep2,
                                directManager: savedDirectManager,
                                deletionFilter: savedDeletionFilter);
                          }

                          await Future.delayed(
                              const Duration(milliseconds: 1000));

                          // if failed,use refreshFailed()
                          _refreshController2.refreshCompleted();
                        }
                      },
                    )
                  ],
                );
              } else if (state is HrDelete) {
                return Stack(
                  children: [
                    Stack(
                      children: [
                        SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 90, 20, 20.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                EmployeesAccountsSearchWidget(
                                  inputStartDate: savedStartDate,
                                  inputEndDate: savedEndDate,
                                  inputAccountId: savedAccountType,
                                  inputWorkDirection: savedWorkDirection,
                                  inputName: savedName,
                                  inputCountry: savedCountry,
                                  inputDep: savedDep,
                                  inputDep2: savedDep2,
                                  inputDirectManager: savedDirectManager,
                                  inputDeletionFilter: savedDeletionFilter,
                                  type: widget.type,
                                  onSubmit: (selectedStartDate,
                                      selectedEndDate,
                                      selectedAccountType,
                                      selectedWorkDirection,
                                      selectedName,
                                      selectedCountry,
                                      selectedDep,
                                      selectedDep2,
                                      selectedDirectManager,
                                      selectedDeletionFilter) {
                                    savedStartDate = selectedStartDate;
                                    savedEndDate = selectedEndDate;
                                    savedAccountType = selectedAccountType;
                                    savedWorkDirection = selectedWorkDirection;
                                    savedName = selectedName;
                                    savedCountry = selectedCountry;
                                    savedDep = selectedDep;
                                    savedDep2 = selectedDep2;
                                    savedDirectManager = selectedDirectManager;
                                    savedDeletionFilter = selectedDeletionFilter;
                                    if(widget.type == 0){
                                      context
                                          .read<HrCubit>()
                                          .getEmpAccList(context, startDate: savedStartDate,
                                          endDate: savedEndDate,
                                          accountType: savedAccountType,
                                          workDirection: savedWorkDirection,
                                          name: savedName,
                                          country: savedCountry,
                                          dep: savedDep,
                                          dep2: savedDep2,
                                          directManager: savedDirectManager,
                                          deletionFilter: savedDeletionFilter);
                                    }else{
                                      context
                                          .read<HrCubit>()
                                          .getEmpAccExitList(context, startDate: savedStartDate,
                                          endDate: savedEndDate,
                                          accountType: savedAccountType,
                                          workDirection: savedWorkDirection,
                                          name: savedName,
                                          country: savedCountry,
                                          dep: savedDep,
                                          dep2: savedDep2,
                                          directManager: savedDirectManager,
                                          deletionFilter: savedDeletionFilter);
                                    }
                                  },
                                ),
                                CustomButton(
                                  onPressed: () {
                                    goto(context, DownloadEmployeeAccountPdfPage(type: widget.type,));
                                  },
                                  isTextCentered: true,
                                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 0.0),
                                  backColor: AppColors.primary,
                                  borderRadius: 15,
                                  innerPadding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  title: 'print'.tr(),
                                  textStyle: AppStyles.aleoSeventeenWhiteBold,
                                ),
                                CustomButton(
                                  onPressed: () {
                                    goto(context, DownloadEmployeeAccountsXlsPage(type: widget.type,));
                                  },
                                  isTextCentered: true,
                                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 0.0),
                                  backColor: AppColors.primary,
                                  borderRadius: 15,
                                  innerPadding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  title: 'export_excel'.tr(),
                                  textStyle: AppStyles.aleoSeventeenWhiteBold,
                                ),
                                DynamicHeightGridView(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: state.data.length,
                                  crossAxisSpacing: 4,
                                  crossAxisCount: 2,
                                  mainAxisSpacing: 2,
                                  builder: (context, index) {
                                    return EmployeeAccountItem(
                                      data: state.data[index],
                                      type: widget.type,
                                      onDeleteEmployee: () {
                                        context.read<HrCubit>().deleteEmployee(
                                            context, state.data[index].id!);
                                      },
                                      onDismissEmployee: () {
                                        context.read<HrCubit>().dismissalEmployee(
                                            context, state.data[index].id!);
                                      },
                                      onReemploymentEmployee: (){
                                        context.read<HrCubit>().reemployeeEmployee(
                                            context, state.data[index].id!);
                                      },
                                      onEditPressed: () async{
                                        var data = await gotoData(context, AddEmplyoeePage(1, state.data[index]));
                                        if(data != null && data){
                                          context.read<HrCubit>().page = 1;
                                          savedStartDate = "";
                                          savedEndDate = "";
                                          savedAccountType = 0;
                                          savedWorkDirection = "";
                                          savedName = "";
                                          savedCountry = -1;
                                          savedDep = 0;
                                          savedDep2 = 0;
                                          savedDirectManager = 0;
                                          savedDeletionFilter = "";
                                          // monitor network fetch

                                          if (widget.type == 0) {
                                            context.read<HrCubit>().getEmpAccList(context,
                                                startDate: savedStartDate,
                                                endDate: savedEndDate,
                                                accountType: savedAccountType,
                                                workDirection: savedWorkDirection,
                                                name: savedName,
                                                country: savedCountry,
                                                dep: savedDep,
                                                dep2: savedDep2,
                                                directManager: savedDirectManager,
                                                deletionFilter: savedDeletionFilter);
                                          } else {
                                            context
                                                .read<HrCubit>()
                                                .getEmpAccExitList(context,
                                                startDate: savedStartDate,
                                                endDate: savedEndDate,
                                                accountType: savedAccountType,
                                                workDirection: savedWorkDirection,
                                                name: savedName,
                                                country: savedCountry,
                                                dep: savedDep,
                                                dep2: savedDep2,
                                                directManager: savedDirectManager,
                                                deletionFilter: savedDeletionFilter);
                                          }
                                          await Future.delayed(
                                              const Duration(milliseconds: 1000));
                                          _refreshController2.refreshCompleted();
                                        }
                                      },
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                        App_Bar(
                          title: widget.title,
                          LeadingIcon: Icons.keyboard_arrow_right,
                          LeadingOnClick: () {
                            goBack(context);
                          },
                          actionIcon: widget.type == 0 ? Icons.add : null,
                          actionClick: () async{
                            var data = await gotoData(
                                context,
                                AddEmplyoeePage(
                                    0,
                                    EmployeeAccountModel(
                                        type: '',
                                        finalPlaces: [],
                                        name: '',
                                        image: '',
                                        email: '',
                                        id: 0,
                                        activity: '',
                                        bn: '',
                                        cardFrom: 0,
                                        cardTo: 0,
                                        confirmCode: 0,
                                        daysOff: '',
                                        daysOn: '',
                                        deletedAt: '',
                                        department1: '',
                                        department2: '',
                                        department: '',
                                        departmentId2: 0,
                                        departmentId: 0,
                                        dismissalDate: '',
                                        dismissalId: 0,
                                        endAutoTime: '',
                                        endWorkTime: '',
                                        id2: 0,
                                        idNumber: '',
                                        isAdmin: 0,
                                        isConfirmed: 0,
                                        isOpen: 0,
                                        managerId: 0,
                                        managerName: '',
                                        mobile: '',
                                        note: '',
                                        position: '',
                                        sendEmail: '',
                                        sendSms: '',
                                        sendWhatsapp: '',
                                        signature: '',
                                        startAutoTime: '',
                                        startWorkTime: '',
                                        statusId: 0,
                                        country: "",
                                        violationOnOff: "غير مفعل")));
                            if(data != null && data){
                              context.read<HrCubit>().page = 1;
                              savedStartDate = "";
                              savedEndDate = "";
                              savedAccountType = 0;
                              savedWorkDirection = "";
                              savedName = "";
                              savedCountry = -1;
                              savedDep = 0;
                              savedDep2 = 0;
                              savedDirectManager = 0;
                              savedDeletionFilter = "";
                              // monitor network fetch

                              if (widget.type == 0) {
                                context.read<HrCubit>().getEmpAccList(context,
                                    startDate: savedStartDate,
                                    endDate: savedEndDate,
                                    accountType: savedAccountType,
                                    workDirection: savedWorkDirection,
                                    name: savedName,
                                    country: savedCountry,
                                    dep: savedDep,
                                    dep2: savedDep2,
                                    directManager: savedDirectManager,
                                    deletionFilter: savedDeletionFilter);
                              } else {
                                context
                                    .read<HrCubit>()
                                    .getEmpAccExitList(context,
                                    startDate: savedStartDate,
                                    endDate: savedEndDate,
                                    accountType: savedAccountType,
                                    workDirection: savedWorkDirection,
                                    name: savedName,
                                    country: savedCountry,
                                    dep: savedDep,
                                    dep2: savedDep2,
                                    directManager: savedDirectManager,
                                    deletionFilter: savedDeletionFilter);
                              }

                              await Future.delayed(
                                  const Duration(milliseconds: 1000));

                              // if failed,use refreshFailed()
                              _refreshController2.refreshCompleted();
                            }
                          },
                        )
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      color: Colors.grey.withOpacity(0.4),
                      child: const Center(child: CircularProgressIndicator()),
                    )
                  ],
                );
              } else {
                return const CircularProgressIndicator();
              }
            },
          ),
        ));
  }
}
