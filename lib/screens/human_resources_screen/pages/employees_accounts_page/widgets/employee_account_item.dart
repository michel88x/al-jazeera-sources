import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_images.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/network/urls.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employee_account_model.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/add_employee_page/view/add_employee_page.dart';
import 'package:flutter/material.dart';

class EmployeeAccountItem extends StatelessWidget {
  final EmployeeAccountModel data;
  final int type;
  final VoidCallback onDeleteEmployee; //context.read<HrCubit>().deleteEmployee(context, state.data[index].id);
  final VoidCallback onDismissEmployee; //context.read<HrCubit>().dismissalEmployee(context, state.data[index].id);
  final VoidCallback onReemploymentEmployee;
  final VoidCallback onEditPressed;

  const EmployeeAccountItem({Key? key, required this.data,required this.type, required this.onDeleteEmployee,required this.onDismissEmployee, required this.onReemploymentEmployee,
  required this.onEditPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: data.deletedAt != '-'
                  ? AppColors.glowingRed.withOpacity(0.3)
                  : Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black.withOpacity(0.4),
                    spreadRadius: 0,
                    blurRadius: 2)
              ]),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                data.image == null
                    ? Stack(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 140,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white,
                              boxShadow: const [
                                BoxShadow(
                                    color: Colors.grey,
                                    spreadRadius: 1,
                                    blurRadius: 5)
                              ],
                            ),
                            child: const Icon(
                              Icons.person_outline_rounded,
                            ),
                          ),
                          data.deletedAt != '-' && type == 1
                              ? PopupMenuButton<int>(
                                  padding: EdgeInsets.zero,
                                  //  key: _key,
                                  onSelected: (inde) {
                                    onReemploymentEmployee();
                                  },
                                  itemBuilder: (context) {
                                    return <PopupMenuEntry<int>>[
                                      PopupMenuItem(
                                          value: 0,
                                          child: Text('reemployment'.tr())),
                                    ];
                                  },
                                )
                              : data.deletedAt != '-' && type == 0?
                          Container() :
                          buildPopupMenuButton(context),
                        ],
                      )
                    : Stack(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 140,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Colors.grey,
                                      spreadRadius: 1,
                                      blurRadius: 5)
                                ],
                                image: data.image == null
                                    ? const DecorationImage(
                                        image: AssetImage(AppImages.user),
                                        fit: BoxFit.cover)
                                    : DecorationImage(
                                        image: NetworkImage(data.image!),
                                        fit: BoxFit.cover)),
                          ),
                          data.deletedAt != '-' && type == 1
                              ? PopupMenuButton<int>(
                                  padding: EdgeInsets.zero,
                                  //  key: _key,
                                  onSelected: (inde) {
                                    onReemploymentEmployee();
                                  },
                                  itemBuilder: (context) {
                                    return <PopupMenuEntry<int>>[
                                      PopupMenuItem(
                                          value: 0,
                                          child: Text('reemployment'.tr())),
                                    ];
                                  },
                                )
                              :
                          data.deletedAt != '-' && type == 0?
                              Container() :
                          buildPopupMenuButton(context),
                        ],
                      ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 15, 8, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(top: 6,),
                        child: Icon(Icons.person,
                            color: AppColors.secondary, size: 15),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: Text(
                          data.name!,
                          style: AppStyles.cairoTwelveBold,
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
                    child: Row(
                      children: [
                        data.type == 'system_admin'.tr()
                            ? const Icon(Icons.ac_unit_sharp,
                                color: AppColors.secondary, size: 15)
                            : data.type == 'employee2'.tr()
                                ? const Icon(Icons.account_circle_outlined,
                                    color: AppColors.primary, size: 15)
                                : const Icon(Icons.line_style_sharp,
                                    color: Colors.green, size: 15),
                        const SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: Text(
                            data.type!,
                            style: AppStyles.cairoTwelveGrey,
                          ),
                        )
                      ],
                    )),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 5, 8, 0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.fromLTRB(0, 6, 0, 0),
                        child: Icon(Icons.email_outlined,
                            color: AppColors.secondary, size: 15),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: Text(
                          data.email!,
                          style: AppStyles.cairoTwelve,
                        ),
                      )
                    ],
                  ),
                ),
                data.mobile == null
                    ? const Visibility(
                        visible: false,
                        child: Text(''),
                      )
                    : Padding(
                        padding: const EdgeInsets.fromLTRB(8.0, 5, 8, 0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.fromLTRB(0, 6, 0, 0),
                              child: Icon(Icons.phone_android,
                                  color: AppColors.secondary, size: 15),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: Text(
                                data.mobile!,
                                style: AppStyles.cairoTwelve,
                              ),
                            )
                          ],
                        ),
                      ),
                data.idNumber == ''
                    ? const Visibility(
                        visible: false,
                        child: Text(''),
                      )
                    : data.idNumber == null
                        ? const Visibility(
                            visible: false,
                            child: Text(''),
                          )
                        : data.activity == null
                            ? const Visibility(
                                visible: false,
                                child: Text(''),
                              )
                            : Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(8.0, 5, 8, 0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const Padding(
                                      padding: EdgeInsets.fromLTRB(0, 6, 0, 0),
                                      child: Icon(Icons.card_travel,
                                          color: AppColors.secondary, size: 15),
                                    ),
                                    const SizedBox(
                                      width: 5,
                                    ),
                                    Expanded(
                                      child: Text(
                                        data.activity!,
                                        style: AppStyles.cairoTwelve,
                                      ),
                                    )
                                  ],
                                ),
                              ),
              ],
            ),
          )),
    );
  }

  PopupMenuButton<int> buildPopupMenuButton(BuildContext context) {
    return PopupMenuButton<int>(
      padding: EdgeInsets.zero,
      //  key: _key,
      onSelected: (inde) {
        // if (inde == 0) {
        //   goto(context, EmlpyeeDetailsPage(data.id.toString(), data.name!));
        // } else
          if (inde == 1) {
          onEditPressed();
        } else if (inde == 2) {
            onDismissEmployee();
        }
        //   else if (inde == 3) {
        //   onDismissEmployee();
        // }
      },
      itemBuilder: (context) {
        return <PopupMenuEntry<int>>[
          //PopupMenuItem(value: 0, child: Text('view'.tr())),
          PopupMenuItem(value: 1, child: Text('edit'.tr())),
          PopupMenuItem(value: 2, child: Text('fire'.tr())),
          //PopupMenuItem(value: 3, child: Text('fire'.tr())),
          //PopupMenuItem(value: 4, child: Text('print_info'.tr())),
        ];
      },
    );
  }
}
