part of 'employees_accounts_search_bloc.dart';

@immutable
abstract class EmployeesAccountsSearchEvent {}

class GetEmployeeAccountSearchLists extends EmployeesAccountsSearchEvent{
  final String? inputStartDate;
  final String? inputEndDate;
  final int? inputAccountId;
  final String? inputWorkDirection;
  final String? inputName;
  final int? inputCountry;
  final int? inputDep;
  final int? inputDep2;
  final int? inputDirectManager;
  final String? inputDeletionFilter;
  final int type;

  GetEmployeeAccountSearchLists({
    this.inputStartDate,
    this.inputEndDate,
    this.inputAccountId,
    this.inputWorkDirection,
    this.inputName,
    this.inputCountry,
    this.inputDep,
    this.inputDep2,
    this.inputDirectManager,
    this.inputDeletionFilter,
    this.type = 0
  });
}
