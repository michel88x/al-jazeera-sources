part of 'employees_accounts_search_bloc.dart';

@immutable
abstract class EmployeesAccountsSearchState {}

class EmployeesAccountsSearchInitial extends EmployeesAccountsSearchState {}

class EmployeesAccountsSearchLoading extends EmployeesAccountsSearchState {}

class EmployeesAccountsSearchError extends EmployeesAccountsSearchState{}

class EmployeesAccountsSearchEmpty extends EmployeesAccountsSearchState{}

class EmployeesAccountsSearchSuccess extends EmployeesAccountsSearchState{
  final List<FilterObject>? accountTypes;
  final List<FilterObject>? workDirections;
  final List<FilterObject>? names;
  final List<FilterObject>? countries;
  final List<FilterObject>? departments;
  final List<FilterObject>? departments2;
  final List<FilterObject>? directManagers;
  final List<FilterObject>? deletionFilters;

  EmployeesAccountsSearchSuccess({
    this.accountTypes,
    this.workDirections,
    this.names,
    this.countries,
    this.departments,
    this.departments2,
    this.directManagers,
    this.deletionFilters
  });
}
