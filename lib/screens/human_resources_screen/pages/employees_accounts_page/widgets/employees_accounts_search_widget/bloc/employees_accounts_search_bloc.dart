
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/constants/enums.dart';
import 'package:employee/core/network/models/base_list_response.dart';
import 'package:employee/core/network/models/s_base_response.dart';
import 'package:employee/core/network/urls.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employee_account_model.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/models/employee_account_search_account_types_object.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/models/employee_account_search_account_types_response.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/models/employee_account_search_departments_response.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/models/employee_account_search_departmets_object.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/models/employee_account_search_names_object.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/models/employee_account_search_names_response.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/models/filter_object.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

part 'employees_accounts_search_event.dart';
part 'employees_accounts_search_state.dart';

class EmployeesAccountsSearchBloc extends Bloc<EmployeesAccountsSearchEvent, EmployeesAccountsSearchState> {

  int numErrors = 0;
  FilterObject? selectedAccountType;
  FilterObject? selectedWorkDirection;
  FilterObject? selectedName;
  FilterObject? selectedCountry;
  FilterObject? selectedDepartment;
  FilterObject? selectedDepartment2;
  FilterObject? selectedDirectManager;
  FilterObject? selectedDeletionFilter;
  String selectedStartDate = "";
  String selectedEndDate = "";
  TextEditingController startDateController = TextEditingController();
  TextEditingController endDateController = TextEditingController();

  EmployeesAccountsSearchBloc() : super(EmployeesAccountsSearchInitial()) {
    on<GetEmployeeAccountSearchLists>((event, emit) async{
      emit(EmployeesAccountsSearchLoading());
      List<FilterObject> accountTypes = <FilterObject>[
        FilterObject(
          id: 0,
          value: "please_select".tr()
        )
      ];
      selectedAccountType = accountTypes[0];
      List<FilterObject> workDirections = <FilterObject>[
        FilterObject(
            id: 0,
            value: "please_select".tr()
        )
      ];
      selectedWorkDirection = workDirections[0];
      List<FilterObject> names = <FilterObject>[
        FilterObject(
            id: 0,
            value: "please_select".tr(),
            stringValue: true
        )
      ];
      selectedName = names[0];
      List<FilterObject> countries = <FilterObject>[
        FilterObject(
            id: -1,
            value: "please_select".tr()
        ),
        FilterObject(
            value: "المملكة العربية السعودية",
            id: 0
        ),
        FilterObject(
            value: "مصر",
            id: 1
        )
      ];
      selectedCountry = countries[0];
      List<FilterObject> departments = <FilterObject>[
        FilterObject(
            id: 0,
            value: "please_select".tr()
        )
      ];
      selectedDepartment = departments[0];
      List<FilterObject> relatedDepartments = <FilterObject>[
        FilterObject(
            id: 0,
            value: "please_select".tr()
        )
      ];
      selectedDepartment2 = relatedDepartments[0];
      List<FilterObject> directManagers = <FilterObject>[
        FilterObject(
            id: 0,
            value: "please_select".tr()
        )
      ];
      selectedDirectManager = directManagers[0];
      List<FilterObject> deletionFilters = <FilterObject>[
        FilterObject(
            id: 0,
            value: "please_select".tr(),
            stringValue: true
        )
      ];
      selectedDeletionFilter = deletionFilters[0];
      List<FilterObject> englishNames = <FilterObject>[];
      List<FilterObject> arabicNames = <FilterObject>[];
      String? token = await getToken();
      BaseListResponse? response = await callListAPI(method: APIMethod.get,
          accessToken: token,
          url: event.type == 0? Urls.allEmployeesAccounts : "${Urls.allEmployeesAccounts}&dismissal=1");
      if(response != null && response.success == true){
        List<EmployeeAccountModel> l = json
            .decode(json.encode(response.data))
            .map<EmployeeAccountModel>((x) => EmployeeAccountModel.fromJson(x))
            .toList();
        if(l.isNotEmpty){
          int i = 1;
          for(EmployeeAccountModel eam in l){
            if(accountTypes.where((element) => element.value == eam.type).isEmpty){
              accountTypes.add(
                  FilterObject(id: eam.isAdmin!, value: eam.type!)
              );
            }
            if(workDirections.where((element) => element.value == eam.activity).isEmpty){
              workDirections.add(
                  FilterObject(id: eam.activityId, value: eam.activity!)
              );
            }
            String nameLanguage = detectLanguage(string: eam.name!);
            if(nameLanguage == "ar"){
              if(arabicNames.where((element) => element.value == eam.name).isEmpty){
                arabicNames.add(FilterObject(id: 1, value: eam.name!, stringValue: true));
              }
            }else{
              if(englishNames.where((element) => element.value == eam.name).isEmpty){
                englishNames.add(FilterObject(id: 1, value: eam.name!, stringValue: true));
              }
            }
            // if(names.where((element) => element.value == eam.name).isEmpty){
            //   names.add(
            //       FilterObject(id: 1, value: eam.name!, stringValue: true)
            //   );
            // }
            if(eam.departmentId != null && eam.department1 != null){
              if(departments.where((element) => element.value == eam.department1).isEmpty){
                departments.add(
                    FilterObject(id: eam.departmentId!, value: eam.department1!)
                );
              }
            }else if(eam.departmentId == null && eam.department1 != null){
              if(departments.where((element) => element.value == eam.department1).isEmpty){
                departments.add(
                    FilterObject(id: -2, value: eam.department1!)
                );
              }
            }
            if(eam.departmentId2 != null && eam.department2 != null){
              if(relatedDepartments.where((element) => element.value == eam.department2).isEmpty){
                relatedDepartments.add(
                    FilterObject(id: eam.departmentId2!, value: eam.department2!)
                );
              }
            }
            if(eam.managerId != null && eam.managerName != null){
              if(directManagers.where((element) => element.value == eam.managerName).isEmpty){
                directManagers.add(
                    FilterObject(id: eam.managerId!, value: eam.managerName!)
                );
              }
            }
            if(eam.deletedAt != null && eam.deletedAt!.isNotEmpty && eam.deletedAt! != "-"){
              if(deletionFilters.where((element) => element.value == eam.deletedAt).isEmpty){
                deletionFilters.add(
                    FilterObject(id: i ,value: eam.deletedAt!, stringValue: true)
                );
              }
            }
            i++;
          }
          if(englishNames.isNotEmpty){
            englishNames.sort((a,b) => a.value.compareTo(b.value));
            names.addAll(englishNames);
          }
          if(arabicNames.isNotEmpty){
            arabicNames.sort((a,b) => a.value.compareTo(b.value));
            names.addAll(arabicNames);
          }
          //Start date
          if(event.inputStartDate != null && event.inputStartDate!.isNotEmpty){
            startDateController.text = event.inputStartDate!;
          }
          //End date
          if(event.inputEndDate != null && event.inputEndDate!.isNotEmpty){
            endDateController.text = event.inputEndDate!;
          }
          //Account type
          int j = -1;
          if(event.inputAccountId != null && event.inputAccountId! != 0){
            for(int i=0;i<accountTypes.length;i++){
              if(accountTypes[i].id == event.inputAccountId){
                j = i;
                break;
              }
            }
            if(j != -1){
              selectedAccountType = accountTypes[j];
            }
          }
          //Work direction
          if(event.inputWorkDirection != null && event.inputWorkDirection!.isNotEmpty){
            j = -1;
            for(int i=0;i<workDirections.length;i++){
              if(workDirections[i].id.toString() == event.inputWorkDirection){
                j = i;
                break;
              }
            }
            if(j != -1){
              selectedWorkDirection = workDirections[j];
            }
          }
          //Name
          if(event.inputName != null && event.inputName!.isNotEmpty){
            j = -1;
            for(int i=0;i<names.length;i++){
              if(names[i].value == event.inputName){
                j = i;
                break;
              }
            }
            if(j != -1){
              selectedName = names[j];
            }
          }
          //Country
          if(event.inputCountry != null){
            j = -1;
            for(int i=0;i<countries.length;i++){
              if(countries[i].id == event.inputCountry){
                j = i;
                break;
              }
            }
            if(j != -1){
              selectedCountry = countries[j];
            }
          }
          //Department
          if(event.inputDep != null && event.inputDep! != 0){
            j = -1;
            for(int i=0;i<departments.length;i++){
              if(departments[i].id == event.inputDep){
                j = i;
                break;
              }
            }
            if(j != -1){
              selectedDepartment = departments[j];
            }
          }
          //Related department
          if(event.inputDep2 != null && event.inputDep2! != 0){
            j = -1;
            for(int i=0;i<relatedDepartments.length;i++){
              if(relatedDepartments[i].id == event.inputDep2){
                j = i;
                break;
              }
            }
            if(j != -1){
              selectedDepartment2 = relatedDepartments[j];
            }
          }
          //Related department
          if(event.inputDep2 != null && event.inputDep2! != 0){
            j = -1;
            for(int i=0;i<relatedDepartments.length;i++){
              if(relatedDepartments[i].id == event.inputDep2){
                j = i;
                break;
              }
            }
            if(j != -1){
              selectedDepartment2 = relatedDepartments[j];
            }
          }
          //Direct manager
          if(event.inputDirectManager != null && event.inputDirectManager! != 0){
            j = -1;
            for(int i=0;i<directManagers.length;i++){
              if(directManagers[i].id == event.inputDirectManager){
                j = i;
                break;
              }
            }
            if(j != -1){
              selectedDirectManager = directManagers[j];
            }
          }
          //Deletion filter
          if(event.inputDeletionFilter != null && event.inputDeletionFilter!.isNotEmpty){
            j = -1;
            for(int i=0;i<deletionFilters.length;i++){
              if(deletionFilters[i].value == event.inputDeletionFilter){
                j = i;
                break;
              }
            }
            if(j != -1){
              selectedDeletionFilter = deletionFilters[j];
            }
          }
          emit(EmployeesAccountsSearchSuccess(
              accountTypes: accountTypes,
              workDirections: workDirections,
              names: names,
              countries: countries,
              departments: departments,
              departments2: relatedDepartments,
              directManagers: directManagers,
              deletionFilters: deletionFilters
          ));
        }else{
          emit(EmployeesAccountsSearchEmpty());
        }
      }else{
        emit(EmployeesAccountsSearchError());
      }
    });
  }
}
