import 'package:datetime_picker_formfield_new/datetime_picker_formfield.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/base_widgets/custom_button.dart';
import 'package:employee/core/base_widgets/loader.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/add_employee_page/widgets/title_add_page.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/bloc/employees_accounts_search_bloc.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/models/filter_object.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EmployeesAccountsSearchWidget extends StatefulWidget {
  final Function(String, String, int, String, String, int, int, int, int, String) onSubmit;
  final String? inputStartDate;
  final String? inputEndDate;
  final int? inputAccountId;
  final String? inputWorkDirection;
  final String? inputName;
  final int? inputCountry;
  final int? inputDep;
  final int? inputDep2;
  final int? inputDirectManager;
  final String? inputDeletionFilter;
  final int type;

  EmployeesAccountsSearchWidget({
    Key? key,
    required this.onSubmit,
    this.inputStartDate,
    this.inputEndDate,
    this.inputAccountId,
    this.inputWorkDirection,
    this.inputName,
    this.inputCountry,
    this.inputDep,
    this.inputDep2,
    this.inputDirectManager,
    this.inputDeletionFilter,
    this.type = 0
  })
      : super(key: key);

  @override
  State<EmployeesAccountsSearchWidget> createState() =>
      _EmployeesAccountsSearchWidgetState();
}

class _EmployeesAccountsSearchWidgetState
    extends State<EmployeesAccountsSearchWidget> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          EmployeesAccountsSearchBloc()..add(GetEmployeeAccountSearchLists(
            inputStartDate: widget.inputStartDate,
            inputEndDate: widget.inputEndDate,
            inputAccountId: widget.inputAccountId,
            inputWorkDirection: widget.inputWorkDirection,
            inputName: widget.inputName,
            inputCountry: widget.inputCountry,
            inputDep: widget.inputDep,
            inputDep2: widget.inputDep2,
            inputDirectManager: widget.inputDirectManager,
            inputDeletionFilter: widget.inputDeletionFilter,
            type: widget.type
          )),
      child: Builder(builder: (context) {
        return BlocBuilder(
            bloc: context.read<EmployeesAccountsSearchBloc>(),
            builder: (context, state) {
              if (state is EmployeesAccountsSearchLoading) {
                return Container(
                  width: double.infinity,
                  height: 200,
                  margin: const EdgeInsets.only(top: 40, left: 10, right: 10),
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.4),
                            spreadRadius: 0,
                            blurRadius: 2)
                      ]),
                  child: const Center(
                    child: Loader(),
                  ),
                );
              } else if (state is EmployeesAccountsSearchError ||
                  state is EmployeesAccountsSearchEmpty) {
                return Container();
              } else if (state is EmployeesAccountsSearchSuccess) {
                return Container(
                  width: double.infinity,
                  margin: const EdgeInsets.only(top: 40, left: 10, right: 10),
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.4),
                            spreadRadius: 0,
                            blurRadius: 2)
                      ]),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        "search_options".tr(),
                        textAlign: TextAlign.right,
                        style: AppStyles.cairoFourteen,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TitleAddPage("from_start_date".tr()),
                      const SizedBox(
                        height: 10,
                      ),
                      DateTimeField(
                        format: DateFormat("yyyy-MM-dd"),
                        controller: context
                            .read<EmployeesAccountsSearchBloc>()
                            .startDateController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1)),
                            contentPadding: const EdgeInsets.all(10)),
                        onShowPicker: (context, currentValue) {
                          return showDatePicker(
                            context: context,
                            firstDate: DateTime(1900),
                            initialDate: currentValue ?? DateTime.now(),
                            lastDate: DateTime(2100),
                          );
                        },
                      ),
                      TitleAddPage("to_start_date".tr()),
                      const SizedBox(
                        height: 10,
                      ),
                      DateTimeField(
                        format: DateFormat("yyyy-MM-dd"),
                        controller: context
                            .read<EmployeesAccountsSearchBloc>()
                            .endDateController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10),
                                borderSide: const BorderSide(
                                    color: Colors.grey, width: 1)),
                            contentPadding: const EdgeInsets.all(10)),
                        onShowPicker: (context, currentValue) {
                          return showDatePicker(
                            context: context,
                            firstDate: DateTime(1900),
                            initialDate: currentValue ?? DateTime.now(),
                            lastDate: DateTime(2100),
                          );
                        },
                      ),
                      if (state.accountTypes != null &&
                          state.accountTypes!.isNotEmpty)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            TitleAddPage("account_type".tr()),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border:
                                    Border.all(color: Colors.grey, width: 1),
                                color: Colors.white,
                              ),
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Center(
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      isExpanded: true,
                                      isDense: true,
                                      hint: Text(
                                        context
                                            .read<EmployeesAccountsSearchBloc>()
                                            .selectedAccountType!
                                            .value,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Theme.of(context).hintColor,
                                        ),
                                      ),
                                      items: state.accountTypes!
                                          .map((item) => DropdownMenuItem<
                                                  FilterObject>(
                                                value: item,
                                                child: Text(
                                                  item.value,
                                                  style: const TextStyle(
                                                    fontSize: 14,
                                                  ),
                                                ),
                                              ))
                                          .toList(),
                                      value: context
                                          .read<EmployeesAccountsSearchBloc>()
                                          .selectedAccountType,
                                      onChanged: (value) {
                                        setState(() {
                                          context
                                              .read<
                                                  EmployeesAccountsSearchBloc>()
                                              .selectedAccountType = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      if (state.workDirections != null && state.workDirections!.isNotEmpty)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            TitleAddPage("job_direction".tr()),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                border:
                                Border.all(color: Colors.grey, width: 1),
                              ),
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Padding(
                                padding:
                                const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Center(
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      isExpanded: true,
                                      isDense: true,
                                      hint: Text(
                                        context
                                            .read<EmployeesAccountsSearchBloc>()
                                            .selectedWorkDirection!
                                            .value,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Theme.of(context).hintColor,
                                        ),
                                      ),
                                      items: state.workDirections!
                                          .map((item) => DropdownMenuItem<
                                          FilterObject>(
                                        value: item,
                                        child: Text(
                                          item.value,
                                          style: const TextStyle(
                                            fontSize: 14,
                                          ),
                                        ),
                                      ))
                                          .toList(),
                                      value: context
                                          .read<EmployeesAccountsSearchBloc>()
                                          .selectedWorkDirection,
                                      onChanged: (value) {
                                        setState(() {
                                          context
                                              .read<
                                              EmployeesAccountsSearchBloc>()
                                              .selectedWorkDirection = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      if (state.names != null && state.names!.isNotEmpty)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            TitleAddPage("name".tr()),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                border:
                                Border.all(color: Colors.grey, width: 1),
                              ),
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Padding(
                                padding:
                                const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Center(
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      isExpanded: true,
                                      isDense: true,
                                      hint: Text(
                                        context
                                            .read<EmployeesAccountsSearchBloc>()
                                            .selectedName!
                                            .value,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Theme.of(context).hintColor,
                                        ),
                                      ),
                                      items: state.names!
                                          .map((item) => DropdownMenuItem<
                                          FilterObject>(
                                        value: item,
                                        child: Text(
                                          item.value!,
                                          style: const TextStyle(
                                            fontSize: 14,
                                          ),
                                        ),
                                      ))
                                          .toList(),
                                      value: context
                                          .read<EmployeesAccountsSearchBloc>()
                                          .selectedName,
                                      onChanged: (value) {
                                        setState(() {
                                          context
                                              .read<
                                              EmployeesAccountsSearchBloc>()
                                              .selectedName = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      if (state.countries != null && state.countries!.isNotEmpty)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            TitleAddPage("country".tr()),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                border:
                                Border.all(color: Colors.grey, width: 1),
                              ),
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Padding(
                                padding:
                                const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Center(
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      isExpanded: true,
                                      isDense: true,
                                      hint: Text(
                                        context
                                            .read<EmployeesAccountsSearchBloc>()
                                            .selectedCountry!
                                            .value,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Theme.of(context).hintColor,
                                        ),
                                      ),
                                      items: state.countries!
                                          .map((item) => DropdownMenuItem<
                                          FilterObject>(
                                        value: item,
                                        child: Text(
                                          item.value,
                                          style: const TextStyle(
                                            fontSize: 14,
                                          ),
                                        ),
                                      ))
                                          .toList(),
                                      value: context
                                          .read<EmployeesAccountsSearchBloc>()
                                          .selectedCountry,
                                      onChanged: (value) {
                                        setState(() {
                                          context
                                              .read<
                                              EmployeesAccountsSearchBloc>()
                                              .selectedCountry = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      if (state.departments != null && state.departments!.isNotEmpty)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            TitleAddPage("section".tr()),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                border:
                                    Border.all(color: Colors.grey, width: 1),
                              ),
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Center(
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      isExpanded: true,
                                      isDense: true,
                                      hint: Text(
                                        context
                                            .read<EmployeesAccountsSearchBloc>()
                                            .selectedDepartment!
                                            .value,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Theme.of(context).hintColor,
                                        ),
                                      ),
                                      items: state.departments!
                                          .map((item) => DropdownMenuItem<
                                                  FilterObject>(
                                                value: item,
                                                child: Text(
                                                  item.value!,
                                                  style: const TextStyle(
                                                    fontSize: 14,
                                                  ),
                                                ),
                                              ))
                                          .toList(),
                                      value: context
                                          .read<EmployeesAccountsSearchBloc>()
                                          .selectedDepartment,
                                      onChanged: (value) {
                                        setState(() {
                                          context
                                              .read<
                                                  EmployeesAccountsSearchBloc>()
                                              .selectedDepartment = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            TitleAddPage("related_section".tr()),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                border:
                                    Border.all(color: Colors.grey, width: 1),
                              ),
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Center(
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      isExpanded: true,
                                      isDense: true,
                                      hint: Text(
                                        context
                                            .read<EmployeesAccountsSearchBloc>()
                                            .selectedDepartment2!
                                            .value,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Theme.of(context).hintColor,
                                        ),
                                      ),
                                      items: state.departments2!
                                          .map((item) => DropdownMenuItem<
                                                  FilterObject>(
                                                value: item,
                                                child: Text(
                                                  item.value!,
                                                  style: const TextStyle(
                                                    fontSize: 14,
                                                  ),
                                                ),
                                              ))
                                          .toList(),
                                      value: context
                                          .read<EmployeesAccountsSearchBloc>()
                                          .selectedDepartment2,
                                      onChanged: (value) {
                                        setState(() {
                                          context
                                              .read<
                                                  EmployeesAccountsSearchBloc>()
                                              .selectedDepartment2 = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      if (state.directManagers != null && state.directManagers!.isNotEmpty)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            TitleAddPage("direct_boss".tr()),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                border:
                                Border.all(color: Colors.grey, width: 1),
                              ),
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Padding(
                                padding:
                                const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Center(
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      isExpanded: true,
                                      isDense: true,
                                      hint: Text(
                                        context
                                            .read<EmployeesAccountsSearchBloc>()
                                            .selectedDirectManager!
                                            .value,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Theme.of(context).hintColor,
                                        ),
                                      ),
                                      items: state.directManagers!
                                          .map((item) => DropdownMenuItem<
                                          FilterObject>(
                                        value: item,
                                        child: Text(
                                          item.value,
                                          style: const TextStyle(
                                            fontSize: 14,
                                          ),
                                        ),
                                      ))
                                          .toList(),
                                      value: context
                                          .read<EmployeesAccountsSearchBloc>()
                                          .selectedDirectManager,
                                      onChanged: (value) {
                                        setState(() {
                                          context
                                              .read<
                                              EmployeesAccountsSearchBloc>()
                                              .selectedDirectManager = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      if (state.deletionFilters != null && state.deletionFilters!.isNotEmpty)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            TitleAddPage("deletion_filter".tr()),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                border:
                                Border.all(color: Colors.grey, width: 1),
                              ),
                              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Padding(
                                padding:
                                const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Center(
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      isExpanded: true,
                                      isDense: true,
                                      hint: Text(
                                        context
                                            .read<EmployeesAccountsSearchBloc>()
                                            .selectedDeletionFilter!
                                            .value,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Theme.of(context).hintColor,
                                        ),
                                      ),
                                      items: state.deletionFilters!
                                          .map((item) => DropdownMenuItem<
                                          FilterObject>(
                                        value: item,
                                        child: Text(
                                          item.value,
                                          style: const TextStyle(
                                            fontSize: 14,
                                          ),
                                        ),
                                      ))
                                          .toList(),
                                      value: context
                                          .read<EmployeesAccountsSearchBloc>()
                                          .selectedDeletionFilter,
                                      onChanged: (value) {
                                        setState(() {
                                          context
                                              .read<
                                              EmployeesAccountsSearchBloc>()
                                              .selectedDeletionFilter = value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      CustomButton(
                        onPressed: () {
                          print("jdfdfhfirfejfiefuehf: ${context.read<EmployeesAccountsSearchBloc>().selectedCountry!.id}");
                          widget.onSubmit(
                              context.read<EmployeesAccountsSearchBloc>().startDateController.text,
                              context.read<EmployeesAccountsSearchBloc>().endDateController.text,
                              context.read<EmployeesAccountsSearchBloc>().selectedAccountType!.id,
                              context.read<EmployeesAccountsSearchBloc>().selectedWorkDirection!.id.toString(),
                              context.read<EmployeesAccountsSearchBloc>().selectedName!.value,
                              context.read<EmployeesAccountsSearchBloc>().selectedCountry!.id,
                              context.read<EmployeesAccountsSearchBloc>().selectedDepartment!.id,
                            context.read<EmployeesAccountsSearchBloc>().selectedDepartment2!.id,
                            context.read<EmployeesAccountsSearchBloc>().selectedDirectManager!.id,
                            context.read<EmployeesAccountsSearchBloc>().selectedDeletionFilter!.value,
                          );
                        },
                        isTextCentered: true,
                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0.0),
                        backColor: AppColors.primary,
                        borderRadius: 15,
                        innerPadding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                        title: 'search'.tr(),
                        textStyle: AppStyles.aleoSeventeenWhiteBold,
                      )
                    ],
                  ),
                );
              }
              return Container();
            });
      }),
    );
  }
}
