import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/models/employee_account_search_account_types_object.dart';

class EmployeeAccountSearchAccountTypesResponse {
  List<EmployeeAccountSearchAccountsTypesObject>? isAdmin;

  EmployeeAccountSearchAccountTypesResponse({this.isAdmin});

  EmployeeAccountSearchAccountTypesResponse.fromJson(
      Map<String, dynamic> json) {
    if (json['is_admin'] != null) {
      isAdmin = <EmployeeAccountSearchAccountsTypesObject>[];
      json['is_admin'].forEach((v) {
        isAdmin!.add(new EmployeeAccountSearchAccountsTypesObject.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.isAdmin != null) {
      data['is_admin'] = this.isAdmin!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}