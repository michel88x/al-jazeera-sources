class FilterObject{
  final dynamic id;
  final String value;
  final bool? stringValue;

  FilterObject({
    required this.id,
    required this.value,
    this.stringValue = false
  });
}