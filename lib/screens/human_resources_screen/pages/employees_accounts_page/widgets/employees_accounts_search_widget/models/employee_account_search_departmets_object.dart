class EmployeeAccountSearchDepartmentsObject {
  int? id;
  String? name;
  int? managerId;
  String? discountMng;
  String? createdAt;
  String? updatedAt;
  Null? deletedAt;
  int? userId;
  String? signature;
  String? departmentMonitor;
  String? discountMntr;
  String? managerName;

  EmployeeAccountSearchDepartmentsObject(
      {this.id,
        this.name,
        this.managerId,
        this.discountMng,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.userId,
        this.signature,
        this.departmentMonitor,
        this.discountMntr,
        this.managerName});

  EmployeeAccountSearchDepartmentsObject.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    managerId = json['manager_id'];
    discountMng = json['discount_mng'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    userId = json['user_id'];
    signature = json['signature'];
    departmentMonitor = json['department_monitor'];
    discountMntr = json['discount_mntr'];
    managerName = json['manager_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['manager_id'] = this.managerId;
    data['discount_mng'] = this.discountMng;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['user_id'] = this.userId;
    data['signature'] = this.signature;
    data['department_monitor'] = this.departmentMonitor;
    data['discount_mntr'] = this.discountMntr;
    data['manager_name'] = this.managerName;
    return data;
  }
}