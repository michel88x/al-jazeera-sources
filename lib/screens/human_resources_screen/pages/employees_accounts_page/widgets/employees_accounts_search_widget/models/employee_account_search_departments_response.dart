import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/models/employee_account_search_departmets_object.dart';

class EmployeeAccountSearchDepartmentsResponse {
  List<EmployeeAccountSearchDepartmentsObject>? departments;

  EmployeeAccountSearchDepartmentsResponse({this.departments});

  EmployeeAccountSearchDepartmentsResponse.fromJson(Map<String, dynamic> json) {
    if (json['departments'] != null) {
      departments = <EmployeeAccountSearchDepartmentsObject>[];
      json['departments'].forEach((v) {
        departments!.add(new EmployeeAccountSearchDepartmentsObject.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.departments != null) {
      data['departments'] = this.departments!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}