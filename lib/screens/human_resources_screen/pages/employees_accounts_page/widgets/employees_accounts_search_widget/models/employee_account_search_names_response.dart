import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/widgets/employees_accounts_search_widget/models/employee_account_search_names_object.dart';

class EmployeeAccountSearchNamesResponse {
  List<EmployeeAccountSearchNamesObject>? users;

  EmployeeAccountSearchNamesResponse({this.users});

  EmployeeAccountSearchNamesResponse.fromJson(Map<String, dynamic> json) {
    if (json['users'] != null) {
      users = <EmployeeAccountSearchNamesObject>[];
      json['users'].forEach((v) {
        users!.add(new EmployeeAccountSearchNamesObject.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.users != null) {
      data['users'] = this.users!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}