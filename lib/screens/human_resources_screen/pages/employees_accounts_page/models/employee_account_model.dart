import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employees_accounts_final_day_off_model.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employees_accounts_place_model.dart';

class EmployeeAccountModel {
  int? id;
  int? id2;
  String? type;
  String? name;
  String? email;
  int? isAdmin;
  int? statusId;
  String? mobile;
  String? bn;
  String? position;
  String? activity;
  String? activityId;
  String? department;
  String? note;
  String? emailVerifiedAt;
  int? isConfirmed;
  int? confirmCode;
  int? cardFrom;
  int? cardTo;
  String? image;
  String? image2;
  int? departmentId;
  String? createdAt;
  String? updatedAt;
  int? isOpen;
  String? faceToken;
  String? startWorkTime;
  String? endWorkTime;
  String? startAutoTime;
  String? endAutoTime;
  String? daysOff;
  String? deletedAt;
  String? codeTime;
  int? dismissalId;
  String? dismissalDate;
  int? managerId;
  int? departmentId2;
  String? managerName;
  String? daysOn;
  String? sendSms;
  String? sendEmail;
  String? sendWhatsapp;
  String? idNumber;
  String? signature;
  String? signature2;
  String? performanceValue;
  String? department2;
  String? department1;
  List<EmployeesAccountsPlaceModel>? finalPlaces;
  List<EmployeesAccountsFinalDayOffModel>? finalDaysOff;
  List<EmployeesAccountsFinalDayOffModel>? finalDaysOn;
  String? violationOnOff;
  dynamic isDelegate;
  String? country;

  EmployeeAccountModel(
      {this.id,
        this.id2,
        this.type,
        this.name,
        this.email,
        this.isAdmin,
        this.statusId,
        this.mobile,
        this.bn,
        this.position,
        this.activity,
        this.activityId,
        this.department,
        this.note,
        this.emailVerifiedAt,
        this.isConfirmed,
        this.confirmCode,
        this.cardFrom,
        this.cardTo,
        this.image,
        this.image2,
        this.departmentId,
        this.createdAt,
        this.updatedAt,
        this.isOpen,
        this.faceToken,
        this.startWorkTime,
        this.endWorkTime,
        this.startAutoTime,
        this.endAutoTime,
        this.daysOff,
        this.deletedAt,
        this.codeTime,
        this.dismissalId,
        this.dismissalDate,
        this.managerId,
        this.departmentId2,
        this.managerName,
        this.daysOn,
        this.sendSms,
        this.sendEmail,
        this.sendWhatsapp,
        this.idNumber,
        this.signature,
        this.signature2,
        this.performanceValue,
        this.department2,
        this.department1,
        this.finalPlaces,
        this.finalDaysOff,
        this.finalDaysOn,
        this.violationOnOff,
        this.isDelegate,
        this.country});

  EmployeeAccountModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    id2 = json['id2'];
    type = json['type'];
    name = json['name'];
    email = json['email'];
    isAdmin = json['is_admin'];
    statusId = json['status_id'];
    mobile = json['mobile'];
    bn = json['bn'];
    position = json['position'];
    activity = json['activity'];
    activityId = json['activity_id'];
    department = json['department'];
    note = json['note'];
    emailVerifiedAt = json['email_verified_at'];
    isConfirmed = json['is_confirmed'];
    confirmCode = json['confirm_code'];
    cardFrom = json['card_from'];
    cardTo = json['card_to'];
    image = json['image'];
    image2 = json['image2'];
    departmentId = json['department_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    isOpen = json['is_open'];
    faceToken = json['face_token'];
    startWorkTime = json['start_work_time'];
    endWorkTime = json['end_work_time'];
    startAutoTime = json['start_auto_time'];
    endAutoTime = json['end_auto_time'];
    daysOff = json['days_off'];
    deletedAt = json['deleted_at'];
    codeTime = json['code_time'];
    dismissalId = json['dismissal_id'];
    dismissalDate = json['dismissal_date'];
    managerId = json['manager_id'];
    departmentId2 = json['department_id2'];
    managerName = json['manager_name'];
    daysOn = json['days_on'];
    sendSms = json['send_sms'];
    sendEmail = json['send_email'];
    sendWhatsapp = json['send_whatsapp'];
    idNumber = json['id_number'];
    signature = json['signature'];
    signature2 = json['signature2'];
    performanceValue = json['performance_value'];
    department2 = json['department2'];
    department1 = json['department1'];
    if (json['final_places'] != null) {
      finalPlaces = <EmployeesAccountsPlaceModel>[];
      json['final_places'].forEach((v) {
        finalPlaces!.add(new EmployeesAccountsPlaceModel.fromJson(v));
      });
    }
    if (json['final_days_off'] != null) {
      finalDaysOff = <EmployeesAccountsFinalDayOffModel>[];
      json['final_days_off'].forEach((v) {
        finalDaysOff!.add(new EmployeesAccountsFinalDayOffModel.fromJson(v));
      });
    }
    if (json['final_days_on'] != null) {
      finalDaysOn = <EmployeesAccountsFinalDayOffModel>[];
      json['final_days_on'].forEach((v) {
        finalDaysOn!.add(new EmployeesAccountsFinalDayOffModel.fromJson(v));
      });
    }
    violationOnOff = json['violation_on_off'];
    isDelegate = json['is_delegate'];
    country = json['country'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id2'] = this.id2;
    data['type'] = this.type;
    data['name'] = this.name;
    data['email'] = this.email;
    data['is_admin'] = this.isAdmin;
    data['status_id'] = this.statusId;
    data['mobile'] = this.mobile;
    data['bn'] = this.bn;
    data['position'] = this.position;
    data['activity'] = this.activity;
    data['activity_id'] = this.activityId;
    data['department'] = this.department;
    data['note'] = this.note;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['is_confirmed'] = this.isConfirmed;
    data['confirm_code'] = this.confirmCode;
    data['card_from'] = this.cardFrom;
    data['card_to'] = this.cardTo;
    data['image'] = this.image;
    data['image2'] = this.image2;
    data['department_id'] = this.departmentId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['is_open'] = this.isOpen;
    data['face_token'] = this.faceToken;
    data['start_work_time'] = this.startWorkTime;
    data['end_work_time'] = this.endWorkTime;
    data['start_auto_time'] = this.startAutoTime;
    data['end_auto_time'] = this.endAutoTime;
    data['days_off'] = this.daysOff;
    data['deleted_at'] = this.deletedAt;
    data['code_time'] = this.codeTime;
    data['dismissal_id'] = this.dismissalId;
    data['dismissal_date'] = this.dismissalDate;
    data['manager_id'] = this.managerId;
    data['department_id2'] = this.departmentId2;
    data['manager_name'] = this.managerName;
    data['days_on'] = this.daysOn;
    data['send_sms'] = this.sendSms;
    data['send_email'] = this.sendEmail;
    data['send_whatsapp'] = this.sendWhatsapp;
    data['id_number'] = this.idNumber;
    data['signature'] = this.signature;
    data['signature2'] = this.signature2;
    data['performance_value'] = this.performanceValue;
    data['department2'] = this.department2;
    data['department1'] = this.department1;
    if (this.finalPlaces != null) {
      data['final_places'] = this.finalPlaces!.map((v) => v.toJson()).toList();
    }
    if (this.finalDaysOff != null) {
      data['final_days_off'] =
          this.finalDaysOff!.map((v) => v.toJson()).toList();
    }
    if (this.finalDaysOn != null) {
      data['final_days_on'] = this.finalDaysOn!.map((v) => v.toJson()).toList();
    }
    data['violation_on_off'] = this.violationOnOff;
    data['is_delegate'] = this.isDelegate;
    data['country'] = this.country;
    return data;
  }
}





