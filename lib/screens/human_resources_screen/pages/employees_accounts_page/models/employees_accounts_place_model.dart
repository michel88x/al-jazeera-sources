class EmployeesAccountsPlaceModel {
  int? id;
  double? lat;
  double? lng;
  int? rad;
  String? name;
  String? createdAt;
  String? updatedAt;
  String? deletedAt;
  String? createdDate;
  String? deletedDate;
  bool? selected;

  EmployeesAccountsPlaceModel(
      {this.id,
        this.lat,
        this.lng,
        this.rad,
        this.name,
        this.createdAt,
        this.updatedAt,
        this.deletedAt,
        this.createdDate,
        this.selected,
        this.deletedDate});

  EmployeesAccountsPlaceModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    lat = json['lat'];
    lng = json['lng'];
    rad = json['rad'];
    name = json['name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    createdDate = json['created_date'];
    deletedDate = json['deleted_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['rad'] = this.rad;
    data['name'] = this.name;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['created_date'] = this.createdDate;
    data['deleted_date'] = this.deletedDate;
    return data;
  }
}