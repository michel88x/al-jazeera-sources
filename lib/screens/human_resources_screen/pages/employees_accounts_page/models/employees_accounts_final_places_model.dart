import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employees_accounts_place_model.dart';

class EmployeesAccountsFinalPlacesModel {
  EmployeesAccountsPlaceModel? place;
  bool? selected;

  EmployeesAccountsFinalPlacesModel({this.place, this.selected});

  EmployeesAccountsFinalPlacesModel.fromJson(Map<String, dynamic> json) {
    place = json['place'] != null ? new EmployeesAccountsPlaceModel.fromJson(json['place']) : null;
    selected = json['selected'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.place != null) {
      data['place'] = this.place!.toJson();
    }
    data['selected'] = this.selected;
    return data;
  }
}