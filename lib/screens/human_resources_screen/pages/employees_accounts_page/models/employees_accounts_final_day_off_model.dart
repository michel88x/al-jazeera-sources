class EmployeesAccountsFinalDayOffModel {
  String? day;
  int? id;
  bool? selected;

  EmployeesAccountsFinalDayOffModel({this.day, this.id, this.selected});

  EmployeesAccountsFinalDayOffModel.fromJson(Map<String, dynamic> json) {
    day = json['day'];
    id = json['id'];
    selected = json['selected'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['day'] = this.day;
    data['id'] = this.id;
    data['selected'] = this.selected;
    return data;
  }
}