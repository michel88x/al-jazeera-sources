import 'dart:convert';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/base_widgets/loader.dart';
import 'package:employee/core/constants/enums.dart';
import 'package:employee/core/network/models/base_list_response.dart';
import 'package:employee/core/network/urls.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employee_account_model.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart';

class DownloadEmployeeAccountsXlsPage extends StatefulWidget {

  final int type;
  const DownloadEmployeeAccountsXlsPage({Key? key, this.type = 0}) : super(key: key);

  @override
  State<DownloadEmployeeAccountsXlsPage> createState() => _DownloadEmployeeAccountsXlsPageState();
}

class _DownloadEmployeeAccountsXlsPageState extends State<DownloadEmployeeAccountsXlsPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    downloadXls();
  }
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: Loader(),
        ),
      ),
    );
  }

  downloadXls() async{
    String? token = await getToken();
    BaseListResponse? response = await callListAPI(method: APIMethod.get,accessToken: token, url: widget.type == 0? Urls.allEmployeesAccounts : "${Urls.allEmployeesAccounts}&dismissal=1");
    if(response != null && response.success == true) {
      List<EmployeeAccountModel> l = json
          .decode(json.encode(response.data))
          .map<EmployeeAccountModel>((x) => EmployeeAccountModel.fromJson(x))
          .toList();
      if(l.isNotEmpty){
        generate(l);
      }else{
        showFailureMessage("failed_get_data".tr());
        goBack(context);
      }
    }else{
      showFailureMessage("failed_get_data".tr());
      goBack(context);
    }
  }

  Future<void> generate(List<EmployeeAccountModel> accounts) async {
    try{
      final Workbook workbook = Workbook();
      final Worksheet sheet = workbook.worksheets[0];
      //Set titles
      sheet.getRangeByName("A1").setText("الرقم");
      sheet.getRangeByName("B1").setText("الاسم");
      sheet.getRangeByName("C1").setText("البريد الالكتروني");
      sheet.getRangeByName("D1").setText("رقم الجوال");
      sheet.getRangeByName("E1").setText("نوع الحساب");
      sheet.getRangeByName("F1").setText("جهة العمل");
      sheet.getRangeByName("G1").setText("الوظيفة");
      sheet.getRangeByName("H1").setText("الدولة");
      sheet.getRangeByName("I1").setText("القسم المرتبط");
      sheet.getRangeByName("J1").setText("القسم");
      sheet.getRangeByName("K1").setText("المدير المباشر");
      sheet.getRangeByName("L1").setText("رقم الهوية");
      sheet.getRangeByName("M1").setText("غير مفعل");
      sheet.getRangeByName("N1").setText("التاريخ");
      int i = 2;
      for(EmployeeAccountModel eam in accounts){
        sheet.getRangeByName("A$i").setText(eam.id.toString());
        sheet.getRangeByName("B$i").setText(eam.name ?? "");
        sheet.getRangeByName("C$i").setText(eam.email ?? "");
        sheet.getRangeByName("D$i").setText(eam.mobile ?? "");
        sheet.getRangeByName("E$i").setText(eam.type ?? "");
        sheet.getRangeByName("F$i").setText(eam.activity ?? "");
        sheet.getRangeByName("G$i").setText(eam.position ?? "");
        sheet.getRangeByName("H$i").setText(eam.country ?? "");
        sheet.getRangeByName("I$i").setText(eam.department2 ?? "");
        sheet.getRangeByName("J$i").setText(eam.department ?? "");
        sheet.getRangeByName("K$i").setText(eam.managerName ?? "");
        sheet.getRangeByName("L$i").setText(eam.bn ?? "");
        sheet.getRangeByName("M$i").setText(eam.violationOnOff ?? "");
        sheet.getRangeByName("N$i").setText(eam.createdAt ?? "");
        i++;
      }
      final List<int> bytes = workbook.saveAsStream();
      workbook.dispose();
      final dir = await getExternalStorageDirectory();
      final String path = widget.type == 0? "${dir!.path}/employees_accounts.xlsx" : "${dir!.path}/deleted_employees_accounts.xlsx";
      final File file = File(path);
      await file.writeAsBytes(bytes, flush: true);
      showSuccessMessage("report_created_successfully".tr() + file.path);
      OpenFile.open(file.path);
      goBack(context);
    }catch(e){
      showFailureMessage("failed_create_report".tr());
      goBack(context);
    }
  }
}
