import 'dart:convert';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/base_widgets/loader.dart';
import 'package:employee/core/constants/enums.dart';
import 'package:employee/core/network/models/base_list_response.dart';
import 'package:employee/core/network/urls.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employee_account_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pdfw;

class DownloadEmployeeAccountPdfPage extends StatefulWidget {
  final int type;
  const DownloadEmployeeAccountPdfPage({
    Key? key,
    this.type = 0
  }) : super(key: key);

  @override
  State<DownloadEmployeeAccountPdfPage> createState() => _DownloadEmployeeAccountPdfPageState();
}

class _DownloadEmployeeAccountPdfPageState extends State<DownloadEmployeeAccountPdfPage> {
  late ByteData font;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    downloadPdf();
  }
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: Loader(),
        ),
      ),
    );
  }

  downloadPdf() async{
    String? token = await getToken();
    BaseListResponse? response = await callListAPI(method: APIMethod.get,accessToken: token, url: widget.type == 1? "${Urls.allEmployeesAccounts}&dismissal=1" : Urls.allEmployeesAccounts);
    if(response != null && response.success == true) {
      List<EmployeeAccountModel> l = json
          .decode(json.encode(response.data))
          .map<EmployeeAccountModel>((x) => EmployeeAccountModel.fromJson(x))
          .toList();
      if(l.isNotEmpty){
        File f = await generate(l);
        bool exist = await f.exists();
        if(exist){
          showSuccessMessage("report_created_successfully".tr() + f.path);
          print(f.path);
          OpenFile.open(f.path);
          goBack(context);
        }else{
          showFailureMessage("failed_create_report".tr());
          goBack(context);
        }
      }else{
        showFailureMessage("failed_get_data".tr());
        goBack(context);
      }
    }else{
      showFailureMessage("failed_get_data".tr());
      goBack(context);
    }
  }

  Future<File> saveDocument({
    required String name,
    required pdfw.Document pdf,
  }) async {
    final bytes = await pdf.save();

    final dir = await getExternalStorageDirectory();
    final file = File('${dir!.path}/$name');

    await file.writeAsBytes(bytes);
    return file;
  }

  Future<File> generate(List<EmployeeAccountModel> accounts) async {
    final pdf = pdfw.Document();
    var arabicFont = pdfw.Font.ttf(await rootBundle.load("assets/fonts/amiri.ttf"));
    pdf.addPage(pdfw.MultiPage(
        //theme: pdfw.ThemeData.withFont(base: arabicFont),
        pageFormat: PdfPageFormat.a4,
        maxPages: 5000,
        build: (context) => [
          buildTitle(arabicFont),
          buildAccounts(accounts, arabicFont),
        ],
        //footer: (context) => buildFooter(arabicFont)
    ));
    return saveDocument(name: widget.type == 0? "employees_accounts.pdf" : "deleted_employees_accounts.pdf", pdf: pdf);
  }

  pdfw.Padding paddedHeadingTextCell(String textContent,pdfw.Font font) {
    return pdfw.Padding(
      padding: pdfw.EdgeInsets.all(4),
      child: pdfw.Column(children: [
        pdfw.Text(
          textContent,
          style: pwTableHeadingTextStyle(font),
          textAlign: pdfw.TextAlign.center,
          textDirection: pdfw.TextDirection.rtl
        ),
      ]),
    );
  }

  pdfw.TextStyle pwTableHeadingTextStyle(pdfw.Font font) =>
      pdfw.TextStyle(fontWeight: pdfw.FontWeight.bold, font: font, fontSize: 6);

  pdfw.Padding paddedTextCell(String textContent,pdfw.Font font) {
    return pdfw.Padding(
      padding: pdfw.EdgeInsets.all(4),
      child:
      pdfw.Column(crossAxisAlignment: pdfw.CrossAxisAlignment.start, children: [
        pdfw.Text(textContent, textAlign: pdfw.TextAlign.center, style: pdfw.TextStyle(font: font, fontSize: 6),
            textDirection: pdfw.TextDirection.rtl),
      ]),
    );
  }

  pdfw.Widget buildAccounts(List<EmployeeAccountModel> accounts, pdfw.Font font) {
    List<pdfw.TableRow> tableRows = <pdfw.TableRow>[
      pdfw.TableRow(
        repeat: true,
        children: [
          //paddedHeadingTextCell("الرقم", font),
          paddedHeadingTextCell("الاسم", font),
          paddedHeadingTextCell("البريد الالكتروني", font),
          paddedHeadingTextCell("رقم الجوال", font),
          paddedHeadingTextCell("نوع الحساب", font),
          paddedHeadingTextCell("جهة العمل", font),
          paddedHeadingTextCell("الوظيفة", font),
          paddedHeadingTextCell("الدولة", font),
          paddedHeadingTextCell("القسم المرتبط", font),
        ]
      )
    ];
    for(EmployeeAccountModel e in accounts){
      tableRows.add(pdfw.TableRow(children: [
        //paddedTextCell(e.id.toString(), font),
        paddedTextCell(e.name ?? "", font),
        paddedTextCell(e.email ?? "", font),
        paddedTextCell(e.mobile ?? "", font),
        paddedTextCell(e.type ?? "", font),
        paddedTextCell(e.activity ?? "", font),
        paddedTextCell(e.position ?? "", font),
        paddedTextCell(e.country ?? "", font),
        paddedTextCell(e.department2 ?? "", font),
      ]));
    }
    return pdfw.Table(
      border: const pdfw.TableBorder(
        left: pdfw.BorderSide(color: PdfColors.black, width: 1),
        right: pdfw.BorderSide(color: PdfColors.black, width: 1),
        top: pdfw.BorderSide(color: PdfColors.black, width: 1),
        bottom: pdfw.BorderSide(color: PdfColors.black, width: 1),
      ),
      children: tableRows
    );
    //   pdfw.Column(
    //     crossAxisAlignment: pdfw.CrossAxisAlignment.stretch,
    //     mainAxisSize: pdfw.MainAxisSize.min,
    //     children: [,
    //       pdfw.Directionality(
    //           textDirection: pdfw.TextDirection.rtl,
    //           child: pdfw.Row(
    //               children: [
    //                 pdfw.Expanded(
    //                     child: pdfw.Container(
    //                         width: double.infinity,
    //                         height: 30,
    //                         color: PdfColors.grey300,
    //                         child: pdfw.Center(
    //                             child: pdfw.Text(
    //                                 "الرقم",
    //                                 style: pdfw.TextStyle(
    //                                     fontSize: 10,
    //                                     font: font
    //                                 )
    //                             )
    //                         )
    //                     )
    //                 ),
    //                 pdfw.Expanded(
    //                     child: pdfw.Container(
    //                         width: double.infinity,
    //                         height: 30,
    //                         color: PdfColors.grey300,
    //                         child: pdfw.Center(
    //                             child: pdfw.Text(
    //                                 "الاسم",
    //                                 style: pdfw.TextStyle(
    //                                     fontSize: 10,
    //                                     font: font
    //                                 )
    //                             )
    //                         )
    //                     )
    //                 ),
    //                 pdfw.Expanded(
    //                     child: pdfw.Container(
    //                         width: double.infinity,
    //                         height: 30,
    //                         color: PdfColors.grey300,
    //                         child: pdfw.Center(
    //                             child: pdfw.Text(
    //                                 "البريد الالكتروني",
    //                                 style: pdfw.TextStyle(
    //                                     fontSize: 10,
    //                                     font: font
    //                                 )
    //                             )
    //                         )
    //                     )
    //                 ),
    //                 pdfw.Expanded(
    //                     child: pdfw.Container(
    //                         width: double.infinity,
    //                         height: 30,
    //                         color: PdfColors.grey300,
    //                         child: pdfw.Center(
    //                             child: pdfw.Text(
    //                                 "رقم الجوال",
    //                                 style: pdfw.TextStyle(
    //                                     fontSize: 10,
    //                                     font: font
    //                                 )
    //                             )
    //                         )
    //                     )
    //                 ),
    //                 pdfw.Expanded(
    //                     child: pdfw.Container(
    //                         width: double.infinity,
    //                         height: 30,
    //                         color: PdfColors.grey300,
    //                         child: pdfw.Center(
    //                             child: pdfw.Text(
    //                                 "نوع الحساب",
    //                                 style: pdfw.TextStyle(
    //                                     fontSize: 10,
    //                                     font: font
    //                                 )
    //                             )
    //                         )
    //                     )
    //                 ),
    //                 pdfw.Expanded(
    //                     child: pdfw.Container(
    //                         width: double.infinity,
    //                         height: 30,
    //                         color: PdfColors.grey300,
    //                         child: pdfw.Center(
    //                             child: pdfw.Text(
    //                                 "جهة العمل",
    //                                 style: pdfw.TextStyle(
    //                                     fontSize: 10,
    //                                     font: font
    //                                 )
    //                             )
    //                         )
    //                     )
    //                 ),
    //                 pdfw.Expanded(
    //                     child: pdfw.Container(
    //                         width: double.infinity,
    //                         height: 30,
    //                         color: PdfColors.grey300,
    //                         child: pdfw.Center(
    //                             child: pdfw.Text(
    //                                 "الوظيفة",
    //                                 style: pdfw.TextStyle(
    //                                     fontSize: 10,
    //                                     font: font
    //                                 )
    //                             )
    //                         )
    //                     )
    //                 ),
    //                 pdfw.Expanded(
    //                     child: pdfw.Container(
    //                         width: double.infinity,
    //                         height: 30,
    //                         color: PdfColors.grey300,
    //                         child: pdfw.Center(
    //                             child: pdfw.Text(
    //                                 "الدولة",
    //                                 style: pdfw.TextStyle(
    //                                     fontSize: 10,
    //                                     font: font
    //                                 )
    //                             )
    //                         )
    //                     )
    //                 ),
    //                 pdfw.Expanded(
    //                     child: pdfw.Container(
    //                         width: double.infinity,
    //                         height: 30,
    //                         color: PdfColors.grey300,
    //                         child: pdfw.Center(
    //                             child: pdfw.Text(
    //                                 "القسم المرتبط",
    //                                 style: pdfw.TextStyle(
    //                                     fontSize: 10,
    //                                     font: font
    //                                 )
    //                             )
    //                         )
    //                     )
    //                 ),
    //               ]
    //           )
    //       ),
    //       pdfw.ListView.builder(
    //         itemCount: accounts.length,
    //         itemBuilder: (context, index){
    //           return pdfw.Column(
    //               crossAxisAlignment: pdfw.CrossAxisAlignment.stretch,
    //               mainAxisSize: pdfw.MainAxisSize.min,
    //               children: [
    //                 pdfw.Directionality(
    //                     textDirection: pdfw.TextDirection.rtl,
    //                     child: pdfw.Row(
    //                         children: [
    //                           pdfw.Expanded(
    //                               child: pdfw.Container(
    //                                   width: double.infinity,
    //                                   height: 50,
    //                                   child: pdfw.Center(
    //                                       child: pdfw.Text(
    //                                           accounts[index].id.toString(),
    //                                           style: pdfw.TextStyle(
    //                                               fontSize: 8,
    //                                               font: font
    //                                           )
    //                                       )
    //                                   )
    //                               )
    //                           ),
    //                           pdfw.Expanded(
    //                               child: pdfw.Container(
    //                                   width: double.infinity,
    //                                   height: 50,
    //                                   child: pdfw.Center(
    //                                       child: pdfw.Text(
    //                                           accounts[index].name ?? "",
    //                                           style: pdfw.TextStyle(
    //                                               fontSize: 8,
    //                                               font: font
    //                                           )
    //                                       )
    //                                   )
    //                               )
    //                           ),
    //                           pdfw.Expanded(
    //                               child: pdfw.Container(
    //                                   width: double.infinity,
    //                                   height: 50,
    //                                   child: pdfw.Center(
    //                                       child: pdfw.Text(
    //                                           accounts[index].email ?? "",
    //                                           style: pdfw.TextStyle(
    //                                               fontSize: 8,
    //                                               font: font
    //                                           )
    //                                       )
    //                                   )
    //                               )
    //                           ),
    //                           pdfw.Expanded(
    //                               child: pdfw.Container(
    //                                   width: double.infinity,
    //                                   height: 50,
    //                                   child: pdfw.Center(
    //                                       child: pdfw.Text(
    //                                           accounts[index].mobile ?? "",
    //                                           style: pdfw.TextStyle(
    //                                               fontSize: 8,
    //                                               font: font
    //                                           )
    //                                       )
    //                                   )
    //                               )
    //                           ),
    //                           pdfw.Expanded(
    //                               child: pdfw.Container(
    //                                   width: double.infinity,
    //                                   height: 50,
    //                                   child: pdfw.Center(
    //                                       child: pdfw.Text(
    //                                           accounts[index].type ?? "",
    //                                           style: pdfw.TextStyle(
    //                                               fontSize: 8,
    //                                               font: font
    //                                           )
    //                                       )
    //                                   )
    //                               )
    //                           ),
    //                           pdfw.Expanded(
    //                               child: pdfw.Container(
    //                                   width: double.infinity,
    //                                   height: 50,
    //                                   child: pdfw.Center(
    //                                       child: pdfw.Text(
    //                                           accounts[index].activity ?? "",
    //                                           style: pdfw.TextStyle(
    //                                               fontSize: 8,
    //                                               font: font
    //                                           )
    //                                       )
    //                                   )
    //                               )
    //                           ),
    //                           pdfw.Expanded(
    //                               child: pdfw.Container(
    //                                   width: double.infinity,
    //                                   height: 50,
    //                                   child: pdfw.Center(
    //                                       child: pdfw.Text(
    //                                           accounts[index].position ?? "",
    //                                           style: pdfw.TextStyle(
    //                                               fontSize: 8,
    //                                               font: font
    //                                           )
    //                                       )
    //                                   )
    //                               )
    //                           ),
    //                           pdfw.Expanded(
    //                               child: pdfw.Container(
    //                                   width: double.infinity,
    //                                   height: 50,
    //                                   child: pdfw.Center(
    //                                       child: pdfw.Text(
    //                                           accounts[index].country ?? "",
    //                                           style: pdfw.TextStyle(
    //                                               fontSize: 8,
    //                                               font: font
    //                                           )
    //                                       )
    //                                   )
    //                               )
    //                           ),
    //                           pdfw.Expanded(
    //                               child: pdfw.Container(
    //                                   width: double.infinity,
    //                                   height: 50,
    //                                   child: pdfw.Center(
    //                                       child: pdfw.Text(
    //                                           accounts[index].department2 ?? "",
    //                                           style: pdfw.TextStyle(
    //                                               fontSize: 8,
    //                                               font: font
    //                                           )
    //                                       )
    //                                   )
    //                               )
    //                           ),
    //                         ]
    //                     )
    //                 ),
    //                 pdfw.Divider()
    //               ]
    //           );
    //         },
    //       )
    //     ]
    // );
  }

  pdfw.Widget buildFooter(pdfw.Font font) {
    return pdfw.Column(crossAxisAlignment: pdfw.CrossAxisAlignment.center, children: [
      pdfw.Divider(),
      pdfw.SizedBox(height: 2 * PdfPageFormat.mm),
      pdfw.Text("All rights reserved",textDirection: pdfw.TextDirection.rtl,
      style: pdfw.TextStyle(font: font)
      ),
    ]);
  }

  pdfw.Widget buildTitle(pdfw.Font font) =>
      pdfw.Column(crossAxisAlignment: pdfw.CrossAxisAlignment.stretch, children: [
        pdfw.SizedBox(height: 0.8 * PdfPageFormat.cm),
        pdfw.Text("حسابات الموظفين : - مصادر الجزيرة",
            textAlign: pdfw.TextAlign.right,
            textDirection: pdfw.TextDirection.rtl,
            style: pdfw.TextStyle(fontSize: 24, font: font)),
        pdfw.SizedBox(height: 0.8 * PdfPageFormat.cm),
      ]);
}
