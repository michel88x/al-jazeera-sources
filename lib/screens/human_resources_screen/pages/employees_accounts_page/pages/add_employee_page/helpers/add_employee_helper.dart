
import 'package:image_picker/image_picker.dart';

class AddEmployeeHelper{
  static void pickImage(Function(String) onPick) async{
    final ImagePicker picker = ImagePicker();
    var pickedFile = await picker.getImage(
      source: ImageSource.gallery,
    );
    if(pickedFile != null){
      onPick(pickedFile.path);
    }
  }
}