import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:email_validator/email_validator.dart';
import 'package:datetime_picker_field_platform/datetime_picker_field_platform.dart';
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:employee/screens/human_resources_screen/bloc/hr_cubit.dart';

import 'package:employee/model/add_model.dart';
import 'package:employee/model/hr_model/company_model.dart';
import 'package:employee/model/hr_model/department_model.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/models/employee_account_model.dart';
import 'package:employee/model/week_model.dart';
import 'package:employee/screens/human_resources_screen/models/drop_down_list_response.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/add_employee_page/helpers/add_employee_helper.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/add_employee_page/models/country_model.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/add_employee_page/widgets/add_employee_build_item_c_widget.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/add_employee_page/widgets/add_employee_build_item_w_widget.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/add_employee_page/widgets/add_employee_build_item_widget.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/add_employee_page/widgets/text_field_add_page.dart';
import 'package:employee/screens/human_resources_screen/pages/employees_accounts_page/pages/add_employee_page/widgets/title_add_page.dart';
import 'package:employee/screens/human_resources_screen/bloc/hr_state.dart';
import 'package:employee/screens/human_resources_screen/repository/hr_repository_impl.dart';
import 'package:employee/widget/app_bar_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';

class AddEmplyoeePage extends StatefulWidget {
  int type;
  EmployeeAccountModel item;

  AddEmplyoeePage(this.type, this.item);

  @override
  State<AddEmplyoeePage> createState() => _AddEmplyoeePageState();
}

class _AddEmplyoeePageState extends State<AddEmplyoeePage> {
  TextEditingController nameCon = TextEditingController();
  TextEditingController emailCon = TextEditingController();
  TextEditingController phoneCon = TextEditingController();
  TextEditingController idCon = TextEditingController();
  TextEditingController idEmpCon = TextEditingController();
  TextEditingController posCon = TextEditingController();
  TextEditingController noteCon = TextEditingController();
  TextEditingController startCon = TextEditingController();
  TextEditingController endCon = TextEditingController();
  TextEditingController passCon = TextEditingController();
  TextEditingController passCCon = TextEditingController();
  DateTime? startT;
  bool check = false;
  DateTime? startE;
  DateTime? startTT;
  DateTime? startET;

  List<WeekModel> weekList = [
    WeekModel('sunday'.tr(), false, 1),
    WeekModel('monday'.tr(), false, 2),
    WeekModel('tuesday'.tr(), false, 3),
    WeekModel('wednesday'.tr(), false, 4),
    WeekModel('thursday'.tr(), false, 5),
    WeekModel('friday'.tr(), false, 6),
    WeekModel('saturday'.tr(), false, 7),
  ];

  List<WeekModel> weekListO = [
    WeekModel('sunday'.tr(), false, 1),
    WeekModel('monday'.tr(), false, 2),
    WeekModel('tuesday'.tr(), false, 3),
    WeekModel('wednesday'.tr(), false, 4),
    WeekModel('thursday'.tr(), false, 5),
    WeekModel('friday'.tr(), false, 6),
    WeekModel('saturday'.tr(), false, 7),
  ];

  List<CountryModel> countries = <CountryModel>[
    CountryModel(
        name: "المملكة العربية السعودية",
        id: 0
    ),
    CountryModel(
        name: "مصر",
        id: 1
    )
  ];

  CountryModel? country = CountryModel(
      name: "المملكة العربية السعودية",
      id: 0
  );

  List<AddModel> addItem = [];
  int currentStep = 0;
  int? _groupValue = 1;
  bool click = false;
  bool clicksms = false;
  bool clickw = false;
  bool clickE = false;

  bool clickT = false;
  int? _groupValueS;
  int? _groupValueC;
  bool clickF = false;
  bool clickV = false;

  late List<FocusNode> _focusNodes;

  List<String> titleDrop = [
    'account_type'.tr(),
    'job_direction'.tr(),
    'section_system'.tr(),
    'section'.tr(),
    'account_status'.tr()
  ];

  File selectedImageT = File('');
  File selectedImage = File('');

  @override
  void initState() {
    if (widget.type == 1) {
      setState(() {
        nameCon.text = widget.item.name ?? "";
        emailCon.text = widget.item.email ?? "";
        phoneCon.text = widget.item.mobile ?? "";
        idCon.text = widget.item.idNumber ?? "";
        idEmpCon.text = widget.item.bn ?? "";
        posCon.text = widget.item.position ?? "";
        noteCon.text = widget.item.note ?? "";
        startCon.text =
            widget.item.cardFrom == null ? '' : widget.item.cardFrom!.toString();
        endCon.text = widget.item.cardTo == null ? '' : widget.item.cardTo!.toString();
        if(widget.item.country == "مصر"){
          country = CountryModel(
              name: "مصر",
              id: 1
          );
        }else{
          country = CountryModel(
              name: "المملكة العربية السعودية",
              id: 0
          );
        }
      });
    }
    addItem.add(AddModel('full_name'.tr(), nameCon, 'أحمد الغامدي', false, 0, 0));
    addItem.add(
        AddModel('email'.tr(), emailCon, 'test@test.com', false, 0, 0));
    addItem
        .add(AddModel('mobile_number'.tr(), phoneCon, '966 00 000 0000', false, 1, 0));
    addItem.add(AddModel('id_number'.tr(), idCon, '', false, 1, 0));
    addItem.add(AddModel('employee_number'.tr(), idEmpCon, '', false, 1, 0));
    addItem.add(AddModel('job'.tr(), posCon, '', false, 0, 0));
    addItem.add(AddModel('notes'.tr(), noteCon, '', false, 0, 0));
    addItem.add(AddModel('numbering_barriers_starts'.tr(), startCon, '', false, 0, 0));
    addItem.add(AddModel('numbering_barriers_ends'.tr(), endCon, '', false, 0, 0));
    addItem.add(AddModel('password'.tr(), passCon, '*****', false, 0, 1));
    addItem.add(AddModel('confirm_password'.tr(), passCCon, '*****', false, 0, 1));
    _focusNodes = Iterable<int>.generate(3).map((e) => FocusNode()).toList();
    _focusNodes[0].requestFocus();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => HrCubit(HrRepositoryImpl()),
        child: WillPopScope(
          onWillPop: () async{
            Navigator.pop(context, false);
            return true;
          },
          child: Scaffold(
            backgroundColor: Colors.white,
            body: Container(
              color: Colors.white,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: BlocBuilder<HrCubit, HrState>(builder: (context, state) {
                if (state is HrInitial) {
                  _initializePage(context);
                  return Stack(
                    children: [
                      const Center(
                        child: CircularProgressIndicator(),
                      ),
                      App_Bar(
                        title: widget.type == 0? 'add_employee'.tr() : "edit_employee_account".tr(),
                        LeadingIcon: Icons.keyboard_arrow_right,
                        LeadingOnClick: () {
                          Navigator.pop(context, false);
                        },
                      )
                    ],
                  );
                } else
                  if (state is HrLoadedDropDown) {
                  return Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 100.0),
                        child: Stepper(
                          type: StepperType.horizontal,
                          currentStep: currentStep,
                          onStepCancel: () => currentStep == 0
                              ? goBack(context)
                              : setState(() {
                                  currentStep -= 1;
                                }),
                          onStepContinue: () async {
                            bool isLastStep = (currentStep ==
                                _getSteps(state.data, context).length - 1);
                            if (isLastStep) {
                              _addEmployee(context);

                              //Do something with this information
                            } else {
                              if (currentStep == 0) {
                                for (int i = 0; i < addItem.length; i++) {
                                  if (widget.type == 1) {
                                    if (i == 3 ||
                                        i == 6 ||
                                        i == 7 ||
                                        i == 8 ||
                                        i == 9 ||
                                        i == 10) {
                                      if (addItem[7].con.text != '' &&
                                          addItem[8].con.text != '') {
                                        setState(() {
                                          addItem[7].val = false;
                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text == '' &&
                                          addItem[8].con.text == '') {
                                        setState(() {
                                          addItem[7].val = false;
                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text == '' &&
                                          addItem[8].con.text != '') {
                                        setState(() {
                                          addItem[7].con.text.isEmpty
                                              ? addItem[7].val = true
                                              : addItem[7].val = false;

                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text != '' &&
                                          addItem[8].con.text == '') {
                                        setState(() {
                                          addItem[8].con.text.isEmpty
                                              ? addItem[8].val = true
                                              : addItem[8].val = false;
                                          addItem[7].val = false;
                                        });
                                      }
                                    } else {
                                      setState(() {
                                        addItem[i].con.text.isEmpty
                                            ? addItem[i].val = true
                                            : addItem[i].val = false;
                                      });
                                    }
                                  } else {
                                    if (i == 3 || i == 6 || i == 7 || i == 8) {
                                      if (addItem[7].con.text != '' &&
                                          addItem[8].con.text != '') {
                                        setState(() {
                                          addItem[7].val = false;
                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text == '' &&
                                          addItem[8].con.text == '') {
                                        setState(() {
                                          addItem[7].val = false;
                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text == '' &&
                                          addItem[8].con.text != '') {
                                        setState(() {
                                          addItem[7].con.text.isEmpty
                                              ? addItem[7].val = true
                                              : addItem[7].val = false;

                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text != '' &&
                                          addItem[8].con.text == '') {
                                        setState(() {
                                          addItem[8].con.text.isEmpty
                                              ? addItem[8].val = true
                                              : addItem[8].val = false;

                                          addItem[7].val = false;
                                        });
                                      }
                                    } else {
                                      setState(() {
                                        addItem[i].con.text.isEmpty
                                            ? addItem[i].val = true
                                            : addItem[i].val = false;
                                      });
                                    }
                                  }
                                }
                                check = false;

                                for (int i = 0; i < addItem.length; i++) {
                                  if (addItem[i].val == true) {
                                    setState(() {
                                      check = true;
                                    });
                                  }
                                }
                                if (check == false) {
                                  if (EmailValidator.validate(
                                          addItem[1].con.text) ==
                                      false) {
                                    Fluttertoast.showToast(
                                        msg: "email_form_error".tr(),
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        backgroundColor: Colors.red,
                                        textColor: Colors.white,
                                        fontSize: 16.0);
                                  } else if (addItem[2].con.text.length < 12 ||
                                      !addItem[2]
                                          .con
                                          .text
                                          .substring(0, 3)
                                          .contains('966', 0)) {
                                    Fluttertoast.showToast(
                                        msg: "phone_error".tr(),
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        backgroundColor: Colors.red,
                                        textColor: Colors.white,
                                        fontSize: 16.0);
                                  } else {
                                    if (addItem[9].con.text.toString() ==
                                        addItem[10].con.text.toString()) {
                                      setState(() {
                                        currentStep = 1;
                                      });
                                    } else {
                                      Fluttertoast.showToast(
                                          msg: "passwords_mismatch".tr(),
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIosWeb: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white,
                                          fontSize: 16.0);
                                    }
                                  }
                                }
                              } else {
                                setState(() {
                                  currentStep += 1;
                                });
                              }
                            }
                          },
                          onStepTapped: (step) async {
                            bool isLastStep = (currentStep ==
                                _getSteps(state.data, context).length - 1);

                            if (isLastStep) {
                              _addEmployee(context);

                              //Do something with this information
                            } else {
                              if (currentStep == 0) {
                                for (int i = 0; i < addItem.length; i++) {
                                  if (i == 3 || i == 6 || i == 7 || i == 8) {
                                    if (addItem[7].con.text != '' &&
                                        addItem[8].con.text != '') {
                                      setState(() {
                                        addItem[7].val = false;
                                        addItem[8].val = false;
                                      });
                                    } else if (addItem[7].con.text == '' &&
                                        addItem[8].con.text == '') {
                                      setState(() {
                                        addItem[7].val = false;
                                        addItem[8].val = false;
                                      });
                                    } else if (addItem[7].con.text == '' &&
                                        addItem[8].con.text != '') {
                                      setState(() {
                                        addItem[7].con.text.isEmpty
                                            ? addItem[7].val = true
                                            : addItem[7].val = false;

                                        addItem[8].val = false;
                                      });
                                    } else if (addItem[7].con.text != '' &&
                                        addItem[8].con.text == '') {
                                      setState(() {
                                        addItem[8].con.text.isEmpty
                                            ? addItem[8].val = true
                                            : addItem[8].val = false;

                                        addItem[7].val = false;
                                      });
                                    }
                                  } else {
                                    setState(() {
                                      addItem[i].con.text.isEmpty
                                          ? addItem[i].val = true
                                          : addItem[i].val = false;
                                    });
                                  }
                                }
                                check = false;

                                for (int i = 0; i < addItem.length; i++) {
                                  if (addItem[i].val == true) {
                                    setState(() {
                                      check = true;
                                    });
                                  }
                                }
                                if (check == false) {
                                  if (EmailValidator.validate(
                                          addItem[1].con.text) ==
                                      false) {
                                    Fluttertoast.showToast(
                                        msg: "email_form_error".tr(),
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        backgroundColor: Colors.red,
                                        textColor: Colors.white,
                                        fontSize: 16.0);
                                  } else if (addItem[2].con.text.length < 12 ||
                                      !addItem[2]
                                          .con
                                          .text
                                          .substring(0, 3)
                                          .contains('966', 0)) {
                                    Fluttertoast.showToast(
                                        msg: "phone_error".tr(),
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        backgroundColor: Colors.red,
                                        textColor: Colors.white,
                                        fontSize: 16.0);
                                  } else {
                                    if (addItem[9].con.text.toString() ==
                                        addItem[10].con.text.toString()) {
                                      setState(() {
                                        currentStep = 1;
                                      });
                                    } else {
                                      Fluttertoast.showToast(
                                          msg: "passwords_mismatch".tr(),
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIosWeb: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white,
                                          fontSize: 16.0);
                                    }
                                  }
                                }
                              } else {
                                setState(() {
                                  currentStep = step;
                                });
                              }
                            }
                          },
                          steps: _getSteps(state.data, context),
                        ),
                      ),
                      App_Bar(
                        title: widget.type == 0? 'add_employee'.tr() : "edit_employee_account".tr(),
                        LeadingIcon: Icons.keyboard_arrow_right,
                        LeadingOnClick: () {
                          Navigator.pop(context, false);
                        },
                      )
                    ],
                  );
                } else
                  if (state is HrAdd) {
                  return Stack(
                    children: [
                      Padding(
                          padding: const EdgeInsets.only(top: 100.0),
                          child: Stepper(
                            type: StepperType.horizontal,
                            currentStep: currentStep,
                            onStepCancel: () => currentStep == 0
                                ? goBack(context)
                                : setState(() {
                                    currentStep -= 1;
                                  }),
                            onStepContinue: () async {
                              bool isLastStep = (currentStep ==
                                  _getSteps(state.data, context).length - 1);
                              if (isLastStep) {
                                _addEmployee(context);

                                //Do something with this information
                              } else {
                                if (currentStep == 0) {
                                  for (int i = 0; i < addItem.length; i++) {
                                    if (i == 3 || i == 6 || i == 7 || i == 8) {
                                      if (addItem[7].con.text != '' &&
                                          addItem[8].con.text != '') {
                                        setState(() {
                                          addItem[7].val = false;
                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text == '' &&
                                          addItem[8].con.text == '') {
                                        setState(() {
                                          addItem[7].val = false;
                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text == '' &&
                                          addItem[8].con.text != '') {
                                        setState(() {
                                          addItem[7].con.text.isEmpty
                                              ? addItem[7].val = true
                                              : addItem[7].val = false;

                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text != '' &&
                                          addItem[8].con.text == '') {
                                        setState(() {
                                          addItem[8].con.text.isEmpty
                                              ? addItem[8].val = true
                                              : addItem[8].val = false;

                                          addItem[7].val = false;
                                        });
                                      }
                                    } else {
                                      setState(() {
                                        addItem[i].con.text.isEmpty
                                            ? addItem[i].val = true
                                            : addItem[i].val = false;
                                      });
                                    }
                                  }
                                  check = false;

                                  for (int i = 0; i < addItem.length; i++) {
                                    if (addItem[i].val == true) {
                                      setState(() {
                                        check = true;
                                      });
                                    }
                                  }
                                  if (check == false) {
                                    if (EmailValidator.validate(
                                            addItem[1].con.text) ==
                                        false) {
                                      Fluttertoast.showToast(
                                          msg: "email_form_error".tr(),
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIosWeb: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white,
                                          fontSize: 16.0);
                                    } else if (addItem[2].con.text.length < 12 ||
                                        !addItem[2]
                                            .con
                                            .text
                                            .substring(0, 3)
                                            .contains('966', 0)) {
                                      Fluttertoast.showToast(
                                          msg: "phone_error".tr(),
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIosWeb: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white,
                                          fontSize: 16.0);
                                    } else {
                                      if (addItem[9].con.text.toString() ==
                                          addItem[10].con.text.toString()) {
                                        setState(() {
                                          currentStep = 1;
                                        });
                                      } else {
                                        Fluttertoast.showToast(
                                            msg: "passwords_mismatch".tr(),
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      }
                                    }
                                  }
                                } else {
                                  setState(() {
                                    currentStep += 1;
                                  });
                                }
                              }
                            },
                            onStepTapped: (step) async {
                              bool isLastStep = (currentStep ==
                                  _getSteps(state.data, context).length - 1);

                              if (isLastStep) {
                                _addEmployee(context);

                                //Do something with this information
                              } else {
                                if (currentStep == 0) {
                                  for (int i = 0; i < addItem.length; i++) {
                                    if (i == 3 || i == 6 || i == 7 || i == 8) {
                                      if (addItem[7].con.text != '' &&
                                          addItem[8].con.text != '') {
                                        setState(() {
                                          addItem[7].val = false;
                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text == '' &&
                                          addItem[8].con.text == '') {
                                        setState(() {
                                          addItem[7].val = false;
                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text == '' &&
                                          addItem[8].con.text != '') {
                                        setState(() {
                                          addItem[7].con.text.isEmpty
                                              ? addItem[7].val = true
                                              : addItem[7].val = false;

                                          addItem[8].val = false;
                                        });
                                      } else if (addItem[7].con.text != '' &&
                                          addItem[8].con.text == '') {
                                        setState(() {
                                          addItem[8].con.text.isEmpty
                                              ? addItem[8].val = true
                                              : addItem[8].val = false;

                                          addItem[7].val = false;
                                        });
                                      }
                                    } else {
                                      setState(() {
                                        addItem[i].con.text.isEmpty
                                            ? addItem[i].val = true
                                            : addItem[i].val = false;
                                      });
                                    }
                                  }
                                  check = false;

                                  for (int i = 0; i < addItem.length; i++) {
                                    if (addItem[i].val == true) {
                                      setState(() {
                                        check = true;
                                      });
                                    }
                                  }
                                  if (check == false) {
                                    if (EmailValidator.validate(
                                            addItem[1].con.text) ==
                                        false) {
                                      Fluttertoast.showToast(
                                          msg: "email_form_error".tr(),
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIosWeb: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white,
                                          fontSize: 16.0);
                                    } else if (addItem[2].con.text.length < 12) {
                                      Fluttertoast.showToast(
                                          msg: "phone_error".tr(),
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIosWeb: 1,
                                          backgroundColor: Colors.red,
                                          textColor: Colors.white,
                                          fontSize: 16.0);
                                    } else {
                                      if (addItem[9].con.text.toString() ==
                                          addItem[10].con.text.toString()) {
                                        setState(() {
                                          currentStep = 1;
                                        });
                                      } else {
                                        Fluttertoast.showToast(
                                            msg: "passwords_mismatch".tr(),
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIosWeb: 1,
                                            backgroundColor: Colors.red,
                                            textColor: Colors.white,
                                            fontSize: 16.0);
                                      }
                                    }
                                  }
                                } else {
                                  setState(() {
                                    currentStep = step;
                                  });
                                }
                              }
                            },
                            steps: _getSteps(state.data, context),
                          )),
                      App_Bar(
                        title: widget.type == 0? 'add_employee'.tr() : "edit_employee_account".tr(),
                        LeadingIcon: Icons.keyboard_arrow_right,
                        LeadingOnClick: () {
                          Navigator.pop(context, false);
                        },
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        color: Colors.grey.withOpacity(0.4),
                        child: const Center(child: CircularProgressIndicator()),
                      )
                    ],
                  );
                } else {
                  return const Center(child: CircularProgressIndicator());
                }
              }),
            ),
          ),
        ));
  }

  void _initializePage(BuildContext context) {
    context
        .read<HrCubit>()
        .GetAllListUser(context, widget.type, widget.item)
        .then((value) {
      if (widget.type == 1) {
        setState(() {
          _groupValue = int.parse(widget.item.isOpen!.toString());
          for (int i = 0; i < widget.item.finalPlaces!.length; i++) {
            if (widget.item.finalPlaces![i].id == 7) {
              if (widget.item.finalPlaces![i].selected == true) {
                _groupValueS = 1;
              } else {
                _groupValueS = 0;
              }
            }
            if (widget.item.finalPlaces![i].id == 2) {
              if (widget.item.finalPlaces![i].selected == true) {
                _groupValueC = 1;
              } else {
                _groupValueC = 0;
              }
            }
          }
          if (widget.item.sendWhatsapp == 'active'.tr()) {
            clickw = true;
          }
          if (widget.item.sendSms == 'active'.tr()) {
            clicksms = true;
          }
          if (widget.item.sendEmail == 'active'.tr()) {
            clickE = true;
          }
          if (widget.item.image2 != '') {
            clickF = true;
          }
          if(widget.item.violationOnOff != null && widget.item.violationOnOff!.isNotEmpty){
            if(widget.item.violationOnOff! == "مفعل"){
              clickV = true;
            }else{
              clickV = false;
            }
          }
          if (widget.item.daysOn != null && json
                  .decode(widget.item.daysOn!)
                  .cast<String>()
                  .toList()
                  .length >
              0) {
            click = true;
            startT = DateTime.parse(widget.item.startWorkTime!);
            startE = DateTime.parse(widget.item.endWorkTime!);
            List<String> onList =  json.decode(widget.item.daysOn!).cast<String>().toList();
            for(String s in onList){
              for (int j = 0; j < weekListO.length; j++) {
                if(weekListO[j].id.toString() == s){
                  weekListO[j].clickW = true;
                }
              }
            }
            // for (int i = 0;
            //     i <
            //         json
            //             .decode(widget.item.daysOn!)
            //             .cast<String>()
            //             .toList()
            //             .length;
            //     i++) {
            //   for (int j = 0; j < weekListO.length; j++) {
            //     if (weekListO[i].id.toString() ==
            //         json
            //             .decode(widget.item.daysOn!)
            //             .cast<String>()
            //             .toList()[i]
            //             .toString()) {
            //       weekListO[i].clickW = true;
            //     }
            //   }
            // }
          }



          if (widget.item.daysOff != null && json
              .decode(widget.item.daysOff!)
              .cast<String>()
              .toList()
              .length >
              0) {

            clickT = true;
            print("dfjdkjfdkjfjd: ${widget.item.startAutoTime}");
            startTT = DateTime.parse(widget.item.startAutoTime!);
            startET = DateTime.parse(widget.item.endAutoTime!);
            List<String> offList =  json.decode(widget.item.daysOff!).cast<String>().toList();
            for(String s in offList){
              for (int j = 0; j < weekList.length; j++) {
                if(weekList[j].id.toString() == s){
                  weekList[j].clickW = true;
                }
              }
            }
            // for (int i = 0;
            // i <
            //     json
            //         .decode(widget.item.daysOff!)
            //         .cast<String>()
            //         .toList()
            //         .length;
            // i++) {
            //   for (int j = 0; j < weekList.length; j++) {
            //     if (weekList[i].id.toString() ==
            //         json
            //             .decode(widget.item.daysOff!)
            //             .cast<String>()
            //             .toList()[i]
            //             .toString()) {
            //       weekList[i].clickW = true;
            //     }
            //   }
            // }
          }
        });
      }
    });
  }

  List<Step> _getSteps(DropDownListResponse data, BuildContext con) {
    return <Step>[
      Step(
        state: currentStep > 0 ? StepState.complete : StepState.indexed,
        isActive: currentStep >= 0,
        title: const Text(""),
        content: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.zero,
          physics: const NeverScrollableScrollPhysics(),
          children: [
            Column(
            children: [
              ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Column(children: [
                        ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: addItem.length,
                            itemBuilder: (context, index) {
                              return Column(
                                children: [
                                  TitleAddPage(addItem[index].title),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  TextFieldAddPage(addItem[index]),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                ],
                              );
                            })
                      ]),
                    )
                  ]),
              Builder(builder: (context){
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TitleAddPage("country".tr()),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius.circular(10),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.3),
                                spreadRadius: 1,
                                blurRadius: 2)
                          ]),
                      margin: const EdgeInsets.only(left: 10, right: 5),
                      padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
                      child: Padding(
                        padding:
                        const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Center(
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              isExpanded: true,
                              isDense: true,
                              hint: Text(country!
                                    .name!,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Theme.of(context)
                                      .hintColor,
                                ),
                              ),
                              items: countries
                                  .map((item) =>
                                  DropdownMenuItem<
                                      CountryModel>(
                                    value: item,
                                    child: Text(
                                      item.name!,
                                      style:
                                      const TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                  ))
                                  .toList(),
                              value: country,
                              onChanged: (value) {
                                setState(() {
                                  country = value;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                );
              })

            ],
          ),
          ]
        ),
      ),
      Step(
          state: currentStep > 1 ? StepState.complete : StepState.indexed,
          isActive: currentStep >= 1,
          title: const Text(""),
          content: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              physics: const NeverScrollableScrollPhysics(),
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                  child: Column(children: [
                    ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: titleDrop.length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              TitleAddPage(titleDrop[index]),
                              const SizedBox(
                                height: 10,
                              ),
                              index == 0
                                  ? Container(
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                          boxShadow: const [
                                            BoxShadow(
                                                color: Colors.grey,
                                                blurRadius: 5,
                                                spreadRadius: 1)
                                          ]),
                                      padding:
                                          const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        child: Center(
                                          child: DropdownButtonHideUnderline(
                                            child: DropdownButton(
                                              isExpanded: true,
                                              isDense: true,
                                              hint: Text(
                                                context
                                                    .read<HrCubit>()
                                                    .adminItem!
                                                    .name,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: Theme.of(context)
                                                      .hintColor,
                                                ),
                                              ),
                                              items: data.isAdminList
                                                  .map((item) =>
                                                      DropdownMenuItem<
                                                          CompanyModel>(
                                                        value: item,
                                                        child: Text(
                                                          item.name,
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 14,
                                                          ),
                                                        ),
                                                      ))
                                                  .toList(),
                                              value: context
                                                  .read<HrCubit>()
                                                  .adminItem,
                                              onChanged: (value) {
                                                setState(() {
                                                  context
                                                      .read<HrCubit>()
                                                      .adminItem = value;
                                                });
                                              },
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  : index == 1
                                      ? Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              color: Colors.white,
                                              boxShadow: const [
                                                BoxShadow(
                                                    color: Colors.grey,
                                                    blurRadius: 5,
                                                    spreadRadius: 1)
                                              ]),
                                          padding:
                                              const EdgeInsets.fromLTRB(0, 10, 0, 10),
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                10, 0, 10, 0.0),
                                            child: Center(
                                              child:
                                                  DropdownButtonHideUnderline(
                                                child: DropdownButton(
                                                  isExpanded: true,
                                                  isDense: true,
                                                  hint: Text(
                                                    context
                                                        .read<HrCubit>()
                                                        .comItem!
                                                        .name,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: Theme.of(context)
                                                          .hintColor,
                                                    ),
                                                  ),
                                                  items: data.companyList
                                                      .map((item) =>
                                                          DropdownMenuItem<
                                                              CompanyModel>(
                                                            value: item,
                                                            child: Text(
                                                              item.name,
                                                              style:
                                                                  const TextStyle(
                                                                fontSize: 14,
                                                              ),
                                                            ),
                                                          ))
                                                      .toList(),
                                                  value: context
                                                      .read<HrCubit>()
                                                      .comItem,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      context
                                                          .read<HrCubit>()
                                                          .comItem = value;
                                                    });
                                                  },
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      : index == 2
                                          ? Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: Colors.white,
                                                  boxShadow: const [
                                                    BoxShadow(
                                                        color: Colors.grey,
                                                        blurRadius: 5,
                                                        spreadRadius: 1)
                                                  ]),
                                              padding: const EdgeInsets.fromLTRB(
                                                  0, 10, 0, 10),
                                              child: Center(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          10, 0, 10, 0.0),
                                                  child:
                                                      DropdownButtonHideUnderline(
                                                    child: DropdownButton(
                                                      isExpanded: true,
                                                      isDense: true,
                                                      hint: Text(
                                                        context
                                                            .read<HrCubit>()
                                                            .depItem!
                                                            .name,
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                          color:
                                                              Theme.of(context)
                                                                  .hintColor,
                                                        ),
                                                      ),
                                                      items: data.departmentList
                                                          .map((item) =>
                                                              DropdownMenuItem<
                                                                  DepartmentModel>(
                                                                value: item,
                                                                child: Text(
                                                                  item.name,
                                                                  style:
                                                                      const TextStyle(
                                                                    fontSize:
                                                                        14,
                                                                  ),
                                                                ),
                                                              ))
                                                          .toList(),
                                                      value: context
                                                          .read<HrCubit>()
                                                          .depItem,
                                                      onChanged: (value) {
                                                        setState(() {
                                                          context
                                                              .read<HrCubit>()
                                                              .depItem = value;
                                                        });
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : index == 3
                                              ? Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      color: Colors.white,
                                                      boxShadow: const [
                                                        BoxShadow(
                                                            color: Colors.grey,
                                                            blurRadius: 5,
                                                            spreadRadius: 1)
                                                      ]),
                                                  padding: const EdgeInsets.fromLTRB(
                                                      0, 10, 0, 10),
                                                  child: Center(
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .fromLTRB(
                                                          10, 0, 10, 0.0),
                                                      child:
                                                          DropdownButtonHideUnderline(
                                                        child: DropdownButton(
                                                          isExpanded: true,
                                                          isDense: true,
                                                          hint: Text(
                                                            context
                                                                .read<HrCubit>()
                                                                .secItem!
                                                                .name,
                                                            style: TextStyle(
                                                              fontSize: 14,
                                                              color: Theme.of(
                                                                      context)
                                                                  .hintColor,
                                                            ),
                                                          ),
                                                          items: data
                                                              .departmentList
                                                              .map((item) =>
                                                                  DropdownMenuItem<
                                                                      DepartmentModel>(
                                                                    value: item,
                                                                    child: Text(
                                                                      item.name,
                                                                      style:
                                                                          const TextStyle(
                                                                        fontSize:
                                                                            14,
                                                                      ),
                                                                    ),
                                                                  ))
                                                              .toList(),
                                                          value: context
                                                              .read<HrCubit>()
                                                              .secItem,
                                                          onChanged: (value) {
                                                            setState(() {
                                                              context
                                                                  .read<
                                                                      HrCubit>()
                                                                  .secItem = value;
                                                            });
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              : Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      color: Colors.white,
                                                      boxShadow: const [
                                                        BoxShadow(
                                                            color: Colors.grey,
                                                            blurRadius: 5,
                                                            spreadRadius: 1)
                                                      ]),
                                                  padding: const EdgeInsets.fromLTRB(
                                                      0, 10, 0, 10),
                                                  child: Center(
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .fromLTRB(
                                                          10, 0, 10, 0.0),
                                                      child:
                                                          DropdownButtonHideUnderline(
                                                        child: DropdownButton(
                                                          isExpanded: true,
                                                          isDense: true,
                                                          hint: Text(
                                                            context
                                                                .read<HrCubit>()
                                                                .item!,
                                                            style: TextStyle(
                                                              fontSize: 14,
                                                              color: Theme.of(
                                                                      context)
                                                                  .hintColor,
                                                            ),
                                                          ),
                                                          items: context
                                                              .read<HrCubit>()
                                                              .actList
                                                              .map((item) =>
                                                                  DropdownMenuItem<
                                                                      String>(
                                                                    value: item,
                                                                    child: Text(
                                                                      item,
                                                                      style:
                                                                          const TextStyle(
                                                                        fontSize:
                                                                            14,
                                                                      ),
                                                                    ),
                                                                  ))
                                                              .toList(),
                                                          value: context
                                                              .read<HrCubit>()
                                                              .item,
                                                          onChanged: (value) {
                                                            setState(() {
                                                              context
                                                                  .read<
                                                                      HrCubit>()
                                                                  .item = value;
                                                            });
                                                          },
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                              const SizedBox(
                                height: 20,
                              ),
                            ],
                          );
                        }),
                    TitleAddPage('direct_boss'.tr()),
                    const SizedBox(
                      height: 10,
                    ),
                    con.read<HrCubit>().secItem!.manager_name == ''
                        ? const Visibility(
                            visible: false,
                            child: Text(''),
                          )
                        : Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black.withOpacity(0.3),
                                    spreadRadius: 1,
                                    blurRadius: 2)
                              ],
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(15, 01, 15, 10),
                                child: Text(
                                    con.read<HrCubit>().secItem!.manager_name,
                                    style: AppStyles.cairoNormal))),
                    const SizedBox(
                      height: 10,
                    ),
                  ]),
                )
              ])),
      Step(
        state: currentStep > 2 ? StepState.complete : StepState.indexed,
        isActive: currentStep >= 2,
        title: const Text(""),
        content: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.zero,
            physics: const NeverScrollableScrollPhysics(),
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
                child: Column(children: [
                  TitleAddPage('sign_in_and_out'.tr()),
                  const SizedBox(
                    height: 10,
                  ),
                  AddEmployeeBuildItemWidget(text: "opened".tr(),value: 1,focusNode: _focusNodes[0], groupValue: _groupValue, onChanged: (value){
                    setState(() {
                      _groupValue = value;
                    });
                  },),
                  AddEmployeeBuildItemWidget(text: "specific_node".tr(),value: 0,focusNode: _focusNodes[0], groupValue: _groupValue, onChanged: (value){
                    setState(() {
                      _groupValue = value;
                    });
                  },),
                  _groupValue != 0
                      ? const Visibility(
                          visible: false,
                          child: Text(''),
                        )
                      : Row(
                          children: [
                            AddEmployeeBuildItemWWidget(text: "workshop_address".tr(),value: 0,focusNode: _focusNodes[0], groupValue: _groupValueS,onChanged: (value){
                              setState(() {
                                _groupValueS = value;
                              });
                            },),
                            AddEmployeeBuildItemCWidget(text: "office_address".tr(),value: 0, focusNode: _focusNodes[0], groupValue: _groupValueC,onChanged: (value){
                              setState(() {
                                _groupValueC = value;
                              });
                            },)
                          ],
                        ),
                  Container(
                    height: 1,
                    color: Colors.grey.withOpacity(0.3),
                    width: MediaQuery.of(context).size.width,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        click = !click;
                      });
                    },
                    child: Row(
                      children: [
                        Container(
                          height: 15,
                          width: 15,
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Colors.grey.withOpacity(0.3),
                              ),
                              borderRadius: BorderRadius.circular(3),
                              color: click == false ? Colors.white : AppColors.secondary),
                        ),
                        const SizedBox(
                          width: 15,
                        ),
                        TitleAddPage('specific_work_times'.tr()),
                      ],
                    ),
                  ),
                  click == true
                      ? const SizedBox(
                          height: 10,
                        )
                      : const SizedBox(
                          height: 20,
                        ),
                  click == true
                      ? Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 150,
                                  child: Column(children: [
                                    TitleAddPage('work_start'.tr()),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      width: 150,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                          boxShadow: [
                                            const BoxShadow(
                                                color: Colors.grey,
                                                blurRadius: 5,
                                                spreadRadius: 1)
                                          ]),
                                      child: Padding(
                                        padding: const EdgeInsets.all(1.0),
                                        child: Center(
                                          child: DateTimeFieldPlatform(
                                            mode: DateMode.time,
                                            textConfirm: 'agree'.tr(),
                                            textCancel: 'cancel'.tr(),
                                            initialDate: startT == null
                                                ? DateTime.now()
                                                : startT,
                                            decoration: InputDecoration(
                                              hintText: 'select_time'.tr(),
                                            ),
                                            onConfirm: (v) {
                                              setState(() {
                                                startT = v;
                                              });
                                            },
                                            maximumDate: DateTime.now()
                                                .add(const Duration(hours: 2)),
                                            minimumDate: DateTime.now()
                                                .subtract(
                                                    const Duration(hours: 2)),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                                ),
                                Container(
                                  width: 150,
                                  child: Column(children: [
                                    TitleAddPage('work_end'.tr()),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      width: 150,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                          boxShadow: [
                                            const BoxShadow(
                                                color: Colors.grey,
                                                blurRadius: 5,
                                                spreadRadius: 1)
                                          ]),
                                      child: Padding(
                                        padding: const EdgeInsets.all(1.0),
                                        child: Center(
                                          child: DateTimeFieldPlatform(
                                            mode: DateMode.time,
                                            textConfirm: 'agree'.tr(),
                                            textCancel: 'cancel'.tr(),
                                            initialDate: startE == null
                                                ? DateTime.now()
                                                : startE,
                                            decoration: InputDecoration(
                                              hintText: 'select_time'.tr(),
                                            ),
                                            onConfirm: (v) {
                                              setState(() {
                                                startE = v;
                                              });
                                            },
                                            maximumDate: DateTime.now()
                                                .add(const Duration(hours: 2)),
                                            minimumDate: DateTime.now()
                                                .subtract(
                                                    const Duration(hours: 2)),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            TitleAddPage('off_days'.tr()),
                            const SizedBox(
                              height: 10,
                            ),
                            ListView.builder(
                                itemCount: weekListO.length,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      setState(() {
                                        weekListO[index].clickW =
                                            !weekListO[index].clickW;
                                      });
                                    },
                                    child: Row(
                                      children: [
                                        Container(
                                          height: 15,
                                          width: 15,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                width: 1,
                                                color: Colors.grey
                                                    .withOpacity(0.3),
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(3),
                                              color: weekListO[index].clickW ==
                                                      false
                                                  ? Colors.white
                                                  : AppColors.secondary),
                                        ),
                                        const SizedBox(
                                          width: 15,
                                        ),
                                        Text(weekListO[index].title,
                                            style: AppStyles.cairoNormal),
                                      ],
                                    ),
                                  );
                                })
                          ],
                        )
                      : const Visibility(
                          child: Text(''),
                          visible: false,
                        ),
                  const SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        clickT = !clickT;
                      });
                    },
                    child: Row(
                      children: [
                        Container(
                          height: 15,
                          width: 15,
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Colors.grey.withOpacity(0.3),
                              ),
                              borderRadius: BorderRadius.circular(3),
                              color: clickT == false ? Colors.white : AppColors.secondary),
                        ),
                        const SizedBox(
                          width: 15,
                        ),
                        TitleAddPage('auto_exist_sign'.tr()),
                      ],
                    ),
                  ),
                  clickT == true
                      ? const SizedBox(
                          height: 10,
                        )
                      : const SizedBox(
                          height: 20,
                        ),
                  clickT == true
                      ? Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 150,
                                  child: Column(children: [
                                    TitleAddPage('work_start'.tr()),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      width: 150,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                          boxShadow: [
                                            const BoxShadow(
                                                color: Colors.grey,
                                                blurRadius: 5,
                                                spreadRadius: 1)
                                          ]),
                                      child: Padding(
                                        padding: const EdgeInsets.all(1.0),
                                        child: Center(
                                          child: DateTimeFieldPlatform(
                                            mode: DateMode.time,
                                            textConfirm: 'agree'.tr(),
                                            textCancel: 'cancel'.tr(),
                                            initialDate: startTT == null
                                                ? DateTime.now()
                                                : startTT,
                                            decoration: InputDecoration(
                                              hintText: 'select_time'.tr(),
                                            ),
                                            onConfirm: (v) {
                                              setState(() {
                                                startTT = v;
                                              });
                                            },
                                            maximumDate: DateTime.now()
                                                .add(const Duration(hours: 2)),
                                            minimumDate: DateTime.now()
                                                .subtract(
                                                    const Duration(hours: 2)),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                                ),
                                Container(
                                  width: 150,
                                  child: Column(children: [
                                    TitleAddPage('work_end'.tr()),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      width: 150,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.white,
                                          boxShadow: [
                                            const BoxShadow(
                                                color: Colors.grey,
                                                blurRadius: 5,
                                                spreadRadius: 1)
                                          ]),
                                      child: Padding(
                                        padding: const EdgeInsets.all(1.0),
                                        child: Center(
                                          child: DateTimeFieldPlatform(
                                            mode: DateMode.time,
                                            textConfirm: 'agree'.tr(),
                                            textCancel: 'cancel'.tr(),
                                            initialDate: startET == null
                                                ? DateTime.now()
                                                : startET,
                                            decoration: InputDecoration(
                                              hintText: 'select_time'.tr(),
                                            ),
                                            onConfirm: (v) {
                                              setState(() {
                                                startET = v;
                                              });
                                            },
                                            maximumDate: DateTime.now()
                                                .add(const Duration(hours: 2)),
                                            minimumDate: DateTime.now()
                                                .subtract(
                                                    const Duration(hours: 2)),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            TitleAddPage('off_days'.tr()),
                            const SizedBox(
                              height: 10,
                            ),
                            ListView.builder(
                                itemCount: weekList.length,
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      setState(() {
                                        weekList[index].clickW =
                                            !weekList[index].clickW;
                                      });
                                    },
                                    child: Row(
                                      children: [
                                        Container(
                                          height: 15,
                                          width: 15,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                width: 1,
                                                color: Colors.grey
                                                    .withOpacity(0.3),
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(3),
                                              color: weekList[index].clickW ==
                                                      false
                                                  ? Colors.white
                                                  : AppColors.secondary),
                                        ),
                                        const SizedBox(
                                          width: 15,
                                        ),
                                        Text(weekList[index].title,
                                            style: AppStyles.cairoNormal),
                                      ],
                                    ),
                                  );
                                })
                          ],
                        )
                      : const Visibility(
                          child: Text(''),
                          visible: false,
                        ),
                  const SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        clickF = !clickF;
                      });
                    },
                    child: Row(
                      children: [
                        Container(
                          height: 15,
                          width: 15,
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Colors.grey.withOpacity(0.3),
                              ),
                              borderRadius: BorderRadius.circular(3),
                              color: clickF == false ? Colors.white : AppColors.secondary),
                        ),
                        const SizedBox(
                          width: 15,
                        ),
                        TitleAddPage('use_face_detection'.tr()),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        clicksms = !clicksms;
                      });
                    },
                    child: Row(
                      children: [
                        Container(
                          height: 15,
                          width: 15,
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Colors.grey.withOpacity(0.3),
                              ),
                              borderRadius: BorderRadius.circular(3),
                              color:
                                  clicksms == false ? Colors.white : AppColors.secondary),
                        ),
                        const SizedBox(
                          width: 15,
                        ),
                        TitleAddPage('send_sms'.tr()),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        clickw = !clickw;
                      });
                    },
                    child: Row(
                      children: [
                        Container(
                          height: 15,
                          width: 15,
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Colors.grey.withOpacity(0.3),
                              ),
                              borderRadius: BorderRadius.circular(3),
                              color: clickw == false ? Colors.white : AppColors.secondary),
                        ),
                        const SizedBox(
                          width: 15,
                        ),
                        TitleAddPage('send_whatsapp'.tr()),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        clickE = !clickE;
                      });
                    },
                    child: Row(
                      children: [
                        Container(
                          height: 15,
                          width: 15,
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Colors.grey.withOpacity(0.3),
                              ),
                              borderRadius: BorderRadius.circular(3),
                              color: clickE == false ? Colors.white : AppColors.secondary),
                        ),
                        const SizedBox(
                          width: 15,
                        ),
                        TitleAddPage('send_email'.tr()),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        clickV = !clickV;
                      });
                    },
                    child: Row(
                      children: [
                        Container(
                          height: 15,
                          width: 15,
                          decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Colors.grey.withOpacity(0.3),
                              ),
                              borderRadius: BorderRadius.circular(3),
                              color: clickV == false ? Colors.white : AppColors.secondary),
                        ),
                        const SizedBox(
                          width: 15,
                        ),
                        TitleAddPage('violations'.tr()),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TitleAddPage('employee_image'.tr()),
                  const SizedBox(
                    height: 10,
                  ),
                  widget.type == 1
                      ? InkWell(
                          onTap: () {
                            AddEmployeeHelper.pickImage((p0){
                              setState(() {
                                selectedImage = File(p0);
                              });
                            });
                          },
                          child:
                          selectedImage.path.isNotEmpty?
                              Image.file(selectedImage) :
                          Image.network(widget.item.image!))
                      : InkWell(
                          onTap: () {
                            AddEmployeeHelper.pickImage((p0){
                              setState(() {
                                selectedImage = File(p0);
                              });
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                boxShadow: const [
                                  BoxShadow(
                                      color: Colors.grey,
                                      blurRadius: 5,
                                      spreadRadius: 1)
                                ]),
                            child: selectedImage.path == ''
                                ? const Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Icon(
                                      Icons.add_photo_alternate_outlined,
                                    ))
                                : Image.file(selectedImage),
                          ),
                        ),
                  const SizedBox(
                    height: 20,
                  ),
                  TitleAddPage('employee_signature'.tr()),
                  const SizedBox(
                    height: 10,
                  ),
                  widget.type == 1
                      ? InkWell(
                          onTap: () {
                            AddEmployeeHelper.pickImage((p0){
                              setState(() {
                                selectedImageT = File(p0);
                              });
                            });
                          },
                          child:
                          selectedImageT.path.isNotEmpty?
                              Image.file(selectedImageT) :
                          Image.network(widget.item.signature2!))
                      : InkWell(
                          onTap: () {
                            AddEmployeeHelper.pickImage((p0){
                              setState(() {
                                selectedImageT = File(p0);
                              });
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white,
                                boxShadow: const [
                                  BoxShadow(
                                      color: Colors.grey,
                                      blurRadius: 5,
                                      spreadRadius: 1)
                                ]),
                            child: selectedImageT.path == ''
                                ? const Padding(
                                    padding: EdgeInsets.all(20),
                                    child: Icon(
                                      Icons.add_photo_alternate_outlined,
                                    ))
                                : Image.file(selectedImageT),
                          ),
                        ),
                  const SizedBox(
                    height: 20,
                  ),
                ]),
              )
            ]),
      ),
    ];
  }

  Future<void> _addEmployee(BuildContext context) async {
    List<String> space = [];
    if (_groupValueS == 0) {
      space.add('7');
    }
    if (_groupValueC == 0) {
      space.add('2');
    }

    if (_groupValue == 0) {
    } else {}

    List<int> days = [];
    for (int i = 0; i < weekList.length; i++) {
      if (weekList[i].clickW == true) {
        days.add(weekList[i].id);
      }
    }
    List<int> daysO = [];
    for (int i = 0; i < weekListO.length; i++) {
      if (weekListO[i].clickW == true) {
        daysO.add(weekListO[i].id);
      }
    }
    if (click == true) {
      if (startT == null || startE == null || daysO.isEmpty) {
        Fluttertoast.showToast(
            msg:
            "enter_start_and_end_work_dates".tr(),
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      } else {
        if (clickT == true) {
          if (startTT == null || startET == null || days.length == 0) {
            Fluttertoast.showToast(
                msg:
                "enter_start_and_end_work_dates2".tr(),
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          } else {
            if (clickF == true && widget.type == 0) {
              if (selectedImage.path == '' || selectedImageT.path == '') {
                Fluttertoast.showToast(
                    msg: "input_employee_photo_and_signature".tr(),
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);
              } else {
                if (selectedImageT.path == '' && selectedImage.path == '') {
                  FormData body = FormData.fromMap({
                    "name": addItem[0].con.text,
                    "email": addItem[1].con.text,
                    "mobile": addItem[2].con.text,
                    "id_number": addItem[3].con.text,
                    "bn": addItem[4].con.text,
                    "position": addItem[5].con.text,
                    "note": addItem[6].con.text,
                    "card_from": addItem[7].con.text,
                    "card_to": addItem[8].con.text,
                    "password": addItem[9].con.text,
                    "password_confirmation": addItem[10].con.text,
                    "is_admin":
                    context.read<HrCubit>().adminItem!.id.toString(),
                    "activity": context.read<HrCubit>().comItem!.id.toString(),
                    "department_id":
                    context.read<HrCubit>().depItem!.id.toString(),
                    "department_id2":
                    context.read<HrCubit>().secItem!.id.toString(),
                    "manager_id":
                    context.read<HrCubit>().secItem!.manager_id.toString(),
                    "is_open": _groupValue.toString(),
                    "places": space ?? '',
                    "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                    "start": startT.toString(),
                    "end": startE.toString(),
                    "auto": clickT == false ? '0' : '1',
                    "autostart": startTT.toString(),
                    "autoend": startET.toString(),
                    "send_sms": clicksms == false ? '0' : '1',
                    "send_whatsapp": clickw == false ? '0' : '1',
                    "send_email": clickE == false ? '0' : '1',
                    "status_id":
                    context.read<HrCubit>().item.toString() == 'active'.tr()
                        ? '1'
                        : '0',
                    "face_detection": clickF == false ? '0' : '1',
                    "country_id" : country!.id.toString(),
                    "violation_on_off" : clickV == false? '0' : '1',
                  });
                  for (int i = 0; i < daysO.length; i++) {
                    body.fields
                        .add(MapEntry("days_on[$i]", daysO[i].toString()));
                  }
                  for (int i = 0; i < days.length; i++) {
                    body.fields
                        .add(MapEntry("days_off[$i]", days[i].toString()));
                  }
                  if(widget.type == 0) {
                    context.read<HrCubit>().addEmployee(context, body);
                  }else{
                    print("aaaaaaa: 20");
                    context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
                  }
                } else if (selectedImageT.path == '' &&
                    selectedImage.path != '') {
                  FormData body = FormData.fromMap({
                    "name": addItem[0].con.text,
                    "email": addItem[1].con.text,
                    "mobile": addItem[2].con.text,
                    "id_number": addItem[3].con.text,
                    "bn": addItem[4].con.text,
                    "position": addItem[5].con.text,
                    "note": addItem[6].con.text,
                    "card_from": addItem[7].con.text,
                    "card_to": addItem[8].con.text,
                    "password": addItem[9].con.text,
                    "password_confirmation": addItem[10].con.text,
                    "is_admin":
                    context.read<HrCubit>().adminItem!.id.toString(),
                    "activity": context.read<HrCubit>().comItem!.id.toString(),
                    "department_id":
                    context.read<HrCubit>().depItem!.id.toString(),
                    "department_id2":
                    context.read<HrCubit>().secItem!.id.toString(),
                    "manager_id":
                    context.read<HrCubit>().secItem!.manager_id.toString(),
                    "is_open": _groupValue.toString(),
                    "places": space == null ? '' : space,
                    "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                    "start": startT.toString(),
                    "end": startE.toString(),
                    "auto": clickT == false ? '0' : '1',
                    "autostart": startTT.toString(),
                    "autoend": startET.toString(),
                    /* "signature": selectedImageT.path == ''
                                    ? null
                                    : await MultipartFile.fromFile(selectedImageT.path,
                                    filename: selectedImageT.path.split('/').last),*/
                    "image": selectedImage.path == ''
                        ? null
                        : await MultipartFile.fromFile(selectedImage.path,
                        filename: selectedImage.path.split('/').last),
                    "send_sms": clicksms == false ? '0' : '1',
                    "send_whatsapp": clickw == false ? '0' : '1',
                    "send_email": clickE == false ? '0' : '1',
                    "status_id":
                    context.read<HrCubit>().item.toString() == 'active'.tr()
                        ? '1'
                        : '0',
                    "face_detection": clickF == false ? '0' : '1',
                    "country_id" : country!.id.toString(),
                    "violation_on_off" : clickV == false? '0' : '1',
                  });
                  for (int i = 0; i < daysO.length; i++) {
                    body.fields
                        .add(MapEntry("days_on[$i]", daysO[i].toString()));
                  }
                  for (int i = 0; i < days.length; i++) {
                    body.fields
                        .add(MapEntry("days_off[$i]", days[i].toString()));
                  }
                  if(widget.type == 0) {
                    context.read<HrCubit>().addEmployee(context, body);
                  }else{
                    print("aaaaaaa: 19");
                    context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
                  }
                } else if (selectedImageT.path != '' &&
                    selectedImage.path == '') {
                  FormData body = FormData.fromMap({
                    "name": addItem[0].con.text,
                    "email": addItem[1].con.text,
                    "mobile": addItem[2].con.text,
                    "id_number": addItem[3].con.text,
                    "bn": addItem[4].con.text,
                    "position": addItem[5].con.text,
                    "note": addItem[6].con.text,
                    "card_from": addItem[7].con.text,
                    "card_to": addItem[8].con.text,
                    "password": addItem[9].con.text,
                    "password_confirmation": addItem[10].con.text,
                    "is_admin":
                    context.read<HrCubit>().adminItem!.id.toString(),
                    "activity": context.read<HrCubit>().comItem!.id.toString(),
                    "department_id":
                    context.read<HrCubit>().depItem!.id.toString(),
                    "department_id2":
                    context.read<HrCubit>().secItem!.id.toString(),
                    "manager_id":
                    context.read<HrCubit>().secItem!.manager_id.toString(),
                    "is_open": _groupValue.toString(),
                    "places": space == null ? '' : space,
                    "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                    "start": startT.toString(),
                    "end": startE.toString(),
                    "auto": clickT == false ? '0' : '1',
                    "autostart": startTT.toString(),
                    "autoend": startET.toString(),
                    "signature": selectedImageT.path == ''
                        ? null
                        : await MultipartFile.fromFile(selectedImageT.path,
                        filename: selectedImageT.path.split('/').last),
                    /*   "image": selectedImage.path == ''
                                    ? null
                                    : await MultipartFile.fromFile(selectedImage.path,
                                    filename: selectedImage.path.split('/').last),*/
                    "send_sms": clicksms == false ? '0' : '1',
                    "send_whatsapp": clickw == false ? '0' : '1',
                    "send_email": clickE == false ? '0' : '1',
                    "status_id":
                    context.read<HrCubit>().item.toString() == 'active'.tr()
                        ? '1'
                        : '0',
                    "face_detection": clickF == false ? '0' : '1',
                    "country_id" : country!.id.toString(),
                    "violation_on_off" : clickV == false? '0' : '1',
                  });
                  for (int i = 0; i < daysO.length; i++) {
                    body.fields
                        .add(MapEntry("days_on[$i]", daysO[i].toString()));
                  }
                  for (int i = 0; i < days.length; i++) {
                    body.fields
                        .add(MapEntry("days_off[$i]", days[i].toString()));
                  }
                  if(widget.type == 0) {
                    context.read<HrCubit>().addEmployee(context, body);
                  }else{
                    print("aaaaaaa: 18");
                    context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
                  }
                } else if (selectedImageT.path != '' &&
                    selectedImage.path != '') {
                  FormData body = FormData.fromMap({
                    "name": addItem[0].con.text,
                    "email": addItem[1].con.text,
                    "mobile": addItem[2].con.text,
                    "id_number": addItem[3].con.text,
                    "bn": addItem[4].con.text,
                    "position": addItem[5].con.text,
                    "note": addItem[6].con.text,
                    "card_from": addItem[7].con.text,
                    "card_to": addItem[8].con.text,
                    "password": addItem[9].con.text,
                    "password_confirmation": addItem[10].con.text,
                    "is_admin":
                    context.read<HrCubit>().adminItem!.id.toString(),
                    "activity": context.read<HrCubit>().comItem!.id.toString(),
                    "department_id":
                    context.read<HrCubit>().depItem!.id.toString(),
                    "department_id2":
                    context.read<HrCubit>().secItem!.id.toString(),
                    "manager_id":
                    context.read<HrCubit>().secItem!.manager_id.toString(),
                    "is_open": _groupValue.toString(),
                    "places": space == null ? '' : space,
                    "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                    "start": startT.toString(),
                    "end": startE.toString(),
                    "auto": clickT == false ? '0' : '1',
                    "autostart": startTT.toString(),
                    "autoend": startET.toString(),
                    "signature": selectedImageT.path == ''
                        ? null
                        : await MultipartFile.fromFile(selectedImageT.path,
                        filename: selectedImageT.path.split('/').last),
                    "image": selectedImage.path == ''
                        ? null
                        : await MultipartFile.fromFile(selectedImage.path,
                        filename: selectedImage.path.split('/').last),
                    "send_sms": clicksms == false ? '0' : '1',
                    "send_whatsapp": clickw == false ? '0' : '1',
                    "send_email": clickE == false ? '0' : '1',
                    "status_id":
                    context.read<HrCubit>().item.toString() == 'active'.tr()
                        ? '1'
                        : '0',
                    "face_detection": clickF == false ? '0' : '1',
                    "country_id" : country!.id.toString(),
                    "violation_on_off" : clickV == false? '0' : '1',
                  });
                  for (int i = 0; i < daysO.length; i++) {
                    body.fields
                        .add(MapEntry("days_on[$i]", daysO[i].toString()));
                  }
                  for (int i = 0; i < days.length; i++) {
                    body.fields
                        .add(MapEntry("days_off[$i]", days[i].toString()));
                  }
                  if(widget.type == 0) {
                    context.read<HrCubit>().addEmployee(context, body);
                  }else{
                    print("aaaaaaa: 17");
                    context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
                  }
                }
              }
            } else {
              if (selectedImageT.path == '' && selectedImage.path == '') {
                FormData body = FormData.fromMap({
                  "name": addItem[0].con.text,
                  "email": addItem[1].con.text,
                  "mobile": addItem[2].con.text,
                  "id_number": addItem[3].con.text,
                  "bn": addItem[4].con.text,
                  "position": addItem[5].con.text,
                  "note": addItem[6].con.text,
                  "card_from": addItem[7].con.text,
                  "card_to": addItem[8].con.text,
                  "password": addItem[9].con.text,
                  "password_confirmation": addItem[10].con.text,
                  "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                  "activity": context.read<HrCubit>().comItem!.id.toString(),
                  "department_id":
                  context.read<HrCubit>().depItem!.id.toString(),
                  "department_id2":
                  context.read<HrCubit>().secItem!.id.toString(),
                  "manager_id":
                  context.read<HrCubit>().secItem!.manager_id.toString(),
                  "is_open": _groupValue.toString(),
                  "places": space == null ? '' : space,
                  "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                  "start": startT.toString(),
                  "end": startE.toString(),
                  "auto": clickT == false ? '0' : '1',
                  "autostart": startTT.toString(),
                  "autoend": startET.toString(),
                  "send_sms": clicksms == false ? '0' : '1',
                  "send_whatsapp": clickw == false ? '0' : '1',
                  "send_email": clickE == false ? '0' : '1',
                  "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                      ? '1'
                      : '0',
                  "face_detection": clickF == false ? '0' : '1',
                  "country_id" : country!.id.toString(),
                  "violation_on_off" : clickV == false? '0' : '1',
                });
                for (int i = 0; i < daysO.length; i++) {
                  body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
                }
                for (int i = 0; i < days.length; i++) {
                  body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
                }
                print(body.fields);
                if(widget.type == 0) {
                  context.read<HrCubit>().addEmployee(context, body);
                }else{
                  print("aaaaaaa: 16");
                  print("name: ${addItem[0].con.text}");
                  print("email: ${addItem[1].con.text}");
                  print("is_admin: ${context.read<HrCubit>().adminItem!.id.toString()}");
                  print("is_open: ${_groupValue.toString()}");
                  context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
                }
              } else if (selectedImageT.path == '' &&
                  selectedImage.path != '') {
                FormData body = FormData.fromMap({
                  "name": addItem[0].con.text,
                  "email": addItem[1].con.text,
                  "mobile": addItem[2].con.text,
                  "id_number": addItem[3].con.text,
                  "bn": addItem[4].con.text,
                  "position": addItem[5].con.text,
                  "note": addItem[6].con.text,
                  "card_from": addItem[7].con.text,
                  "card_to": addItem[8].con.text,
                  "password": addItem[9].con.text,
                  "password_confirmation": addItem[10].con.text,
                  "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                  "activity": context.read<HrCubit>().comItem!.id.toString(),
                  "department_id":
                  context.read<HrCubit>().depItem!.id.toString(),
                  "department_id2":
                  context.read<HrCubit>().secItem!.id.toString(),
                  "manager_id":
                  context.read<HrCubit>().secItem!.manager_id.toString(),
                  "is_open": _groupValue.toString(),
                  "places": space == null ? '' : space,
                  "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                  "start": startT.toString(),
                  "end": startE.toString(),
                  "auto": clickT == false ? '0' : '1',
                  "autostart": startTT.toString(),
                  "autoend": startET.toString(),
                  /* "signature": selectedImageT.path == ''
                                    ? null
                                    : await MultipartFile.fromFile(selectedImageT.path,
                                    filename: selectedImageT.path.split('/').last),*/
                  "image": selectedImage.path == ''
                      ? null
                      : await MultipartFile.fromFile(selectedImage.path,
                      filename: selectedImage.path.split('/').last),
                  "send_sms": clicksms == false ? '0' : '1',
                  "send_whatsapp": clickw == false ? '0' : '1',
                  "send_email": clickE == false ? '0' : '1',
                  "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                      ? '1'
                      : '0',
                  "face_detection": clickF == false ? '0' : '1',
                  "country_id" : country!.id.toString(),
                  "violation_on_off" : clickV == false? '0' : '1',
                });
                for (int i = 0; i < daysO.length; i++) {
                  body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
                }
                for (int i = 0; i < days.length; i++) {
                  body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
                }
                if(widget.type == 0) {
                  context.read<HrCubit>().addEmployee(context, body);
                }else{
                  print("aaaaaaa: 15");
                  context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
                }
              } else if (selectedImageT.path != '' &&
                  selectedImage.path == '') {
                FormData body = FormData.fromMap({
                  "name": addItem[0].con.text,
                  "email": addItem[1].con.text,
                  "mobile": addItem[2].con.text,
                  "id_number": addItem[3].con.text,
                  "bn": addItem[4].con.text,
                  "position": addItem[5].con.text,
                  "note": addItem[6].con.text,
                  "card_from": addItem[7].con.text,
                  "card_to": addItem[8].con.text,
                  "password": addItem[9].con.text,
                  "password_confirmation": addItem[10].con.text,
                  "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                  "activity": context.read<HrCubit>().comItem!.id.toString(),
                  "department_id":
                  context.read<HrCubit>().depItem!.id.toString(),
                  "department_id2":
                  context.read<HrCubit>().secItem!.id.toString(),
                  "manager_id":
                  context.read<HrCubit>().secItem!.manager_id.toString(),
                  "is_open": _groupValue.toString(),
                  "places": space == null ? '' : space,
                  "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                  "start": startT.toString(),
                  "end": startE.toString(),
                  "auto": clickT == false ? '0' : '1',
                  "autostart": startTT.toString(),
                  "autoend": startET.toString(),
                  "signature": selectedImageT.path == ''
                      ? null
                      : await MultipartFile.fromFile(selectedImageT.path,
                      filename: selectedImageT.path.split('/').last),
                  /*   "image": selectedImage.path == ''
                                    ? null
                                    : await MultipartFile.fromFile(selectedImage.path,
                                    filename: selectedImage.path.split('/').last),*/
                  "send_sms": clicksms == false ? '0' : '1',
                  "send_whatsapp": clickw == false ? '0' : '1',
                  "send_email": clickE == false ? '0' : '1',
                  "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                      ? '1'
                      : '0',
                  "face_detection": clickF == false ? '0' : '1',
                  "country_id" : country!.id.toString(),
                  "violation_on_off" : clickV == false? '0' : '1',
                });
                for (int i = 0; i < daysO.length; i++) {
                  body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
                }
                for (int i = 0; i < days.length; i++) {
                  body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
                }
                if(widget.type == 0) {
                  context.read<HrCubit>().addEmployee(context, body);
                }else{
                  print("aaaaaaa: 14");
                  context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
                }
              } else if (selectedImageT.path != '' &&
                  selectedImage.path != '') {
                FormData body = FormData.fromMap({
                  "name": addItem[0].con.text,
                  "email": addItem[1].con.text,
                  "mobile": addItem[2].con.text,
                  "id_number": addItem[3].con.text,
                  "bn": addItem[4].con.text,
                  "position": addItem[5].con.text,
                  "note": addItem[6].con.text,
                  "card_from": addItem[7].con.text,
                  "card_to": addItem[8].con.text,
                  "password": addItem[9].con.text,
                  "password_confirmation": addItem[10].con.text,
                  "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                  "activity": context.read<HrCubit>().comItem!.id.toString(),
                  "department_id":
                  context.read<HrCubit>().depItem!.id.toString(),
                  "department_id2":
                  context.read<HrCubit>().secItem!.id.toString(),
                  "manager_id":
                  context.read<HrCubit>().secItem!.manager_id.toString(),
                  "is_open": _groupValue.toString(),
                  "places": space == null ? '' : space,
                  "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                  "start": startT.toString(),
                  "end": startE.toString(),
                  "auto": clickT == false ? '0' : '1',
                  "autostart": startTT.toString(),
                  "autoend": startET.toString(),
                  "signature": selectedImageT.path == ''
                      ? null
                      : await MultipartFile.fromFile(selectedImageT.path,
                      filename: selectedImageT.path.split('/').last),
                  "image": selectedImage.path == ''
                      ? null
                      : await MultipartFile.fromFile(selectedImage.path,
                      filename: selectedImage.path.split('/').last),
                  "send_sms": clicksms == false ? '0' : '1',
                  "send_whatsapp": clickw == false ? '0' : '1',
                  "send_email": clickE == false ? '0' : '1',
                  "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                      ? '1'
                      : '0',
                  "face_detection": clickF == false ? '0' : '1',
                  "country_id" : country!.id.toString(),
                  "violation_on_off" : clickV == false? '0' : '1',
                });
                for (int i = 0; i < daysO.length; i++) {
                  body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
                }
                for (int i = 0; i < days.length; i++) {
                  body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
                }
                if(widget.type == 0) {
                  context.read<HrCubit>().addEmployee(context, body);
                }else{
                  print("aaaaaaa: 13");
                  context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
                }
              }
            }
          }
        } else {
          if (clickF == true && widget.type == 0) {
            if (selectedImage.path == '' || selectedImageT.path == '') {
              Fluttertoast.showToast(
                  msg: "input_employee_photo_and_signature".tr(),
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            }
          } else {
            if (selectedImageT.path == '' && selectedImage.path == '') {
              FormData body = FormData.fromMap({
                "name": addItem[0].con.text,
                "email": addItem[1].con.text,
                "mobile": addItem[2].con.text,
                "id_number": addItem[3].con.text,
                "bn": addItem[4].con.text,
                "position": addItem[5].con.text,
                "note": addItem[6].con.text,
                "card_from": addItem[7].con.text,
                "card_to": addItem[8].con.text,
                "password": addItem[9].con.text,
                "password_confirmation": addItem[10].con.text,
                "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                "activity": context.read<HrCubit>().comItem!.id.toString(),
                "department_id": context.read<HrCubit>().depItem!.id.toString(),
                "department_id2":
                context.read<HrCubit>().secItem!.id.toString(),
                "manager_id":
                context.read<HrCubit>().secItem!.manager_id.toString(),
                "is_open": _groupValue.toString(),
                "places": space == null ? '' : space,
                "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                "start": startT.toString(),
                "end": startE.toString(),
                "auto": clickT == false ? '0' : '1',
                "autostart": startTT.toString(),
                "autoend": startET.toString(),
                "send_sms": clicksms == false ? '0' : '1',
                "send_whatsapp": clickw == false ? '0' : '1',
                "send_email": clickE == false ? '0' : '1',
                "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                    ? '1'
                    : '0',
                "face_detection": clickF == false ? '0' : '1',
                "country_id" : country!.id.toString(),
                "violation_on_off" : clickV == false? '0' : '1',
              });
              for (int i = 0; i < daysO.length; i++) {
                body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
              }
              for (int i = 0; i < days.length; i++) {
                body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
              }
              print(body.fields);
              if(widget.type == 0) {
                context.read<HrCubit>().addEmployee(context, body);
              }else{
                print("aaaaaaa: 12");
                context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
              }
            } else if (selectedImageT.path == '' && selectedImage.path != '') {
              FormData body = FormData.fromMap({
                "name": addItem[0].con.text,
                "email": addItem[1].con.text,
                "mobile": addItem[2].con.text,
                "id_number": addItem[3].con.text,
                "bn": addItem[4].con.text,
                "position": addItem[5].con.text,
                "note": addItem[6].con.text,
                "card_from": addItem[7].con.text,
                "card_to": addItem[8].con.text,
                "password": addItem[9].con.text,
                "password_confirmation": addItem[10].con.text,
                "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                "activity": context.read<HrCubit>().comItem!.id.toString(),
                "department_id": context.read<HrCubit>().depItem!.id.toString(),
                "department_id2":
                context.read<HrCubit>().secItem!.id.toString(),
                "manager_id":
                context.read<HrCubit>().secItem!.manager_id.toString(),
                "is_open": _groupValue.toString(),
                "places": space == null ? '' : space,
                "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                "start": startT.toString(),
                "end": startE.toString(),
                "auto": clickT == false ? '0' : '1',
                "autostart": startTT.toString(),
                "autoend": startET.toString(),
                /* "signature": selectedImageT.path == ''
                                    ? null
                                    : await MultipartFile.fromFile(selectedImageT.path,
                                    filename: selectedImageT.path.split('/').last),*/
                "image": selectedImage.path == ''
                    ? null
                    : await MultipartFile.fromFile(selectedImage.path,
                    filename: selectedImage.path.split('/').last),
                "send_sms": clicksms == false ? '0' : '1',
                "send_whatsapp": clickw == false ? '0' : '1',
                "send_email": clickE == false ? '0' : '1',
                "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                    ? '1'
                    : '0',
                "face_detection": clickF == false ? '0' : '1',
                "country_id" : country!.id.toString(),
                "violation_on_off" : clickV == false? '0' : '1',
              });
              for (int i = 0; i < daysO.length; i++) {
                body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
              }
              for (int i = 0; i < days.length; i++) {
                body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
              }
              if(widget.type == 0) {
                context.read<HrCubit>().addEmployee(context, body);
              }else{
                print("aaaaaaa: 11");
                context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
              }
            } else if (selectedImageT.path != '' && selectedImage.path == '') {
              FormData body = FormData.fromMap({
                "name": addItem[0].con.text,
                "email": addItem[1].con.text,
                "mobile": addItem[2].con.text,
                "id_number": addItem[3].con.text,
                "bn": addItem[4].con.text,
                "position": addItem[5].con.text,
                "note": addItem[6].con.text,
                "card_from": addItem[7].con.text,
                "card_to": addItem[8].con.text,
                "password": addItem[9].con.text,
                "password_confirmation": addItem[10].con.text,
                "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                "activity": context.read<HrCubit>().comItem!.id.toString(),
                "department_id": context.read<HrCubit>().depItem!.id.toString(),
                "department_id2":
                context.read<HrCubit>().secItem!.id.toString(),
                "manager_id":
                context.read<HrCubit>().secItem!.manager_id.toString(),
                "is_open": _groupValue.toString(),
                "places": space == null ? '' : space,
                "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                "start": startT.toString(),
                "end": startE.toString(),
                "auto": clickT == false ? '0' : '1',
                "autostart": startTT.toString(),
                "autoend": startET.toString(),
                "signature": selectedImageT.path == ''
                    ? null
                    : await MultipartFile.fromFile(selectedImageT.path,
                    filename: selectedImageT.path.split('/').last),
                /*   "image": selectedImage.path == ''
                                    ? null
                                    : await MultipartFile.fromFile(selectedImage.path,
                                    filename: selectedImage.path.split('/').last),*/
                "send_sms": clicksms == false ? '0' : '1',
                "send_whatsapp": clickw == false ? '0' : '1',
                "send_email": clickE == false ? '0' : '1',
                "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                    ? '1'
                    : '0',
                "face_detection": clickF == false ? '0' : '1',
                "country_id" : country!.id.toString(),
                "violation_on_off" : clickV == false? '0' : '1',
              });
              for (int i = 0; i < daysO.length; i++) {
                body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
              }
              for (int i = 0; i < days.length; i++) {
                body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
              }
              if(widget.type == 0) {
                context.read<HrCubit>().addEmployee(context, body);
              }else{
                print("aaaaaaa: 10");
                context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
              }
            } else if (selectedImageT.path != '' && selectedImage.path != '') {
              FormData body = FormData.fromMap({
                "name": addItem[0].con.text,
                "email": addItem[1].con.text,
                "mobile": addItem[2].con.text,
                "id_number": addItem[3].con.text,
                "bn": addItem[4].con.text,
                "position": addItem[5].con.text,
                "note": addItem[6].con.text,
                "card_from": addItem[7].con.text,
                "card_to": addItem[8].con.text,
                "password": addItem[9].con.text,
                "password_confirmation": addItem[10].con.text,
                "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                "activity": context.read<HrCubit>().comItem!.id.toString(),
                "department_id": context.read<HrCubit>().depItem!.id.toString(),
                "department_id2":
                context.read<HrCubit>().secItem!.id.toString(),
                "manager_id":
                context.read<HrCubit>().secItem!.manager_id.toString(),
                "is_open": _groupValue.toString(),
                "places": space == null ? '' : space,
                "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                "start": startT.toString(),
                "end": startE.toString(),
                "auto": clickT == false ? '0' : '1',
                "autostart": startTT.toString(),
                "autoend": startET.toString(),
                "signature": selectedImageT.path == ''
                    ? null
                    : await MultipartFile.fromFile(selectedImageT.path,
                    filename: selectedImageT.path.split('/').last),
                "image": selectedImage.path == ''
                    ? null
                    : await MultipartFile.fromFile(selectedImage.path,
                    filename: selectedImage.path.split('/').last),
                "send_sms": clicksms == false ? '0' : '1',
                "send_whatsapp": clickw == false ? '0' : '1',
                "send_email": clickE == false ? '0' : '1',
                "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                    ? '1'
                    : '0',
                "face_detection": clickF == false ? '0' : '1',
                "country_id" : country!.id.toString(),
                "violation_on_off" : clickV == false? '0' : '1',
              });
              for (int i = 0; i < daysO.length; i++) {
                body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
              }
              for (int i = 0; i < days.length; i++) {
                body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
              }
              if(widget.type == 0) {
                context.read<HrCubit>().addEmployee(context, body);
              }else{
                print("aaaaaaa: 9");
                context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
              }
            }
          }
        }
      }
    } else {
      if (clickT == true) {
        if (startTT == null || startET == null || days.length == 0) {
          Fluttertoast.showToast(
              msg:
              "enter_start_and_end_work_dates2".tr(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
        } else {
          if (clickF == true && widget.type == 0) {
            if (selectedImage.path == '' || selectedImageT.path == '') {
              Fluttertoast.showToast(
                  msg: "input_employee_photo_and_signature".tr(),
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIosWeb: 1,
                  backgroundColor: Colors.red,
                  textColor: Colors.white,
                  fontSize: 16.0);
            }
          } else {
            if (selectedImageT.path == '' && selectedImage.path == '') {
              FormData body = FormData.fromMap({
                "name": addItem[0].con.text,
                "email": addItem[1].con.text,
                "mobile": addItem[2].con.text,
                "id_number": addItem[3].con.text,
                "bn": addItem[4].con.text,
                "position": addItem[5].con.text,
                "note": addItem[6].con.text,
                "card_from": addItem[7].con.text,
                "card_to": addItem[8].con.text,
                "password": addItem[9].con.text,
                "password_confirmation": addItem[10].con.text,
                "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                "activity": context.read<HrCubit>().comItem!.id.toString(),
                "department_id": context.read<HrCubit>().depItem!.id.toString(),
                "department_id2":
                context.read<HrCubit>().secItem!.id.toString(),
                "manager_id":
                context.read<HrCubit>().secItem!.manager_id.toString(),
                "is_open": _groupValue.toString(),
                "places": space == null ? '' : space,
                "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                "start": startT.toString(),
                "end": startE.toString(),
                "auto": clickT == false ? '0' : '1',
                "autostart": startTT.toString(),
                "autoend": startET.toString(),
                "send_sms": clicksms == false ? '0' : '1',
                "send_whatsapp": clickw == false ? '0' : '1',
                "send_email": clickE == false ? '0' : '1',
                "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                    ? '1'
                    : '0',
                "face_detection": clickF == false ? '0' : '1',
                "country_id" : country!.id.toString(),
                "violation_on_off" : clickV == false? '0' : '1',
              });
              for (int i = 0; i < daysO.length; i++) {
                body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
              }
              for (int i = 0; i < days.length; i++) {
                body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
              }
              if(widget.type == 0) {
                context.read<HrCubit>().addEmployee(context, body);
              }else{
                print("aaaaaaa: 8");
                context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
              }
            } else if (selectedImageT.path == '' && selectedImage.path != '') {
              FormData body = FormData.fromMap({
                "name": addItem[0].con.text,
                "email": addItem[1].con.text,
                "mobile": addItem[2].con.text,
                "id_number": addItem[3].con.text,
                "bn": addItem[4].con.text,
                "position": addItem[5].con.text,
                "note": addItem[6].con.text,
                "card_from": addItem[7].con.text,
                "card_to": addItem[8].con.text,
                "password": addItem[9].con.text,
                "password_confirmation": addItem[10].con.text,
                "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                "activity": context.read<HrCubit>().comItem!.id.toString(),
                "department_id": context.read<HrCubit>().depItem!.id.toString(),
                "department_id2":
                context.read<HrCubit>().secItem!.id.toString(),
                "manager_id":
                context.read<HrCubit>().secItem!.manager_id.toString(),
                "is_open": _groupValue.toString(),
                "places": space == null ? '' : space,
                "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                "start": startT.toString(),
                "end": startE.toString(),
                "auto": clickT == false ? '0' : '1',
                "autostart": startTT.toString(),
                "autoend": startET.toString(),
                /* "signature": selectedImageT.path == ''
                                    ? null
                                    : await MultipartFile.fromFile(selectedImageT.path,
                                    filename: selectedImageT.path.split('/').last),*/
                "image": selectedImage.path == ''
                    ? null
                    : await MultipartFile.fromFile(selectedImage.path,
                    filename: selectedImage.path.split('/').last),
                "send_sms": clicksms == false ? '0' : '1',
                "send_whatsapp": clickw == false ? '0' : '1',
                "send_email": clickE == false ? '0' : '1',
                "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                    ? '1'
                    : '0',
                "face_detection": clickF == false ? '0' : '1',
                "country_id" : country!.id.toString(),
                "violation_on_off" : clickV == false? '0' : '1',
              });
              for (int i = 0; i < daysO.length; i++) {
                body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
              }
              for (int i = 0; i < days.length; i++) {
                body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
              }
              if(widget.type == 0) {
                context.read<HrCubit>().addEmployee(context, body);
              }else{
                print("aaaaaaa: 7");
                context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
              }
            } else if (selectedImageT.path != '' && selectedImage.path == '') {
              FormData body = FormData.fromMap({
                "name": addItem[0].con.text,
                "email": addItem[1].con.text,
                "mobile": addItem[2].con.text,
                "id_number": addItem[3].con.text,
                "bn": addItem[4].con.text,
                "position": addItem[5].con.text,
                "note": addItem[6].con.text,
                "card_from": addItem[7].con.text,
                "card_to": addItem[8].con.text,
                "password": addItem[9].con.text,
                "password_confirmation": addItem[10].con.text,
                "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                "activity": context.read<HrCubit>().comItem!.id.toString(),
                "department_id": context.read<HrCubit>().depItem!.id.toString(),
                "department_id2":
                context.read<HrCubit>().secItem!.id.toString(),
                "manager_id":
                context.read<HrCubit>().secItem!.manager_id.toString(),
                "is_open": _groupValue.toString(),
                "places": space == null ? '' : space,
                "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                "start": startT.toString(),
                "end": startE.toString(),
                "auto": clickT == false ? '0' : '1',
                "autostart": startTT.toString(),
                "autoend": startET.toString(),
                "signature": selectedImageT.path == ''
                    ? null
                    : await MultipartFile.fromFile(selectedImageT.path,
                    filename: selectedImageT.path.split('/').last),
                /*   "image": selectedImage.path == ''
                                    ? null
                                    : await MultipartFile.fromFile(selectedImage.path,
                                    filename: selectedImage.path.split('/').last),*/
                "send_sms": clicksms == false ? '0' : '1',
                "send_whatsapp": clickw == false ? '0' : '1',
                "send_email": clickE == false ? '0' : '1',
                "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                    ? '1'
                    : '0',
                "face_detection": clickF == false ? '0' : '1',
                "country_id" : country!.id.toString(),
                "violation_on_off" : clickV == false? '0' : '1',
              });
              for (int i = 0; i < daysO.length; i++) {
                body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
              }
              for (int i = 0; i < days.length; i++) {
                body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
              }
              if(widget.type == 0) {
                context.read<HrCubit>().addEmployee(context, body);
              }else{
                print("aaaaaaa: 6");
                context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
              }
            } else if (selectedImageT.path != '' && selectedImage.path != '') {
              FormData body = FormData.fromMap({
                "name": addItem[0].con.text,
                "email": addItem[1].con.text,
                "mobile": addItem[2].con.text,
                "id_number": addItem[3].con.text,
                "bn": addItem[4].con.text,
                "position": addItem[5].con.text,
                "note": addItem[6].con.text,
                "card_from": addItem[7].con.text,
                "card_to": addItem[8].con.text,
                "password": addItem[9].con.text,
                "password_confirmation": addItem[10].con.text,
                "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
                "activity": context.read<HrCubit>().comItem!.id.toString(),
                "department_id": context.read<HrCubit>().depItem!.id.toString(),
                "department_id2":
                context.read<HrCubit>().secItem!.id.toString(),
                "manager_id":
                context.read<HrCubit>().secItem!.manager_id.toString(),
                "is_open": _groupValue.toString(),
                "places": space == null ? '' : space,
                "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
                "start": startT.toString(),
                "end": startE.toString(),
                "auto": clickT == false ? '0' : '1',
                "autostart": startTT.toString(),
                "autoend": startET.toString(),
                "signature": selectedImageT.path == ''
                    ? null
                    : await MultipartFile.fromFile(selectedImageT.path,
                    filename: selectedImageT.path.split('/').last),
                "image": selectedImage.path == ''
                    ? null
                    : await MultipartFile.fromFile(selectedImage.path,
                    filename: selectedImage.path.split('/').last),
                "send_sms": clicksms == false ? '0' : '1',
                "send_whatsapp": clickw == false ? '0' : '1',
                "send_email": clickE == false ? '0' : '1',
                "status_id": context.read<HrCubit>().item.toString() == 'active'.tr()
                    ? '1'
                    : '0',
                "face_detection": clickF == false ? '0' : '1',
                "country_id" : country!.id.toString(),
                "violation_on_off" : clickV == false? '0' : '1',
              });
              for (int i = 0; i < daysO.length; i++) {
                body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
              }
              for (int i = 0; i < days.length; i++) {
                body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
              }
              if(widget.type == 0) {
                context.read<HrCubit>().addEmployee(context, body);
              }else{
                print("aaaaaaa: 5");
                context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
              }
            }
          }
        }
      } else {
        if (clickF == true && widget.type == 0) {
          if (selectedImage.path == '' || selectedImageT.path == '') {
            Fluttertoast.showToast(
                msg: "input_employee_photo_and_signature".tr(),
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 1,
                backgroundColor: Colors.red,
                textColor: Colors.white,
                fontSize: 16.0);
          }
        } else {
          if (selectedImageT.path == '' && selectedImage.path == '') {
            FormData body = FormData.fromMap({
              "name": addItem[0].con.text,
              "email": addItem[1].con.text,
              "mobile": addItem[2].con.text,
              "id_number": addItem[3].con.text,
              "bn": addItem[4].con.text,
              "position": addItem[5].con.text,
              "note": addItem[6].con.text,
              "card_from": addItem[7].con.text,
              "card_to": addItem[8].con.text,
              "password": addItem[9].con.text,
              "password_confirmation": addItem[10].con.text,
              "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
              "activity": context.read<HrCubit>().comItem!.id.toString(),
              "department_id": context.read<HrCubit>().depItem!.id.toString(),
              "department_id2": context.read<HrCubit>().secItem!.id.toString(),
              "manager_id":
              context.read<HrCubit>().secItem!.manager_id.toString(),
              "is_open": _groupValue.toString(),
              "places": space == null ? '' : space,
              "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
              "start": startT.toString(),
              "end": startE.toString(),
              "auto": clickT == false ? '0' : '1',
              "autostart": startTT.toString(),
              "autoend": startET.toString(),
              "send_sms": clicksms == false ? '0' : '1',
              "send_whatsapp": clickw == false ? '0' : '1',
              "send_email": clickE == false ? '0' : '1',
              "status_id":
              context.read<HrCubit>().item.toString() == 'active'.tr() ? '1' : '0',
              "face_detection": clickF == false ? '0' : '1',
              "country_id" : country!.id.toString(),
              "violation_on_off" : clickV == false? '0' : '1',
            });
            print(body.fields);
            for (int i = 0; i < daysO.length; i++) {
              body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
            }
            for (int i = 0; i < days.length; i++) {
              body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
            }
            if(widget.type == 0) {
              context.read<HrCubit>().addEmployee(context, body);
            }else{
              print("aaaaaaa: 4");
              context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
            }
          } else if (selectedImageT.path == '' && selectedImage.path != '') {
            FormData body = FormData.fromMap({
              "name": addItem[0].con.text,
              "email": addItem[1].con.text,
              "mobile": addItem[2].con.text,
              "id_number": addItem[3].con.text,
              "bn": addItem[4].con.text,
              "position": addItem[5].con.text,
              "note": addItem[6].con.text,
              "card_from": addItem[7].con.text,
              "card_to": addItem[8].con.text,
              "password": addItem[9].con.text,
              "password_confirmation": addItem[10].con.text,
              "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
              "activity": context.read<HrCubit>().comItem!.id.toString(),
              "department_id": context.read<HrCubit>().depItem!.id.toString(),
              "department_id2": context.read<HrCubit>().secItem!.id.toString(),
              "manager_id":
              context.read<HrCubit>().secItem!.manager_id.toString(),
              "is_open": _groupValue.toString(),
              "places": space == null ? '' : space,
              "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
              "start": startT.toString(),
              "end": startE.toString(),
              "auto": clickT == false ? '0' : '1',
              "autostart": startTT.toString(),
              "autoend": startET.toString(),
              /* "signature": selectedImageT.path == ''
                                    ? null
                                    : await MultipartFile.fromFile(selectedImageT.path,
                                    filename: selectedImageT.path.split('/').last),*/
              "image": selectedImage.path == ''
                  ? null
                  : await MultipartFile.fromFile(selectedImage.path,
                  filename: selectedImage.path.split('/').last),
              "send_sms": clicksms == false ? '0' : '1',
              "send_whatsapp": clickw == false ? '0' : '1',
              "send_email": clickE == false ? '0' : '1',
              "status_id":
              context.read<HrCubit>().item.toString() == 'active'.tr() ? '1' : '0',
              "face_detection": clickF == false ? '0' : '1',
              "country_id" : country!.id.toString(),
              "violation_on_off" : clickV == false? '0' : '1',
            });
            for (int i = 0; i < daysO.length; i++) {
              body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
            }
            for (int i = 0; i < days.length; i++) {
              body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
            }
            if(widget.type == 0) {
              context.read<HrCubit>().addEmployee(context, body);
            }else{
              print("aaaaaaa: 3");
              context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
            }
          } else if (selectedImageT.path != '' && selectedImage.path == '') {
            FormData body = FormData.fromMap({
              "name": addItem[0].con.text,
              "email": addItem[1].con.text,
              "mobile": addItem[2].con.text,
              "id_number": addItem[3].con.text,
              "bn": addItem[4].con.text,
              "position": addItem[5].con.text,
              "note": addItem[6].con.text,
              "card_from": addItem[7].con.text,
              "card_to": addItem[8].con.text,
              "password": addItem[9].con.text,
              "password_confirmation": addItem[10].con.text,
              "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
              "activity": context.read<HrCubit>().comItem!.id.toString(),
              "department_id": context.read<HrCubit>().depItem!.id.toString(),
              "department_id2": context.read<HrCubit>().secItem!.id.toString(),
              "manager_id":
              context.read<HrCubit>().secItem!.manager_id.toString(),
              "is_open": _groupValue.toString(),
              "places": space == null ? '' : space,
              "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
              "start": startT.toString(),
              "end": startE.toString(),
              "auto": clickT == false ? '0' : '1',
              "autostart": startTT.toString(),
              "autoend": startET.toString(),
              "signature": selectedImageT.path == ''
                  ? null
                  : await MultipartFile.fromFile(selectedImageT.path,
                  filename: selectedImageT.path.split('/').last),
              /*   "image": selectedImage.path == ''
                                    ? null
                                    : await MultipartFile.fromFile(selectedImage.path,
                                    filename: selectedImage.path.split('/').last),*/
              "send_sms": clicksms == false ? '0' : '1',
              "send_whatsapp": clickw == false ? '0' : '1',
              "send_email": clickE == false ? '0' : '1',
              "status_id":
              context.read<HrCubit>().item.toString() == 'active'.tr() ? '1' : '0',
              "face_detection": clickF == false ? '0' : '1',
              "country_id" : country!.id.toString(),
              "violation_on_off" : clickV == false? '0' : '1',
            });
            for (int i = 0; i < daysO.length; i++) {
              body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
            }
            for (int i = 0; i < days.length; i++) {
              body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
            }
            if(widget.type == 0) {
              context.read<HrCubit>().addEmployee(context, body);
            }else{
              print("aaaaaaa: 2");
              context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
            }
          } else if (selectedImageT.path != '' && selectedImage.path != '') {
            FormData body = FormData.fromMap({
              "name": addItem[0].con.text,
              "email": addItem[1].con.text,
              "mobile": addItem[2].con.text,
              "id_number": addItem[3].con.text,
              "bn": addItem[4].con.text,
              "position": addItem[5].con.text,
              "note": addItem[6].con.text,
              "card_from": addItem[7].con.text,
              "card_to": addItem[8].con.text,
              "password": addItem[9].con.text,
              "password_confirmation": addItem[10].con.text,
              "is_admin": context.read<HrCubit>().adminItem!.id.toString(),
              "activity": context.read<HrCubit>().comItem!.id.toString(),
              "department_id": context.read<HrCubit>().depItem!.id.toString(),
              "department_id2": context.read<HrCubit>().secItem!.id.toString(),
              "manager_id":
              context.read<HrCubit>().secItem!.manager_id.toString(),
              "is_open": _groupValue.toString(),
              "places": space == null ? '' : space,
              "special": click == false ? widget.type == 0? '0' : "off" : widget.type == 0? '1' : "on",
              "start": startT.toString(),
              "end": startE.toString(),
              "auto": clickT == false ? '0' : '1',
              "autostart": startTT.toString(),
              "autoend": startET.toString(),
              "signature": selectedImageT.path == ''
                  ? null
                  : await MultipartFile.fromFile(selectedImageT.path,
                  filename: selectedImageT.path.split('/').last),
              "image": selectedImage.path == ''
                  ? null
                  : await MultipartFile.fromFile(selectedImage.path,
                  filename: selectedImage.path.split('/').last),
              "send_sms": clicksms == false ? '0' : '1',
              "send_whatsapp": clickw == false ? '0' : '1',
              "send_email": clickE == false ? '0' : '1',
              "status_id":
              context.read<HrCubit>().item.toString() == 'active'.tr() ? '1' : '0',
              "face_detection": clickF == false ? '0' : '1',
              "country_id" : country!.id.toString(),
              "violation_on_off" : clickV == false? '0' : '1',
            });
            for (int i = 0; i < daysO.length; i++) {
              body.fields.add(MapEntry("days_on[$i]", daysO[i].toString()));
            }
            for (int i = 0; i < days.length; i++) {
              body.fields.add(MapEntry("days_off[$i]", days[i].toString()));
            }
            if(widget.type == 0) {
              context.read<HrCubit>().addEmployee(context, body);
            }else{
              print("aaaaaaa: 1");
              context.read<HrCubit>().updateEmployee(context, body, widget.item.id!);
            }
          }
        }
      }
    }
  }
}
