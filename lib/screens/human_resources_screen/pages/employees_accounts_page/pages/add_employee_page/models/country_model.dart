import 'package:equatable/equatable.dart';

class CountryModel extends Equatable{
  final int? id;
  final String? name;

  CountryModel({
    this.id,
    this.name
  });

  @override
  // TODO: implement props
  List<Object?> get props => [id, name];
}