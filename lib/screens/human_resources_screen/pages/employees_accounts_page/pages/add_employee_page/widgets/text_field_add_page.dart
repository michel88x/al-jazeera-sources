import 'package:dynamic_height_grid_view/dynamic_height_grid_view.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/screens/human_resources_screen/bloc/hr_cubit.dart';
import 'package:employee/model/add_model.dart';
import 'package:employee/screens/human_resources_screen/repository/hr_repository.dart';
import 'package:employee/screens/human_resources_screen/bloc/hr_state.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class TextFieldAddPage extends StatelessWidget {
  AddModel item;
  TextFieldAddPage(this.item, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.3),
              spreadRadius: 1,
              blurRadius: 2)
        ],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
          child: TextField(
            controller: item.con,
            onTap: () {
              item.con.selection = TextSelection.fromPosition(
                  TextPosition(offset: item.con.text.length));
            },
            obscureText: item.pass==1?true:false,

            keyboardType:item.num==1?TextInputType.number:TextInputType.emailAddress ,
            decoration: InputDecoration(
              border: InputBorder.none,
              errorText: item.val ? 'field_required'.tr() : null,

              hintText: item.hint,
              hintStyle: AppStyles.tajawalNormalGreyOpacity,
              contentPadding: const EdgeInsets.symmetric(horizontal: 0, vertical: 5),
            ),
          )),
    );
  }
}
