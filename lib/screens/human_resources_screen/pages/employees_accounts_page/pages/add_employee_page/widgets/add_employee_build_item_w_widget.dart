import 'package:flutter/material.dart';

class AddEmployeeBuildItemWWidget extends StatelessWidget {

  final String text;
  final int value;
  final FocusNode focusNode;
  final int? groupValue; //_groupValueS
  final Function(int?) onChanged;

  const AddEmployeeBuildItemWWidget({Key? key, required this.text, required this.value, required this.focusNode, required this.groupValue, required this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150,
      child: ListTile(
        title: Text(text),
        leading: Radio<int>(
          groupValue:groupValue,
          value: value,
          onChanged: onChanged,
          hoverColor: Colors.yellow,
          activeColor: Colors.pink,
          focusColor: Colors.purple,
          fillColor:
          MaterialStateColor.resolveWith((Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered)) {
              return Colors.orange;
            } else if (states.contains(MaterialState.selected)) {
              return Colors.teal;
            }
            if (states.contains(MaterialState.focused)) {
              return Colors.blue;
            } else {
              return Colors.black12;
            }
          }),
          overlayColor:
          MaterialStateColor.resolveWith((Set<MaterialState> states) {
            if (states.contains(MaterialState.hovered)) {
              return Colors.lightGreenAccent;
            }
            if (states.contains(MaterialState.focused)) {
              return Colors.brown;
            } else {
              return Colors.white;
            }
          }),
          splashRadius: 25,
          toggleable: true,
          visualDensity: VisualDensity.compact,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          //   focusNode: focusNode,
        ),
      ),
    );
  }
}
