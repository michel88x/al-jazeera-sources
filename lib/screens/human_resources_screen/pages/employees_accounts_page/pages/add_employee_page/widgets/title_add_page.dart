
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TitleAddPage extends StatelessWidget {
  String title;
  TitleAddPage(this.title);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(title,style: AppStyles.cairoFifteenPrimaryBold),
      ],
    );
  }
}
