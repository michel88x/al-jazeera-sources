import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:flutter/material.dart';

class ForgetStepperTitles extends StatelessWidget {
  final int click;

  const ForgetStepperTitles({Key? key, required this.click}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return click == 1
        ? Padding(
            padding: const EdgeInsets.fromLTRB(20, 50, 20, 0.0),
            child: Row(
              children: [
                Text('email'.tr(), style: AppStyles.aleoTwelvePrimaryBold),
              ],
            ),
          )
        : click == 2
            ? Padding(
                padding: const EdgeInsets.fromLTRB(20, 50, 20, 0.0),
                child: Row(
                  children: [
                    Text('verification_code'.tr(),
                        style: AppStyles.aleoTwelvePrimaryBold),
                  ],
                ),
              )
            : Padding(
                padding: const EdgeInsets.fromLTRB(20, 50, 20, 0.0),
                child: Row(
                  children: [
                    Text('submit_password'.tr(),
                        style: AppStyles.aleoTwelvePrimaryBold),
                  ],
                ),
              );
  }
}
