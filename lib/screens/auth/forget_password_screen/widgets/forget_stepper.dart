import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:flutter/material.dart';

class ForgetStepper extends StatelessWidget {

  final int click;
  const ForgetStepper({Key? key, required this.click}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: const BoxDecoration(
              color: AppColors.secondary, shape: BoxShape.circle),
          child: click == 1
              ? Padding(
            padding: const EdgeInsets.all(10),
            child: Text(click.toString(),
                style: AppStyles.cairoTwelveWhiteBold,
                textAlign: TextAlign.center),
          )
              : const Padding(
            padding: EdgeInsets.all(7),
            child: Icon(Icons.check,
                color: Colors.white, size: 13),
          ),
        ),
        Container(height: 1,width: 100,color: Colors.grey.withOpacity(0.4),),
        Container(
          decoration: const BoxDecoration(
              color: AppColors.secondary, shape: BoxShape.circle),
          child: click == 1
              ? Padding(
            padding: const EdgeInsets.all(10),
            child: Text('2',
                style: AppStyles.cairoTwelveWhiteBold,
                textAlign: TextAlign.center),
          ): click == 2
              ? Padding(
            padding: const EdgeInsets.all(10),
            child: Text(click.toString(),
                style: AppStyles.cairoTwelveWhiteBold,
                textAlign: TextAlign.center),
          )
              : const Padding(
            padding: EdgeInsets.all(7),
            child: Icon(Icons.check,
                color: Colors.white, size: 13),
          ),
        ),
        Container(height: 1,width: 100,color: Colors.grey.withOpacity(0.4),),
        Container(
          decoration: const BoxDecoration(
              color: AppColors.secondary, shape: BoxShape.circle),
          child:
          click == 1
              ? Padding(
            padding: const EdgeInsets.all(10),
            child: Text('3',
                style: AppStyles.cairoTwelveWhiteBold,
                textAlign: TextAlign.center),
          ):
          click == 2
              ? Padding(
            padding: const EdgeInsets.all(10),
            child: Text('3',
                style: AppStyles.cairoTwelveWhiteBold,
                textAlign: TextAlign.center),
          ):
          click == 3
              ? Padding(
            padding: const EdgeInsets.all(10),
            child: Text(click.toString(),
                style: AppStyles.cairoTwelveWhiteBold,
                textAlign: TextAlign.center),
          )
              : const Padding(
            padding: EdgeInsets.all(7),
            child: Icon(Icons.check,
                color: Colors.white, size: 13),
          ),
        ),
      ],
    );
  }
}
