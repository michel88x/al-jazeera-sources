import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_images.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/base_widgets/custom_button.dart';
import 'package:employee/core/base_widgets/custom_text_field.dart';
import 'package:employee/screens/auth/forget_password_screen/widgets/forget_stepper.dart';
import 'package:employee/screens/auth/forget_password_screen/widgets/forget_stepper_titles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';

class ForgetPasswordScreen extends StatefulWidget {
  @override
  State<ForgetPasswordScreen> createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  bool showLog = true;
  TextEditingController emailLog = TextEditingController();
  TextEditingController passLog = TextEditingController();
  bool showLogn = true;
  String code = '';
  TextEditingController passLogn = TextEditingController();
  int click =1;
  bool _onEditing = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.zero,
          children: [
            Column(
              children: [
                const SizedBox(height: 80),
                Center(
                  child: Image.asset(
                    AppImages.logo,
                    height: 150,
                    width: 150,
                  ),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0.0),
                  child: ForgetStepper(click: click),
                ),
                ForgetStepperTitles(click: click,),
                click == 1
                    ? Padding(
                        padding: const EdgeInsets.fromLTRB(20, 5, 20, 0.0),
                        child: CustomTextField(
                          width: MediaQuery.of(context).size.width - 50,
                          borderRadius: 15,
                          borderColor: AppColors.primary.withOpacity(0.7),
                          backColor: Colors.white,
                            boxPaddings: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                            controller: emailLog,
                            label: "email".tr(),
                            prefixIcon: const Icon(
                              Icons.email,
                              color: AppColors.secondary,
                              size: 20,
                            ),
                            labelStyle: AppStyles.tajawalNormalGrey,
                            fieldVerticalPadding: 5,
                          fieldHorizontalPadding: 0,
                        ),
                      )
                    :click == 2? Padding(
                        padding: const EdgeInsets.fromLTRB(20, 5, 20, 0.0),
                        child: VerificationCode(
                          textStyle: AppStyles.cairoTwentyBlackBold,
                          keyboardType: TextInputType.number,
                          underlineColor: AppColors.secondary,
                          // If this is null it will use primaryColor: Colors.red from Theme
                          length: 4,
                          cursorColor: AppColors.primary,
                          // If this is null it will default to the ambient
                          // clearAll is NOT required, you can delete it
                          // takes any widget, so you can implement your design
                          clearAll: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'resend_code'.tr(),
                              style: AppStyles.cairoFourteenBlueUnderline,
                            ),
                          ),
                          onCompleted: (String value) {},
                          onEditing: (bool value) {
                            setState(() {
                              _onEditing = value;
                            });
                            if (!_onEditing) FocusScope.of(context).unfocus();
                          },
                          isSecure: true,
                          autofocus: true,
                          margin: const EdgeInsets.all(5),
                          fullBorder: true,
                        ),
                      ):
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 5, 20, 0.0),
                  child:
                  CustomTextField(
                      width: MediaQuery.of(context).size.width - 50,
                      borderRadius: 15,
                      borderColor: AppColors.primary.withOpacity(0.7),
                      backColor: Colors.white,
                      boxPaddings: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                      controller: passLog,
                      label: "new_password".tr(),
                      prefixIcon: const Icon(
                        Icons.lock,
                        color: AppColors.secondary,
                        size: 20,
                      ),
                      labelStyle: AppStyles.tajawalNormalGrey,
                      isPassword: true,
                      fieldVerticalPadding: 5,
                      fieldHorizontalPadding: 0
                  ),
                ),
                click==3?
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 15, 20, 0.0),
                  child:
                  CustomTextField(
                    width: MediaQuery.of(context).size.width - 50,
                      borderRadius: 15,
                      borderColor: AppColors.primary.withOpacity(0.7),
                      backColor: Colors.white,
                      boxPaddings: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                      controller: passLogn,
                      label: "confirm_password".tr(),
                      prefixIcon: const Icon(
                        Icons.lock,
                        color: AppColors.secondary,
                        size: 20,
                      ),
                      labelStyle: AppStyles.tajawalNormalGrey,
                      isPassword: true,
                      fieldVerticalPadding: 5,
                    fieldHorizontalPadding: 0,
                  ),
                ):const Visibility(visible: false,child: Text(''),),
                click == 1
                    ? CustomButton(
                  onPressed: (){
                    setState(() {
                      click = 2;
                    });
                  },
                    padding: const EdgeInsets.fromLTRB(35, 55, 35, 0.0),
                    backColor: AppColors.secondary,
                    borderRadius: 15,
                    innerPadding: const EdgeInsets.fromLTRB(100, 10, 100, 10),
                    title: "send_verification_code".tr(),
                    textStyle: AppStyles.aleoFifteenWhiteBold
                ):click == 2?
                    CustomButton(
                      onPressed: (){
                        setState(() {
                          click = 3;
                        });
                      },
                        padding: const EdgeInsets.fromLTRB(35, 55, 35, 0.0),
                        backColor: AppColors.secondary,
                        borderRadius: 15,
                        innerPadding: const EdgeInsets.fromLTRB(100, 10, 100, 10),
                        title: 'confirm'.tr(),
                        textStyle: AppStyles.aleoFifteenWhiteBold
                    ) :
                CustomButton(
                    onPressed: (){
                      setState(() {});
                    },
                    padding: const EdgeInsets.fromLTRB(35, 55, 35, 0.0),
                    backColor: AppColors.secondary,
                    borderRadius: 15,
                    innerPadding: const EdgeInsets.fromLTRB(100, 10, 100, 10),
                    title: 'send'.tr(),
                    textStyle: AppStyles.aleoFifteenWhiteBold
                )
              ],
            ),
          ],
        ));
  }
}
