part of 'otp_cubit.dart';

@immutable
abstract class OtpState {}

class OtpInitial extends OtpState {}

class LoginInitial extends OtpState {
}

class LoginLoaded extends OtpState {

}
class LoginSuccess extends OtpState {
  final LoginResponse data;
  LoginSuccess({required this.data});
}

class LoginError extends OtpState {
  final String message;
  LoginError({required this.message});
}
