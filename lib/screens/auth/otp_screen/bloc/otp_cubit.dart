import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/constants/enums.dart';
import 'package:employee/core/network/models/s_base_response.dart';
import 'package:employee/core/network/urls.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:employee/core/utils/shared_pref.dart';
import 'package:employee/screens/auth/login_screen/models/login_response.dart';
import 'package:employee/screens/general/privacy_screen/view/privacy_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'otp_state.dart';

class OtpCubit extends Cubit<OtpState> {

  OtpCubit() : super(LoginInitial());

  static OtpCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  Future<void> resendCode(BuildContext context) async {
    emit(LoginLoaded());
    String? token = await getToken();
    SBaseResponse? response = await callObjectAPI(method: APIMethod.post, accessToken: token, url: Urls.resendCode);
    if(response != null && response.success){
      showSuccessMessage("code_sent".tr());
      emit(LoginInitial());
    }else{
      showFailureMessage("code_sent_error".tr());
      emit(LoginInitial());
    }
  }

  Future<void> checkCode(BuildContext context, String code) async {
    try {
      if (code == '') {
        showFailureMessage("enter_verification_code".tr());
      } else {
        emit(LoginLoaded());
        String? token = await getToken();
        SBaseResponse? response = await callObjectAPI(method: APIMethod.post, accessToken: token, stringBody: json.encode(
          {
            "code" : code
          }
        ), url: Urls.confirmOtp);

        if(response != null && response.success){
          await SharedPref.saveString(SharedPref.log, '1');
          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
            goto(context, PrivacyScreen());
          });
          emit(LoginSuccess(data: LoginResponse()));
        }else{
          SharedPref.saveString(SharedPref.log, '0');
          emit(LoginError(message: response!.message!));
          showFailureMessage("check_entered_info".tr());
        }
      }
    } on Exception {
      emit(LoginError(message: "can_not_fetch_list".tr()));
    }
  }
}
