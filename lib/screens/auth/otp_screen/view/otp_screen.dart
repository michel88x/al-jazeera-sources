import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_images.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/base_widgets/custom_button.dart';
import 'package:employee/screens/auth/otp_screen/bloc/otp_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';

class OtpScreen extends StatefulWidget {

  final String? confirmCode;

  const OtpScreen({super.key, this.confirmCode});

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  String code = '';
  bool _onEditing = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: BlocProvider(
            create: (context) => OtpCubit(),
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              children: [
                Column(
                  children: [
                    const SizedBox(height: 80),
                    Center(
                      child: Image.asset(
                        AppImages.logo,
                        height: 150,
                        width: 150,
                      ),
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 50, 20, 0.0),
                      child: Row(
                        children: [
                          Text('verification_code'.tr(),
                              style: AppStyles.aleoTwelvePrimaryBold),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 5, 20, 0.0),
                      child: VerificationCode(
                        textStyle: AppStyles.cairoTwentyBlackBold,
                        keyboardType: TextInputType.number,
                        underlineColor: AppColors.secondary,
                        length: 4,
                        cursorColor: AppColors.primary,
                        clearAll: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'resend_code'.tr(),
                            style: AppStyles.cairoFourteenBlueUnderline,
                          ),
                        ),
                        onCompleted: (String value) {
                          setState(() {
                            code = value;
                          });
                        },
                        onEditing: (bool value) {
                          setState(() {
                            _onEditing = value;
                          });
                          if (!_onEditing) FocusScope.of(context).unfocus();
                        },
                        isSecure: true,
                        autofocus: true,
                        margin: const EdgeInsets.all(5),
                        fullBorder: true,
                      ),
                    ),
                    const SizedBox(height: 10,),
                    Center(
                      child: TextButton(
                        child: Text(
                          "resend_code".tr(),
                          textAlign: TextAlign.center,
                          style: AppStyles.cairoFourteenPrimaryBold,
                        ),
                        onPressed: (){
                          context.read<OtpCubit>().resendCode(context);
                        },
                      ),
                    ),
                    Center(child: BlocBuilder<OtpCubit, OtpState>(
                      builder: (context, state) {
                        if (state is LoginInitial) {
                          return CustomButton(
                            onPressed: () {
                              context.read<OtpCubit>().checkCode(context, code);
                            },
                            padding: const EdgeInsets.fromLTRB(35, 55, 35, 0.0),
                            backColor: AppColors.secondary,
                            borderRadius: 15,
                            innerPadding:
                                const EdgeInsets.fromLTRB(100, 10, 100, 10),
                            title: 'send_verification_code'.tr(),
                            textStyle: AppStyles.aleoFifteenWhiteBold,
                          );
                        } else if (state is LoginSuccess) {
                          return CustomButton(
                            onPressed: () {
                              context.read<OtpCubit>().checkCode(context, code);
                            },
                            padding: const EdgeInsets.fromLTRB(35, 55, 35, 0.0),
                            backColor: AppColors.secondary,
                            borderRadius: 15,
                            innerPadding:
                                const EdgeInsets.fromLTRB(100, 10, 100, 10),
                            title: 'send_verification_code'.tr(),
                            textStyle: AppStyles.aleoFifteenWhiteBold,
                          );
                        } else if (state is LoginLoaded) {
                          return const CircularProgressIndicator();
                        } else if (state is LoginError) {
                          return CustomButton(
                            onPressed: () {
                              context.read<OtpCubit>().checkCode(context, code);
                            },
                            padding: const EdgeInsets.fromLTRB(35, 55, 35, 0.0),
                            backColor: AppColors.secondary,
                            borderRadius: 15,
                            innerPadding:
                                const EdgeInsets.fromLTRB(100, 10, 100, 10),
                            title: 'send_verification_code'.tr(),
                            textStyle: AppStyles.aleoFifteenWhiteBold,
                          );
                        } else {
                          return const CircularProgressIndicator();
                        }
                      },
                    )),
                  ],
                ),
              ],
            )));
  }
}
