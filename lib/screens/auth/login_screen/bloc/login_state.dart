part of 'login_cubit.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {
}

class LoginLoaded extends LoginState {

}
class LoginSuccess extends LoginState {
  final LoginResponse data;
  LoginSuccess({required this.data});
}

class LoginError extends LoginState {
  final String message;
  LoginError({required this.message});
}

