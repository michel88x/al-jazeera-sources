import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/constants/enums.dart';
import 'package:employee/core/network/models/s_base_response.dart';
import 'package:employee/core/network/urls.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:employee/core/utils/shared_pref.dart';
import 'package:employee/screens/auth/otp_screen/view/otp_screen.dart';
import 'package:employee/screens/auth/login_screen/models/login_response.dart';
import 'package:employee/screens/general/privacy_screen/view/privacy_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(LoginInitial());

  static LoginCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  Future<void> loginUser(
      BuildContext context, String email, String pass) async {
    try {
      if (email == '' || pass == '') {
        showFailureMessage("enter_all_fields".tr());
      } else {
        emit(LoginLoaded());
        SBaseResponse? response = await callObjectAPI(
            method: APIMethod.post,
            stringBody: json.encode({
              "email" : email,
              "password" : pass
            }),
            url: Urls.login);
        if(response != null && response.success){
          LoginResponse res = LoginResponse.fromJson(response!.data!);
          await saveUser(res.user!);
          await saveToken(res.token!);

          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
            if(response.confirm!.isItAccount! || response.confirm!.confirm!){
              goto(context, PrivacyScreen());
            }else{
              goto(context, OtpScreen(confirmCode: res.user!.confirmCode!.toString(),));
            }
          });
          emit(LoginSuccess(data: res));
        }else{
          await SharedPref.saveString(SharedPref.log, '0');
          emit(LoginError(message: response!.message!));
          showFailureMessage("check_entered_info".tr());
        }
      }
    } on Exception {
      emit(LoginError(
          message: "can_not_fetch_list".tr()));
    }
  }
}
