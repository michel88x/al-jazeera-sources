import 'package:easy_localization/easy_localization.dart';
import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_images.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/base_widgets/custom_button.dart';
import 'package:employee/core/base_widgets/custom_text_field.dart';
import 'package:employee/core/utils/base_functions.dart';
import 'package:employee/screens/auth/forget_password_screen/view/forget_password_screen.dart';
import 'package:employee/screens/auth/login_screen/bloc/login_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool showLog = true;
  TextEditingController emailLog = TextEditingController();
  TextEditingController passLog = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: BlocProvider(
            create: (context) => LoginCubit(),
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              children: [
                Column(
                  children: [
                    const SizedBox(height: 80),
                    Center(
                      child: Image.asset(
                        AppImages.logo,
                        height: 150,
                        width: 150,
                      ),
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0.0),
                      child: Row(
                        children: [
                          Text('app_name'.tr(),
                              style: AppStyles.acmeTwentyOnePrimaryBoldItalic),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 50, 20, 0.0),
                      child: Row(
                        children: [
                          Text('email'.tr(),
                              style: AppStyles.aleoTwelvePrimaryBold),
                        ],
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(20, 5, 20, 0.0),
                        child: CustomTextField(
                          width: MediaQuery.of(context).size.width - 50,
                          borderRadius: 15,
                          borderColor: AppColors.primary.withOpacity(0.7),
                          boxPaddings: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                          controller: emailLog,
                          label: "email".tr(),
                          prefixIcon: const Icon(
                            Icons.email,
                            color: AppColors.secondary,
                            size: 20,
                          ),
                          labelStyle: AppStyles.tajawalNormalGrey,
                          fieldVerticalPadding: 5,
                          fieldHorizontalPadding: 0,
                        )),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 20, 20, 0.0),
                      child: Row(
                        children: [
                          Text('password'.tr(),
                              style: AppStyles.aleoTwelvePrimaryBold),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20, 5, 20, 0.0),
                      child: CustomTextField(
                        width: MediaQuery.of(context).size.width - 50,
                        borderRadius: 15,
                        borderColor: AppColors.primary.withOpacity(0.7),
                        boxPaddings: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                        controller: passLog,
                        label: "password".tr(),
                        prefixIcon: const Icon(
                          Icons.key,
                          color: AppColors.secondary,
                          size: 20,
                        ),
                        labelStyle: AppStyles.tajawalNormalGrey,
                        fieldVerticalPadding: 5,
                        fieldHorizontalPadding: 0,
                        isPassword: true,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        goto(context, ForgetPasswordScreen());
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(35, 5, 35, 0.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text('${"forget_password".tr()} ؟ ',
                                style: AppStyles.aleoTwelvePrimaryBold),
                          ],
                        ),
                      ),
                    ),
                    Center(child: BlocBuilder<LoginCubit, LoginState>(
                      builder: (context, state) {
                        if (state is LoginInitial) {
                          return CustomButton(
                            onPressed: () {
                              context.read<LoginCubit>().loginUser(
                                  context, emailLog.text, passLog.text);
                            },
                            padding: const EdgeInsets.fromLTRB(35, 55, 35, 0.0),
                            backColor: AppColors.secondary,
                            borderRadius: 15,
                            innerPadding:
                                const EdgeInsets.fromLTRB(100, 10, 100, 10),
                            title: 'login'.tr(),
                            textStyle: AppStyles.aleoSeventeenWhiteBold,
                          );
                        } else if (state is LoginSuccess) {
                          return CustomButton(
                            onPressed: () {
                              context.read<LoginCubit>().loginUser(
                                  context, emailLog.text, passLog.text);
                            },
                            padding: const EdgeInsets.fromLTRB(35, 55, 35, 0.0),
                            backColor: AppColors.secondary,
                            borderRadius: 15,
                            innerPadding:
                                const EdgeInsets.fromLTRB(100, 10, 100, 10),
                            title: 'login'.tr(),
                            textStyle: AppStyles.aleoSeventeenWhiteBold,
                          );
                        } else if (state is LoginLoaded) {
                          return const CircularProgressIndicator();
                        } else if (state is LoginError) {
                          return CustomButton(
                            onPressed: () {
                              context.read<LoginCubit>().loginUser(
                                  context, emailLog.text, passLog.text);
                            },
                            padding: const EdgeInsets.fromLTRB(35, 55, 35, 0.0),
                            backColor: AppColors.secondary,
                            borderRadius: 15,
                            innerPadding:
                                const EdgeInsets.fromLTRB(100, 10, 100, 10),
                            title: 'login'.tr(),
                            textStyle: AppStyles.aleoSeventeenWhiteBold,
                          );
                        } else {
                          return const CircularProgressIndicator();
                        }
                      },
                    )),
                  ],
                ),
              ],
            )));
  }
}
