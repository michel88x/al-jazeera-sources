class PermissionObject {
  int? id;
  String? name;
  String? showName;
  int? parentId;
  PermissionObject(
      {this.id,
        this.name,
        this.showName,
        this.parentId,
        });

  PermissionObject.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    showName = json['show_name'];
    parentId = json['parent_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['show_name'] = this.showName;
    data['parent_id'] = this.parentId;
    return data;
  }
}