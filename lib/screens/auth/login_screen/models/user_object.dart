import 'package:employee/screens/auth/login_screen/models/permission_object.dart';

class UserObject {
  int? id;
  int? id2;
  String? name;
  String? email;
  int? isAdmin;
  int? statusId;
  String? mobile;
  String? bn;
  String? position;
  String? activity;
  String? department;
  String? note;
  String? emailVerifiedAt;
  int? isConfirmed;
  int? confirmCode;
  int? cardFrom;
  int? cardTo;
  String? image;
  int? departmentId;
  String? createdAt;
  String? updatedAt;
  int? isOpen;
  String? faceToken;
  String? startWorkTime;
  String? endWorkTime;
  String? startAutoTime;
  String? endAutoTime;
  String? daysOff;
  String? deletedAt;
  String? codeTime;
  int? dismissalId;
  String? dismissalDate;
  dynamic managerId;
  int? departmentId2;
  dynamic daysOn;
  int? sendSms;
  int? sendEmail;
  int? sendWhatsapp;
  dynamic idNumber;
  String? signature;
  String? performanceValue;
  String? apiToken;
  dynamic violationStatus;
  int? countryId;
  String? violationOnOff;
  List<PermissionObject>? permissions;

  UserObject(
      {this.id,
        this.id2,
        this.name,
        this.email,
        this.isAdmin,
        this.statusId,
        this.mobile,
        this.bn,
        this.position,
        this.activity,
        this.department,
        this.note,
        this.emailVerifiedAt,
        this.isConfirmed,
        this.confirmCode,
        this.cardFrom,
        this.cardTo,
        this.image,
        this.departmentId,
        this.createdAt,
        this.updatedAt,
        this.isOpen,
        this.faceToken,
        this.startWorkTime,
        this.endWorkTime,
        this.startAutoTime,
        this.endAutoTime,
        this.daysOff,
        this.deletedAt,
        this.codeTime,
        this.dismissalId,
        this.dismissalDate,
        this.managerId,
        this.departmentId2,
        this.daysOn,
        this.sendSms,
        this.sendEmail,
        this.sendWhatsapp,
        this.idNumber,
        this.signature,
        this.performanceValue,
        this.apiToken,
        this.violationStatus,
        this.countryId,
        this.violationOnOff,
        this.permissions});

  UserObject.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    id2 = json['id2'];
    name = json['name'];
    email = json['email'];
    isAdmin = json['is_admin'];
    statusId = json['status_id'];
    mobile = json['mobile'];
    bn = json['bn'];
    position = json['position'];
    activity = json['activity'];
    department = json['department'];
    note = json['note'];
    emailVerifiedAt = json['email_verified_at'];
    isConfirmed = json['is_confirmed'];
    confirmCode = json['confirm_code'];
    cardFrom = json['card_from'];
    cardTo = json['card_to'];
    image = json['image'];
    departmentId = json['department_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    isOpen = json['is_open'];
    faceToken = json['face_token'];
    startWorkTime = json['start_work_time'];
    endWorkTime = json['end_work_time'];
    startAutoTime = json['start_auto_time'];
    endAutoTime = json['end_auto_time'];
    daysOff = json['days_off'];
    deletedAt = json['deleted_at'];
    codeTime = json['code_time'];
    dismissalId = json['dismissal_id'];
    dismissalDate = json['dismissal_date'];
    managerId = json['manager_id'];
    departmentId2 = json['department_id2'];
    daysOn = json['days_on'];
    sendSms = json['send_sms'];
    sendEmail = json['send_email'];
    sendWhatsapp = json['send_whatsapp'];
    idNumber = json['id_number'];
    signature = json['signature'];
    performanceValue = json['performance_value'];
    apiToken = json['api_token'];
    violationStatus = json['violation_status'];
    countryId = json['country_id'];
    violationOnOff = json['violation_on_off'];
    if (json['permissions'] != null) {
      permissions = <PermissionObject>[];
      json['permissions'].forEach((v) {
        permissions!.add(new PermissionObject.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id2'] = this.id2;
    data['name'] = this.name;
    data['email'] = this.email;
    data['is_admin'] = this.isAdmin;
    data['status_id'] = this.statusId;
    data['mobile'] = this.mobile;
    data['bn'] = this.bn;
    data['position'] = this.position;
    data['activity'] = this.activity;
    data['department'] = this.department;
    data['note'] = this.note;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['is_confirmed'] = this.isConfirmed;
    data['confirm_code'] = this.confirmCode;
    data['card_from'] = this.cardFrom;
    data['card_to'] = this.cardTo;
    data['image'] = this.image;
    data['department_id'] = this.departmentId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['is_open'] = this.isOpen;
    data['face_token'] = this.faceToken;
    data['start_work_time'] = this.startWorkTime;
    data['end_work_time'] = this.endWorkTime;
    data['start_auto_time'] = this.startAutoTime;
    data['end_auto_time'] = this.endAutoTime;
    data['days_off'] = this.daysOff;
    data['deleted_at'] = this.deletedAt;
    data['code_time'] = this.codeTime;
    data['dismissal_id'] = this.dismissalId;
    data['dismissal_date'] = this.dismissalDate;
    data['manager_id'] = this.managerId;
    data['department_id2'] = this.departmentId2;
    data['days_on'] = this.daysOn;
    data['send_sms'] = this.sendSms;
    data['send_email'] = this.sendEmail;
    data['send_whatsapp'] = this.sendWhatsapp;
    data['id_number'] = this.idNumber;
    data['signature'] = this.signature;
    data['performance_value'] = this.performanceValue;
    data['api_token'] = this.apiToken;
    data['violation_status'] = this.violationStatus;
    data['country_id'] = this.countryId;
    data['violation_on_off'] = this.violationOnOff;
    if (this.permissions != null) {
      data['permissions'] = this.permissions!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}