import 'package:employee/screens/human_resources_screen/services/general_service.dart';

class generalRepositoryImpl implements GeneralRepository {
  GeneralService service = GeneralService();

  @override
  Future<Map<String, dynamic>> getHrListGeneral(String token, String url) {
    return service.getHrListGeneral(token, url);
  }
}

abstract class GeneralRepository {
  Future<Map<String, dynamic>> getHrListGeneral(String token, String url);
}
