import 'package:employee/core/app/app_colors.dart';
import 'package:employee/core/app/app_styles.dart';
import 'package:employee/core/constants/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:google_fonts/google_fonts.dart';


class App_Bar extends StatelessWidget  {
  @override
  App_Bar(
      {this.title,
        this.LeadingIcon,

        this.LeadingOnClick,
this.actionIcon,
        this.actionClick

      });

  var LeadingIcon;


  var title;

  var LeadingOnClick;
  var actionIcon;



  var actionClick;


  // final double height;

  // final IconData suffixIcon;
  // @override
  // Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {


    return  ClipPath(
      clipper: WaveClipperTwo(),
      child: Container(
        height: 120,
        color: AppColors.primary,
        child: Stack(
          children: [
            Align(alignment: Alignment.topCenter, child: Padding(
              padding: const EdgeInsets.fromLTRB(0,30.0,0,0),
              child: Text(title,
                  style:  AppStyles.cairoNineteenWhiteBold),
            ),),
            Align(alignment: Alignment.topRight,
              child: Padding(padding: EdgeInsets.fromLTRB(5, 30, 5, 0),
                child: IconButton(
                  icon:  Icon(LeadingIcon,color: Colors.white,),
                  onPressed: LeadingOnClick,
                ),),),
            Align(alignment: Alignment.topLeft,
              child: Padding(padding: EdgeInsets.fromLTRB(5, 30, 5, 0),
                child: IconButton(
                  icon:  Icon(actionIcon,color: Colors.white,),
                  onPressed: actionClick,
                ),),)
          ],
        ),
      ),
    );
  }

}



